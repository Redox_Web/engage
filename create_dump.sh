#!/bin/bash
SECONDS=0
dbname=engage;

echo 'Starting to dump DB';
mysqldump -h mysql -uroot -p12345 $dbname > data/dumps/dump_"$dbname".sql
echo 'Dump finished. Good Day!';

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
exit 0;