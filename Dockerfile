FROM php:7.0-apache

RUN useradd --create-home -s /bin/bash user\
    && usermod -aG user root\
    && echo -n 'user:user' | chpasswd \
    && chown -R user:user /home/user

WORKDIR /home/user/www

RUN apt-get update \
  && echo 'deb http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list \
  && echo 'deb-src http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list \
  && apt-get install -y wget \
  && wget https://www.dotdeb.org/dotdeb.gpg \
  && apt-key add dotdeb.gpg \
  && apt-get update \
  && apt-get install -y php7.0-mysql \
  && docker-php-ext-install pdo_mysql \
 && apt-get install -y git zlib1g-dev \
 && apt-get install -y libpcre3-dev \
 && apt-get install -y libsodium-dev \
 && apt-get install sqlite3 \
 && pecl install libsodium \
 && docker-php-ext-enable libsodium \
 && docker-php-ext-install zip \
 && a2enmod rewrite \
 && a2enmod proxy \
 && a2enmod proxy_wstunnel \
 && a2enmod headers \
 && a2enmod proxy_http \
 && mv /var/www/html /var/www/public \
 && echo -e "\nexport TERM=xterm" >> /etc/bash.bashrc\
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

RUN /etc/init.d/apache2 start \

COPY ./data/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./data/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf

COPY ./data/fullchain.pem /etc/letsencrypt/live/engage-dev.redox-dev.com/fullchain.pem
COPY ./data/privkey.pem /etc/letsencrypt/live/engage-dev.redox-dev.com/privkey.pem

# Common
RUN apt-get update \
	&& apt-get install -y \
		openssl \
		curl

# PHP
# intl
RUN apt-get update \
	&& apt-get install -y libicu-dev \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install intl

# xml
RUN apt-get update \
	&& apt-get install -y \
	libxml2-dev \
	libxslt-dev \
	php-soap \
	&& docker-php-ext-install \
		dom \
		xmlrpc \
		xsl

# images
RUN apt-get update \
	&& apt-get install -y \
	libfreetype6-dev \
	libjpeg62-turbo-dev \
	libpng12-dev \
	libgd-dev \
	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install \
		gd \
		exif

# mcrypt
RUN apt-get update \
	&& apt-get install -y libmcrypt-dev \
	&& docker-php-ext-install mcrypt

# strings
RUN docker-php-ext-install \
	gettext \
	mbstring

# compression
RUN apt-get update \
	&& apt-get install -y \
	libbz2-dev \
	zlib1g-dev \
	&& docker-php-ext-install \
		zip \
		bz2

# sendmail
RUN apt-get update \
    && apt-get install -y sendmail \
    && apt-get install -y php7.0-xdebug

# others
RUN docker-php-ext-install \
	soap \
	sockets \
	calendar \
	sysvmsg \
	sysvsem \
	sysvshm

# Install XDebug, but not enable by default. Enable using:
# * php -d$XDEBUG_EXT vendor/bin/phpunit
# * php_xdebug vendor/bin/phpunit
RUN pecl install xdebug-2.5.0
ENV XDEBUG_EXT zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20160303/xdebug.so
RUN alias php_xdebug="php -d$XDEBUG_EXT vendor/bin/phpunit"

# Install PHP Code sniffer
RUN curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar \
	&& chmod 755 phpcs.phar \
	&& mv phpcs.phar /usr/local/bin/ \
	&& ln -s /usr/local/bin/phpcs.phar /usr/local/bin/phpcs \
	&& curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar \
	&& chmod 755 phpcbf.phar \
	&& mv phpcbf.phar /usr/local/bin/ \
	&& ln -s /usr/local/bin/phpcbf.phar /usr/local/bin/phpcbf

# Install PHPUnit
RUN curl -OL https://phar.phpunit.de/phpunit.phar \
	&& chmod 755 phpunit.phar \
	&& mv phpunit.phar /usr/local/bin/ \
	&& ln -s /usr/local/bin/phpunit.phar /usr/local/bin/phpunit

# Clean
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/*


RUN chown -R user:user /home/user

# mysql client
RUN apt-get update \
    && apt-get install -y mysql-client
