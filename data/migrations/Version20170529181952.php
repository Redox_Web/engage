<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170529181952 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $result = $this->connection->fetchAssoc("SELECT id FROM acl_roles WHERE name = 'user'");

        $this->addSql("INSERT INTO acl_actions (role_id, resource_class, resource_id, actions) VALUES({$result['id']}, 'string', 'user', 'a:1:{i:0;s:6:\"logout\";}')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM acl_actions WHERE resource_id='user'");
    }
}
