<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170606134453 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offline_event ADD alias VARCHAR(250) NOT NULL, ADD address VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX ux_natural ON social_user');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offline_event DROP alias, DROP address');
        $this->addSql('CREATE UNIQUE INDEX ux_natural ON social_user (`key`, social_id)');
    }
}
