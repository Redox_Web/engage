<?php

namespace MigrationsRedox;

use Application\Entity\User\Sex;
use Application\Entity\User\State;
use Application\Entity\User\UserAbstract;
use Application\Service\User\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use RedoxWeb\WTL\Tool\StaticServiceManager;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170620093208 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $serviceLocator = StaticServiceManager::getServiceManager();
        /** @var User $service */
        $service = $serviceLocator->get(User::class);

        $data['email'] = 'admin@engage.dev';
        $data['state'] = State::ACTIVE;
        $data['identity'] = '';
        $data['fullName'] = 'Admin';
        $data['password'] = 'admin777';
        $data['birthday'] = new \DateTime('17-10-1977');
        $data['type'] = $service::TYPE_ADMIN;
        $data['sex'] = Sex::MALE;
        $service->create($data);
    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `users` WHERE `users`.`email` = 'admin@engage.dev'");
        $this->addSql("DELETE FROM `users_auth` WHERE `users_auth`.`username` = 'admin@engage.dev'");
    }
}
