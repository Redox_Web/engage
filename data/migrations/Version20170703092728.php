<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use RedoxWeb\WTL\Tool\StaticServiceManager;
use StaticPages\Service\Page;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170703092728 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $data = [
            'name' => 'Help',
            'alias' => 'help',
            'content' => 'This is Help Page',
            'javascript' => '',
            'url' => 'help',
            'state' => '1',
            'group' => 'nullGroup',
            'template' => 'full_page',
            'site' => 'front_end',
        ];

        $serviceManager = StaticServiceManager::getServiceManager();
        /** @var Page $staticPageService */
        $staticPageService = $serviceManager->get(Page::class);
        $staticPageService->create($data);
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `pages` WHERE `pages`.`alias` = 'help'");
    }
}
