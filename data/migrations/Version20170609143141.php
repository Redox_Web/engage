<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170609143141 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, parent INT DEFAULT NULL, filename VARCHAR(255) NOT NULL, size NUMERIC(10, 0) NOT NULL, type VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, object_type VARCHAR(255) NOT NULL, width INT DEFAULT NULL, height INT DEFAULT NULL, inheritance_type VARCHAR(255) NOT NULL, INDEX IDX_C53D045F3D8E604F (parent), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F3D8E604F FOREIGN KEY (parent) REFERENCES image (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F3D8E604F');
        $this->addSql('DROP TABLE image');
    }
}
