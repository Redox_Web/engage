<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170612175708 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE active_subscription_package DROP FOREIGN KEY FK_4318002BA76ED395');
        $this->addSql('ALTER TABLE active_subscription_package CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE active_subscription_package ADD CONSTRAINT FK_4318002BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE active_subscription_package DROP FOREIGN KEY FK_4318002BA76ED395');
        $this->addSql('ALTER TABLE active_subscription_package CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE active_subscription_package ADD CONSTRAINT FK_4318002BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
    }
}
