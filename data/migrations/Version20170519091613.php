<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170519091613 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users_auth (user_id INT UNSIGNED NOT NULL, username VARCHAR(254) NOT NULL, hash VARCHAR(255) NOT NULL, session_key CHAR(192) DEFAULT NULL, reset_hash CHAR(32) DEFAULT NULL, reset_expiry DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_3757FE57F85E0677 (username), INDEX username_idx (username), PRIMARY KEY(user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_actions (id INT AUTO_INCREMENT NOT NULL, role_id INT DEFAULT NULL, resource_class VARCHAR(255) NOT NULL, resource_id VARCHAR(255) NOT NULL, actions LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_6E78FA2CD60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_roles (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_32A763785E237E06 (name), INDEX IDX_32A76378727ACA70 (parent_id), INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_atoms (user_id INT UNSIGNED NOT NULL, `key` VARCHAR(255) NOT NULL, `value` VARCHAR(255) NOT NULL, PRIMARY KEY(user_id, `key`)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_auth_logs (id INT UNSIGNED AUTO_INCREMENT NOT NULL, user_id INT UNSIGNED NOT NULL, auth_time DATETIME NOT NULL, ip_address CHAR(16) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_actions_users (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, resource_class VARCHAR(255) NOT NULL, resource_id VARCHAR(255) NOT NULL, actions LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_93AA9DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE acl_actions ADD CONSTRAINT FK_6E78FA2CD60322AC FOREIGN KEY (role_id) REFERENCES acl_roles (id)');
        $this->addSql('ALTER TABLE acl_roles ADD CONSTRAINT FK_32A76378727ACA70 FOREIGN KEY (parent_id) REFERENCES acl_roles (id)');
        $this->addSql('ALTER TABLE acl_actions_users ADD CONSTRAINT FK_93AA9DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acl_actions DROP FOREIGN KEY FK_6E78FA2CD60322AC');
        $this->addSql('ALTER TABLE acl_roles DROP FOREIGN KEY FK_32A76378727ACA70');
        $this->addSql('DROP TABLE users_auth');
        $this->addSql('DROP TABLE acl_actions');
        $this->addSql('DROP TABLE acl_roles');
        $this->addSql('DROP TABLE users_atoms');
        $this->addSql('DROP TABLE users_auth_logs');
        $this->addSql('DROP TABLE acl_actions_users');
    }
}
