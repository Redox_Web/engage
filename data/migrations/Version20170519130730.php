<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170519130730 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE address address VARCHAR(250) DEFAULT NULL, CHANGE timezone timezone VARCHAR(50) DEFAULT NULL, CHANGE description description VARCHAR(250) DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE address address VARCHAR(250) NOT NULL COLLATE utf8_unicode_ci, CHANGE timezone timezone VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE description description VARCHAR(250) NOT NULL COLLATE utf8_unicode_ci');
    }
}
