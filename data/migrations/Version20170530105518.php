<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170530105518 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE pwd_reset_token pwd_reset_token VARCHAR(32) DEFAULT NULL, CHANGE pwd_reset_token_creation_date pwd_reset_token_creation_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE pwd_reset_token pwd_reset_token VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE pwd_reset_token_creation_date pwd_reset_token_creation_date DATETIME NOT NULL');
    }
}
