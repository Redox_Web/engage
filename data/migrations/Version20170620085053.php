<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170620085053 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription_package DROP trial');
        $this->addSql('ALTER TABLE course_item ADD chapter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course_item ADD CONSTRAINT FK_D7B9F818579F4768 FOREIGN KEY (chapter_id) REFERENCES course_chapter (id)');
        $this->addSql('CREATE INDEX IDX_D7B9F818579F4768 ON course_item (chapter_id)');
        $this->addSql('ALTER TABLE course_chapter ADD course_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course_chapter ADD CONSTRAINT FK_F142B0C2591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('CREATE INDEX IDX_F142B0C2591CC992 ON course_chapter (course_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course_chapter DROP FOREIGN KEY FK_F142B0C2591CC992');
        $this->addSql('DROP INDEX IDX_F142B0C2591CC992 ON course_chapter');
        $this->addSql('ALTER TABLE course_chapter DROP course_id');
        $this->addSql('ALTER TABLE course_item DROP FOREIGN KEY FK_D7B9F818579F4768');
        $this->addSql('DROP INDEX IDX_D7B9F818579F4768 ON course_item');
        $this->addSql('ALTER TABLE course_item DROP chapter_id');
        $this->addSql('ALTER TABLE subscription_package ADD trial TINYINT(1) NOT NULL');
    }
}
