<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170523070326 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $result = $this->connection->fetchAssoc("SELECT id FROM acl_roles WHERE name = 'user'");


        $this->addSql("INSERT INTO acl_roles (parent_id, name) VALUES({$result['id']}, 'admin')");
        $this->addSql("INSERT INTO acl_roles (parent_id, name) VALUES({$result['id']}, 'student')");
        $this->addSql("INSERT INTO acl_roles (parent_id, name) VALUES({$result['id']}, 'professor')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM acl_roles WHERE name='admin'");
        $this->addSql("DELETE FROM acl_roles WHERE name='student'");
        $this->addSql("DELETE FROM acl_roles WHERE name='professor'");
    }
}
