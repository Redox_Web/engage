<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170711143136 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $result = $this->connection->fetchAssoc("SELECT id FROM acl_roles WHERE name = 'professor'");

        $this->addSql("INSERT INTO acl_actions (role_id, resource_class, resource_id, actions) VALUES({$result['id']}, 'string', 'top_menu', 'a:1:{i:0;s:20:\"professorShowCourses\";}')");

        $result = $this->connection->fetchAssoc("SELECT id FROM acl_roles WHERE name = 'student'");
        $this->addSql("INSERT INTO acl_actions (role_id, resource_class, resource_id, actions) VALUES({$result['id']}, 'string', 'top_menu', 'a:1:{i:0;s:18:\"studentShowCourses\";}')");
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM acl_actions WHERE resource_id='top_menu'");
    }
}
