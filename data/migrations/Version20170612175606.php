<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170612175606 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE active_subscription_package DROP FOREIGN KEY FK_4318002B36A9EB9A');
        $this->addSql('ALTER TABLE active_subscription_package CHANGE subscription_package_id subscription_package_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE active_subscription_package ADD CONSTRAINT FK_4318002B36A9EB9A FOREIGN KEY (subscription_package_id) REFERENCES subscription_package (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE active_subscription_package DROP FOREIGN KEY FK_4318002B36A9EB9A');
        $this->addSql('ALTER TABLE active_subscription_package CHANGE subscription_package_id subscription_package_id INT NOT NULL');
        $this->addSql('ALTER TABLE active_subscription_package ADD CONSTRAINT FK_4318002B36A9EB9A FOREIGN KEY (subscription_package_id) REFERENCES subscription_package (id) ON DELETE CASCADE');
    }
}
