<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170525124038 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE full_name full_name VARCHAR(250) DEFAULT NULL');
        $this->addSql("INSERT IGNORE INTO `social` (`id`, `social_name`, `social_alias`, `social_url`) VALUES
                      (1, 'Facebook', 'facebook', 'https://www.facebook.com/'),
                      (2, 'Google', 'google', 'https://www.google.com/gmail/')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users CHANGE full_name full_name VARCHAR(250) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DELETE FROM `social` WHERE id IN(1,2)');
    }
}
