<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170616153405 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE courses (id INT AUTO_INCREMENT NOT NULL, author INT DEFAULT NULL, teacher INT DEFAULT NULL, name VARCHAR(250) NOT NULL, description LONGTEXT DEFAULT NULL, alias VARCHAR(255) NOT NULL, execution VARCHAR(250) NOT NULL, how_pass VARCHAR(250) NOT NULL, difficulty_difficulty VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_A9A55A4CE16C6B94 (alias), INDEX IDX_A9A55A4CBDAFD8C8 (author), INDEX IDX_A9A55A4CB0F6A6D5 (teacher), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses_categories (course_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_30ED10BD591CC992 (course_id), INDEX IDX_30ED10BD12469DE2 (category_id), PRIMARY KEY(course_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4CBDAFD8C8 FOREIGN KEY (author) REFERENCES users (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4CB0F6A6D5 FOREIGN KEY (teacher) REFERENCES users (id)');
        $this->addSql('ALTER TABLE courses_categories ADD CONSTRAINT FK_30ED10BD591CC992 FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses_categories ADD CONSTRAINT FK_30ED10BD12469DE2 FOREIGN KEY (category_id) REFERENCES course_category (id)');
        $this->addSql('ALTER TABLE course_category CHANGE alias alias VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AFF87497E16C6B94 ON course_category (alias)');
        $this->addSql('ALTER TABLE courses ADD institution VARCHAR(250) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courses_categories DROP FOREIGN KEY FK_30ED10BD591CC992');
        $this->addSql('DROP TABLE courses');
        $this->addSql('DROP TABLE courses_categories');
        $this->addSql('DROP INDEX UNIQ_AFF87497E16C6B94 ON course_category');
        $this->addSql('ALTER TABLE course_category CHANGE alias alias VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE courses DROP institution');
    }
}
