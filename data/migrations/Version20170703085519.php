<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use RedoxWeb\WTL\Tool\StaticServiceManager;
use StaticPages\Service\Page;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170703085519 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $data = [
            'name' => 'Course Stub',
            'alias' => 'course-stub',
            'content' => 'This is Course Page',
            'javascript' => '',
            'url' => 'courses/star-trek',
            'state' => '1',
            'group' => 'nullGroup',
            'template' => 'full_page',
            'site' => 'front_end',
        ];

        $serviceManager = StaticServiceManager::getServiceManager();
        /** @var Page $staticPageService */
        $staticPageService = $serviceManager->get(Page::class);
        $staticPageService->create($data);
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `pages` WHERE `pages`.`alias` = 'course-stub'");
    }
}
