<?php

namespace MigrationsRedox;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170524152352 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE social (id INT AUTO_INCREMENT NOT NULL, social_name VARCHAR(250) NOT NULL, social_alias VARCHAR(250) NOT NULL, social_url VARCHAR(250) NOT NULL, UNIQUE INDEX UNIQ_7161E187ABFCDF06 (social_alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, social_id INT DEFAULT NULL, `key` VARCHAR(255) NOT NULL, data DATETIME DEFAULT NULL, state_state INT NOT NULL, INDEX IDX_C282052DA76ED395 (user_id), INDEX IDX_C282052DFFEB5B27 (social_id), UNIQUE INDEX ux_natural (`key`, social_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE social_user ADD CONSTRAINT FK_C282052DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE social_user ADD CONSTRAINT FK_C282052DFFEB5B27 FOREIGN KEY (social_id) REFERENCES social (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE social_user DROP FOREIGN KEY FK_C282052DFFEB5B27');
        $this->addSql('DROP TABLE social');
        $this->addSql('DROP TABLE social_user');
    }
}
