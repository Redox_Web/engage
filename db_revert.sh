#!/bin/bash
SECONDS=0
dbname=engage;

echo 'Droping DB';
echo "DROP DATABASE $dbname;" | mysql -h mysql -u root -p12345
echo 'DB dropped';

echo 'Creating DB';
echo "CREATE DATABASE $dbname;" | mysql -h mysql -u root -p12345
echo 'DB created';

echo 'Start import DB';
mysql -h mysql -u root -p12345 $dbname < data/dumps/dump_"$dbname".sql
echo 'Import finished';

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
exit 0;