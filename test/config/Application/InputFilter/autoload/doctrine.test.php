<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'path' => 'test/data/db/db.common.input-filter.sqlite',
                )
            ),
        ),
    ),
);
