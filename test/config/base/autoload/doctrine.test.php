<?php
return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOSqlite\Driver',
                'params' => [
                    'path' => 'test/data/db/db.sqlite',
                ]
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache' => 'array',
                'generate_proxies' => true,
            ]
        ],
        'driver' => [
            'common_driver' => [
                'cache' => 'array',
            ],
            'auth_driver' => [
                'cache' => 'array',
            ],
            'static_pages_driver' => [
                'cache' => 'array',
            ],
            'seo_driver' => [
                'cache' => 'array',
            ],
        ],
    ],
];
