<?php

return [
    'service_manager' => [
        'invokables' => [
        ],
        'factories' => [
            \CirclicalUser\Service\AuthenticationService::class =>
                \FunctionalTest\Framework\Authentication\AuthServiceFactory::class,
            'mail.transport' => \Application\Factory\ServiceMail\FileTransport::class,
            \Zend\Router\RouteStackInterface::class => \FunctionalTest\Framework\RouterFactory::class,
            \Zend\Router\Http\TreeRouteStack::class => \FunctionalTest\Framework\RouterFactory::class,
        ],
    ],
    'redox' => [
        'mail' => array(
            'transport' => array(
                'file' => array
                (
                    'path' => 'test/data/mails/',
                    'callback'  => function (\Zend\Mail\Transport\File $transport) {
                        return 'Message_' . (new \DateTime())->format('Y-m-d_H-i') . '.txt';
                    },
                )
            )
        ),
        'file' => array(
            'uploadPath' => 'test/upload',
            'defaultFileInfoClass' => 'FunctionalTest\Application\Service\File\Mock\FileInfoMock'
        )
    ],
];
