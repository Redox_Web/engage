<?php
namespace FunctionalTest\Application\Seo;

use Frontend\Seo\ConfigProvider;
use FunctionalTest\Framework\Seo\SeoTestCase;
use RedoxWeb\WTL\Dependency\RootAble\RootAbleInterface;

/**
 * Class ExampleTest
 *
 * @author Shahnovsky Alex
 */
class ExampleTest extends SeoTestCase
{
    /**
     * @return string
     */
    protected function getProviderAlias()
    {
        return ConfigProvider::TEST_ALIAS;
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'expected' => array(),
     *      'object' => RootAbleInterface, Object transfered to seo system.
     *      'extra' => array, Some extra data that can be used in seo system.
     *      'fixtures' => array,
     * ]
     *
     * @return array
     */
    public function dataGetSeoData()
    {
        $rootAbleObject = $this->getMockBuilder(RootAbleInterface::class)->getMock();
        $rootAbleObject->expects($this->any())
            ->method('getRootAbleId')
            ->willReturn(15);
        $rootAbleObject->expects($this->any())
            ->method('getRootAbleType')
            ->willReturn('test-alias');

        return [
            'test with no object or extra data provided' => [
                [
                    'SeoH1' => '',
                    'SeoTitle' => '',
                ],
                null,
                [],
                [],
            ],
            'test with no object or extra data provided with fixture, no object' => [
                [
                    'SeoH1' => '',
                    'SeoTitle' => '',
                    'SeoDescription' => '',
                    'SeoKeywords' => '',
                ],
                null,
                [],
                [
                    \FunctionalTest\Application\Seo\Fixture\TestSeo::class
                ],
            ],
            'test with no object or extra data provided with fixture, with object' => [
                [
                    'SeoH1' => 'Seo H1 title',
                    'SeoTitle' => 'Seo title',
                    'SeoDescription' => 'SeoDescription',
                    'SeoKeywords' => '',
                ],
                $rootAbleObject,
                [],
                [
                    \FunctionalTest\Application\Seo\Fixture\TestSeo::class
                ],
            ],
        ];
    }
}
