<?php
namespace FunctionalTest\Application\Seo\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Seo\Entity\SeoDto;
use Seo\Entity\SeoObject;

/**
 * Class TestSeo
 *
 * @author Shahnovsky Alex
 */
class TestSeo extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $seoObject = new SeoObject();
        $seoObject->setObjectType('test-alias');
        $seoObject->setObjectId(15);
        $seoObject->setSeoData(SeoDto::createFromArray(
            [
                'SeoH1' => 'Seo H1 title',
                'SeoTitle' => 'Seo title',
                'SeoDescription' => 'SeoDescription',
            ]
        ));

        $manager->persist($seoObject);
        $manager->flush();
    }
}
