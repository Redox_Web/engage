<?php
namespace FunctionalTest\Application\InputFilterBuilder\Course\Item;

use Application\Entity\Course\Chapter;
use Application\Entity\Course\Item\Quiz;
use Application\Entity\Course\Item\Quiz\QuizVO;
use Application\Entity\Course\Item\State;
use FunctionalTest\Framework\EntityInputFilterTestCase;
use FunctionalTest\Framework\InstanceMarker\ObjectMarker;
use PHPUnit\Framework\TestCase;

/**
 * Class ArticleTest
 *
 * @author Shahnovsky Alex
 */
class QuizTest extends EntityInputFilterTestCase
{
    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Quiz::class;
    }

    /**
     * Список внутренних полей сущности, для которых не должно быть фильтров и валидаторов
     * identity - добавится автоматически
     *
     * @return array
     */
    protected function excludeProperties()
    {
        return [
            'alias',
        ];
    }

    /**
     * @return string
     */
    protected function getBuilderName()
    {
        return \Application\InputFilterBuilder\Course\Item\Strategy\Quiz::class;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    public function dataValid()
    {
        return [
            'valid data' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                        'questions' => [
                            [
                                'question' => 'Who?',
                                'answers' => [
                                    1 => 'correct answer',
                                ],
                                'correctAnswer' => 1,
                            ],
                        ],
                    ],
                    'chapter' => 1,
                ],
                'expected' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => new ObjectMarker(
                        State::class,
                        function (State $value, TestCase $phpunit) {
                            $phpunit->assertSame(State::ACTIVE, $value->getState());
                        }
                    ),
                    'startDate' => new ObjectMarker(
                        \DateTime::class,
                        function (\DateTime $value, TestCase $phpunit) {
                            $phpunit->assertSame('2015-01-01', $value->format('Y-m-d'));
                        }
                    ),
                    'quiz' => new ObjectMarker(
                        QuizVO::class,
                        function (QuizVO $value, TestCase $phpunit) {
                            $phpunit->assertSame(
                                [
                                    'questions' => [
                                        [
                                            'question' => 'Who?',
                                            'answers' => [
                                                1 => 'correct answer',
                                            ],
                                            'correctAnswer' => 1,
                                        ],
                                    ],
                                ],
                                $value->getQuizData()->toArray()
                            );
                        }
                    ),
                    'isFree' => true,
                    'chapter' => new ObjectMarker(
                        Chapter::class,
                        function (Chapter $value, TestCase $phpunit) {
                            $phpunit->assertSame(1, $value->getIdentity());
                        }
                    ),
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
            ],
        ];
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    public function dataNotValid()
    {
        return [
            'empty data' => [
                'data' => [
                ],
                'messages' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'wrong correct answer' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                        'questions' => [
                            [
                                'question' => 'Who?',
                                'answers' => [
                                    1 => 'correct answer',
                                ],
                                'correctAnswer' => 2,
                            ],
                        ],
                    ],
                ],
                'messages' => [
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'empty answers' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                        'questions' => [
                            [
                                'question' => 'Who?',
                                'answers' => [
                                ],
                                'correctAnswer' => 1,
                            ],
                        ],
                    ],
                ],
                'messages' => [
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'no question name' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                        'questions' => [
                            [
                                'answers' => [
                                    1 => 'correct answer',
                                ],
                                'correctAnswer' => 2,
                            ],
                        ],
                    ],
                ],
                'messages' => [
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'no questions' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                    ],
                ],
                'messages' => [
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'not array for quiz' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => 'string',
                ],
                'messages' => [
                    'quiz',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
        ];
    }
}
