<?php
namespace FunctionalTest\Application\InputFilterBuilder\Course\Item;

use Application\Entity\Course\Chapter;
use Application\Entity\Course\Item\Video;
use Application\Entity\Course\Item\State;
use FunctionalTest\Framework\EntityInputFilterTestCase;
use FunctionalTest\Framework\InstanceMarker\ObjectMarker;
use PHPUnit\Framework\TestCase;

/**
 * Class ArticleTest
 *
 * @author Shahnovsky Alex
 */
class VideoTest extends EntityInputFilterTestCase
{
    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Video::class;
    }

    /**
     * Список внутренних полей сущности, для которых не должно быть фильтров и валидаторов
     * identity - добавится автоматически
     *
     * @return array
     */
    protected function excludeProperties()
    {
        return [
            'alias',
        ];
    }

    /**
     * @return string
     */
    protected function getBuilderName()
    {
        return \Application\InputFilterBuilder\Course\Item\Strategy\Video::class;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    public function dataValid()
    {
        return [
            'valid data' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'video' => 'video/8df7b73a7820f4aef47864f2a6c5fccf',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'chapter' => 1,
                ],
                'expected' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'video' => 'video/8df7b73a7820f4aef47864f2a6c5fccf',
                    'state' => new ObjectMarker(
                        State::class,
                        function (State $value, TestCase $phpunit) {
                            $phpunit->assertSame(State::ACTIVE, $value->getState());
                        }
                    ),
                    'startDate' => new ObjectMarker(
                        \DateTime::class,
                        function (\DateTime $value, TestCase $phpunit) {
                            $phpunit->assertSame('2015-01-01', $value->format('Y-m-d'));
                        }
                    ),
                    'isFree' => true,
                    'chapter' => new ObjectMarker(
                        Chapter::class,
                        function (Chapter $value, TestCase $phpunit) {
                            $phpunit->assertSame(1, $value->getIdentity());
                        }
                    ),
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
            ],
        ];
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    public function dataNotValid()
    {
        return [
            'empty data' => [
                'data' => [
                ],
                'messages' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'video',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
            'invalid video url' => [
                'data' => [
                    'video' => new \stdClass(),
                ],
                'messages' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'video',
                    'chapter',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
        ];
    }
}
