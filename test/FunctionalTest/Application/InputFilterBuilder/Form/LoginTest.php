<?php
namespace FunctionalTest\Application\InputFilterBuilder\Form;

use FunctionalTest\Framework\InputFilterBuilderTestCase;

/**
 * Class Login
 *
 * @author Shahnovsky Alex
 */
class LoginTest extends InputFilterBuilderTestCase
{

    /**
     * @return string
     */
    protected function getBuilderName()
    {
        return \Application\InputFilterBuilder\Form\Login::class;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    public function dataValid()
    {
        return [
            [
                'data' => [
                    'email' => 'test@test.com',
                    'password' => 'password1234',
                ],
                'expected' => [
                    'email' => 'test@test.com',
                    'password' => 'password1234',
                ],
                'fixtures' => [
                ],
            ],
        ];
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    public function dataNotValid()
    {
        return [
            [
                'data' => [
                    'email' => 'test',
                ],
                'messages' => [
                    'email',
                    'password',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
        ];
    }
}
