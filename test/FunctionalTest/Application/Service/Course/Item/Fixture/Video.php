<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Video
 *
 * @author Shahnovsky Alex
 */
class Video extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $video = new \Application\Entity\Course\Item\Video();
        $video->setName('Video name');
        $video->setDescription('Video description');
        $video->setIsFree(true);
        $video->setStartDate(new \DateTime());
        $video->setState(State::createFromScalar(State::ACTIVE));
        $video->setVideo('video/8df7b73a7820f4aef47864f2a6c5fccf');
        $chapter = $this->getReference('course_chapter');
        $video->setChapter($chapter);

        $manager->persist($video);
        $manager->flush();
    }
}
