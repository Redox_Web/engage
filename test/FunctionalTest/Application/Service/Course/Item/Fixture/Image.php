<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Image
 *
 * @author Paladi Vitalie
 */
class Image extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $item = new \Application\Entity\Course\Item\Image();
        $item->setName('Image item name');
        $item->setDescription('Image item description');
        $item->setIsFree(true);
        $item->setStartDate(new \DateTime());
        $item->setState(State::createFromScalar(State::ACTIVE));
        $item->setImage('img/8df7b73a7820f4aef47864f2a6c5fccf');
        $chapter = $this->getReference('course_chapter');
        $item->setChapter($chapter);

        $manager->persist($item);
        $manager->flush();
    }
}
