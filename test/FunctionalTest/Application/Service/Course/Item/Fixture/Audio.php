<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Audio
 *
 * @author Shahnovsky Alex
 */
class Audio extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $audio = new \Application\Entity\Course\Item\Audio();
        $audio->setName('Audio name');
        $audio->setDescription('Audio description');
        $audio->setIsFree(true);
        $audio->setStartDate(new \DateTime());
        $audio->setState(State::createFromScalar(State::ACTIVE));
        $audio->setAudio('audio/audio-link');
        $chapter = $this->getReference('course_chapter');
        $audio->setChapter($chapter);

        $manager->persist($audio);
        $manager->flush();
    }
}
