<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Document
 *
 * @author Paladi Vitalie
 */
class Document extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $document = new \Application\Entity\Course\Item\Document();
        $document->setName('Document name');
        $document->setDescription('Document description');
        $document->setIsFree(true);
        $document->setStartDate(new \DateTime());
        $document->setState(State::createFromScalar(State::ACTIVE));
        $document->setDocument('http://youtube.com/some-link');
        $chapter = $this->getReference('course_chapter');
        $document->setChapter($chapter);

        $manager->persist($document);
        $manager->flush();
    }
}
