<?php
namespace FunctionalTest\Application\Service\Subscription\Fixture;

use Application\Entity\Subscription\ActiveSubscriptionPackage;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ActiveSubscriptionPackageFixture
 *
 * @author Shahnovsky Alex
 */
class ActiveSubscriptionPackageFixture extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $subscriptionPackage = $this->getReference('subscription_package');
        $user = $this->getReference('user_admin_fixture');

        $activeSubsPackage = new ActiveSubscriptionPackage();
        $activeSubsPackage->setUser($user);
        $activeSubsPackage->setSubscriptionPackage($subscriptionPackage);
        $activeSubsPackage->setStartDate(new \DateTime('2016-10-01 12:05:12'));
        $activeSubsPackage->setEndDate(new \DateTime('2017-01-01 12:05:12'));

        $manager->persist($activeSubsPackage);
        $manager->flush();
    }
}
