<?php

namespace FunctionalTest\Application\Service\File;

use Closure;
use \RedoxWeb\WTL\Entity\File\Image;
use \RedoxWeb\WTL\Entity\File\ImageAbstract;
use FunctionalTest\Framework\ServiceTestCase as TestCase;

class ImageTest extends TestCase
{
    protected function tearDown()
    {
        parent::tearDown();
        $files = glob($this->getFileUploadPath().'*'); // get all file names
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
    }

    /**
     * Возвращает имя сервиса в ServiceManager
     *
     * @return string
     */
    protected function getServiceName()
    {
        return \RedoxWeb\WTL\Service\File\Image::class;
    }

    /**
     * Возвращает тип сервиса (имя класса)
     *
     * @return string
     */
    protected function getServiceType()
    {
        return \RedoxWeb\WTL\Service\File\Image::class;
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'data' => [....],
     *      'fixtures' => array()
     *      'entityName' => string
     * ]
     *
     * @return array
     */
    public function dataCreateSuccess()
    {
        $function = function (ImageAbstract $entity, \PHPUnit\Framework\TestCase $self) {
            $self->assertFileExists($entity->getPath());
        };

        return array(
            'validData' => array(
                'data' => array(
                    'data' => [
                        'title' => 'Test title',
                        'objectType' => 'idea'
                    ],
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => $this->getFilePath().'redoxLeft.jpeg',
                        'error' => 0
                    ),
                    'inheritance_type' => 'image'
                ),
                'fixtures'   => [],
                'entityName' => \RedoxWeb\WTL\Entity\File\Image::class,
                'asserts' => $function
            ),
            'data set only type' => array(
                'data' => array(
                    'data' => [
                        'objectType' => 'idea'
                    ],
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => $this->getFilePath().'redoxLeft.jpeg',
                        'error' => 0
                    ),
                    'inheritance_type' => 'image'
                ),
                'fixtures'   => [],
                'entityName' => \RedoxWeb\WTL\Entity\File\Image::class,
                'asserts' => $function
            ),
            'data set inheritance type thumbnail' => array(
                'data' => array(
                    'data' => [
                        'objectType' => 'idea',
                        'sizeRectangle' => [
                            'width' => 300,
                            'height' => 200
                        ],
                        'parent' => '1',
                    ],
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => $this->getFilePath().'redoxLeft.jpeg',
                        'error' => 0
                    ),
                    'inheritance_type' => 'thumbnail'
                ),
                'fixtures'   => [
                    \FunctionalTest\Application\Service\File\Fixture\Image::class,
                ],
                'entityName' => \RedoxWeb\WTL\Entity\File\Thumbnail::class,
                'asserts' => $function
            ),
            /*'data not exist' => array(
                'data' => array(
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => $this->getFilePath().'redoxLeft.jpeg',
                        'error' => 0
                    )
                ),
                'fixtures'   => [],
                'entityName' => 'Common\Entity\File\Image',
                'asserts' => $function
            )*/
        );
    }

    /**
     * dataProvider for testCreateFailed
     *
     * Format: [
     *  'data' => array(invalid data),
     *  'fixtures' => array(),
     *  'messageKeys' => array(список всех ожидаемых ключей)
     * ]
     *
     * @return array
     */
    public function dataCreateFailed()
    {
        return array(
            'invalid name' => array(
                'data' => array(
                    'data' => [
                        'title' => 'Title test',
                        'objectType' => 'idea'
                    ],
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => 'нет файла',
                        'error' => 0
                    ),
                    'inheritance_type' => 'image'
                ),
                'fixtures' => [],
                'messageKeys' => array(
                    'file'
                )
            ),
            'invalid type' => array(
                'data' => array(
                    'data' => [
                        'title' => 'Title test',
                        'objectType' => ''
                    ],
                    'file' => array(
                        'name' => 'test.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => 'нет файла',
                        'error' => 0
                    ),
                    'inheritance_type' => 'image'
                ),
                'fixtures' => [],
                'messageKeys' => array(
                    'objectType'
                )
            ),
        );
    }

    /**
     * dataProvider For testUpdateSuccess
     *
     * Format: [
     *  'id' => int,
     *  'data' => array(...),
     *  'entityName' => 'имя entity',
     *  'fixtureName' => array(список классов fixtures),
     *  'expectedChanges' => array(список колонок в бд и их значения)
     * ]
     *
     * @return array
     */
    public function dataUpdateSuccess()
    {
        return [
            'one' => [
                'id' => 1,
                'data' => [
                    'data' => ['Новый тайтл'],
                    'file' => [
                        'name' => 'test22.jpeg',
                        'type' => 'image/jpeg',
                        'size' => 23288,
                        'tmp_name' => $this->getFilePath().'redoxLeft1.jpeg',
                        'error' => 0
                    ],
                    'inheritance_type' => 'image'
                ],
                'entityName' => \RedoxWeb\WTL\Entity\File\Image::class,
                'fixtures' => [
                    \FunctionalTest\Application\Service\File\Fixture\Image::class
                ],
                'expected' => [

                ]
            ],
        ];
    }

    /**
     * @expectedException \RuntimeException
     *
     * @param int          $identity
     * @param array        $data
     * @param string       $entityName
     * @param array|string $fixtureName
     * @param              $expectedChanges
     * @param callable     $callback
     * @dataProvider dataUpdateSuccess
     */
    public function testUpdateSuccess(
        $identity,
        $data,
        $entityName,
        $fixtureName,
        $expectedChanges,
        Closure $callback = null,
        $auth = ['role' => 'admin']
    ) {
        $this->loadFixtures($fixtureName);
        /** @var \RedoxWeb\WTL\Service\File\Image $service */
        $service = $this->getService($this->getServiceName());
        $service->update($identity, $data);
    }

    /**
     * dataProvider For testDeleteSuccess
     *
     * Format: [
     *  'identity' => int,
     *  'fixtures' => array(список классов fixtures),
     *  'asserts' => Closure
     * ]
     *
     * @return array
     */
    public function dataDeleteSuccess()
    {
        copy($this->getFilePath().'redoxLeft.jpeg', $this->getFileUploadPath().'test.jpeg');

        $function = function (Image $entity, \PHPUnit\Framework\TestCase $self) {
            $self->assertFileNotExists($entity->getPath());
        };

        return array(
            array(
                'id' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\File\Fixture\Image::class
                ],
                'asserts' => $function
            )
        );
    }

    public function getFilePath()
    {
        return $this->getRealPath('test/files');
    }

    public function getFileUploadPath()
    {
        return $this->getRealPath('test/upload');
    }

    public function getRealPath($path)
    {
        return rtrim(realpath($path), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }
}
