<?php

namespace FunctionalTest\Application\Service\File\Fixture;

use \RedoxWeb\WTL\Entity\File\Image as ImageEntity;
use \RedoxWeb\WTL\Entity\File\ObjectType;
use \RedoxWeb\WTL\StdLib\Geometric\Figure\Rectangle;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class Image extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $rectangle = new Rectangle(300, 200);
        $imagePath = $this->getFileUploadPath();
        $image1 = new ImageEntity();
        $image1->setSizeRectangle($rectangle);

        $objectType = new ObjectType(ObjectType::TYPE_IDEA);
        $image1->setObjectType($objectType);

        $reflect = new \ReflectionClass($image1);
        $fileName = $reflect->getProperty('fileName');
        $fileName->setAccessible(true);
        $fileName->setValue($image1, 'test.jpeg');

        $size = $reflect->getProperty('size');
        $size->setAccessible(true);
        $size->setValue($image1, 25678);

        $type = $reflect->getProperty('type');
        $type->setAccessible(true);
        $type->setValue($image1, 'img/jpg');

        $path = $reflect->getProperty('path');
        $path->setAccessible(true);
        $path->setValue($image1, $imagePath . $image1->getFileName());

        $manager->persist($image1);

        $manager->flush();
    }

    public function getFileUploadPath()
    {
        return $this->getRealPath('test/upload');
    }

    public function getRealPath($path)
    {
        return rtrim(realpath($path), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }
}
