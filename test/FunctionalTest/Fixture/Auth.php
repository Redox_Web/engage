<?php
namespace FunctionalTest\Fixture;

use CirclicalUser\Entity\Authentication;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class Roles
 *
 * @author Shahnovsky Alex
 */
class Auth extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {

        $auth = new Authentication(999, 'test_username', 'hash', 'session');
        $manager->persist($auth);
        $manager->flush();
    }
}
