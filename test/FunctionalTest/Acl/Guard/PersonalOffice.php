<?php
namespace FunctionalTest\Acl\Guard;

/**
 * Class BackendTest
 *
 * @author Shahnovsky Alex
 */
class PersonalOffice extends TestCase
{
    /**
     * @return array
     */
    public function getIsAllowed()
    {
        return [
            'chapter-update' => [
                'controller' => \PersonalOffice\Controller\ChapterController::class,
                'action' => 'chapterUpdate',
                'expected' => [
                    '*' => false,
                    'professor' => true,
                ],
            ],
            'chapter-create' => [
                'controller' => \PersonalOffice\Controller\ChapterController::class,
                'action' => 'chapterCreate',
                'expected' => [
                    '*' => false,
                    'professor' => true,
                ],
            ],
        ];
    }
}
