<?php
namespace FunctionalTest\Acl\Guard;

/**
 * Class BackendTest
 *
 * @author Shahnovsky Alex
 */
class BackendTest extends TestCase
{
    /**
     * @return array
     */
    public function getIsAllowed()
    {
        return [
            'home' => [
                'controller' => \Backend\Controller\IndexController::class,
                'action' => 'index',
                'expected' => [
                    '*' => false,
                    'admin' => true,
                ],
            ],
            'static pages' => [
                'controller' => \Backend\Controller\StaticPages\StaticPages::class,
                'action' => 'index',
                'expected' => [
                    '*' => false,
                    'admin' => true,
                ],
            ],
        ];
    }
}
