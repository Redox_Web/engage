<?php
namespace FunctionalTest\Acl\Guard;

use CirclicalUser\Service\AccessService;
use FunctionalTest\Acl\TestCase as AclTestCase;

/**
 * Class TestCase
 *
 * @author Shahnovsky Alex
 */
abstract class TestCase extends AclTestCase
{
    /**
     * формат ваозвращаемых данных:
     *
     * 'controller' => 'Controller',
     * 'action' => 'action',
     * 'allowed' => [
     *   'admin' => true,
     *   'user' => false,
     *   '*' => true
     * ]
     *
     * @return array
     */
    abstract public function getIsAllowed();

    /**
     * @return array
     */
    public function dataIsAllow()
    {
        $result = [];
        foreach ($this->getRoles() as $role) {
            foreach ($this->getIsAllowed() as $rule) {
                $rule['role'] = $role;
                $result[] = $rule;
            }
        }

        return $result;
    }

    /**
     * @dataProvider dataIsAllow
     *
     * @param $controller
     * @param $action
     * @param $expected
     * @param $role
     *
     * @internal param $routeName
     */
    public function testIsAllow($controller, $action, $expected, $role)
    {
        /** @var AccessService $accessService */
        $accessService = $this->getService(AccessService::class);
        $expectedForRole = $this->getExpectedResult($expected, $role);
        if ($role == 'group-less') {
            $this->logout();
        } else {
            $this->setAuthenticationRole($role);
        }

        $this->assertSame(
            $expectedForRole,
            $accessService->canAccessAction($controller, $action),
            sprintf(
                'Контроллер: %s, Экшн: %s, Роль: %s; Доступ: %s; Ожидали: %s',
                $controller,
                $action,
                $role,
                $this->getMessage(!$expectedForRole),
                $this->getMessage($expectedForRole)
            )
        );
    }
}
