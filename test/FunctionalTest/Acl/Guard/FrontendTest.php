<?php
namespace FunctionalTest\Acl\Guard;

/**
 * Class FrontendTest
 *
 * @author Shahnovsky Alex
 */
class FrontendTest extends TestCase
{
    /**
     * @return array
     */
    public function getIsAllowed()
    {
        return [
            'home' => [
                'controller' => \Application\Controller\IndexController::class,
                'action' => 'index',
                'expected' => [
                    '*' => true,
                ],
            ],
        ];
    }
}
