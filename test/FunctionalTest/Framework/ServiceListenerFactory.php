<?php
namespace FunctionalTest\Framework;

/**
 * Class ServiceListenerFactory
 *
 * @author Shahnovsky Alex
 */
class ServiceListenerFactory extends \Zend\Mvc\Service\ServiceListenerFactory
{

    public function __construct()
    {
        $this->defaultServiceConfig['factories']['Router'] = 'FunctionalTest\Framework\RouterFactory';
        $this->defaultServiceConfig['factories']['ViewManager'] = 'FunctionalTest\Framework\ViewManagerFactory';
        $this->defaultServiceConfig['factories']['Request'] = 'FunctionalTest\Framework\RequestFactory';
        $this->defaultServiceConfig['factories']['Response'] = 'FunctionalTest\Framework\ResponseFactory';
    }
}
