<?php
namespace FunctionalTest\Framework;

use Closure;
use FunctionalTest\Framework\InstanceMarker\InstanceMarkerInterface;
use FunctionalTest\TestCase;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;
use Zend\InputFilter\InputInterface;

/**
 * Class InputFilterBuilderTestCase
 *
 * @author Shahnovsky Alex
 */
abstract class InputFilterBuilderTestCase extends TestCase
{
    /**
     * @return string
     */
    abstract protected function getBuilderName();

    /**
     * @return BuilderAbstract
     */
    protected function getBuilder()
    {
        $builder = $this->getService($this->getBuilderName());
        $builder->init();

        return $builder;
    }

    /**
     * @param InputInterface[] $inputs
     *
     * @return array
     */
    protected function getExistingFilters(array $inputs)
    {
        $result = [];
        foreach ($inputs as $input) {
            /** @var InputInterface $input */
            if ($input instanceof InputInterface) {
                $result[] = $input->getName();
            } else {
                continue;
            }
        }

        return $result;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    abstract public function dataValid();

    /**
     * @dataProvider dataValid
     * @group        inputFilterValid
     *
     * @param      $data
     * @param      $expected
     * @param null $fixtures
     * @param null|array $context
     *
     * @throws \Exception
     */
    public function testValid($data, $expected, $fixtures = null, array $context = null)
    {
        if (!is_null($fixtures)) {
            $this->loadFixtures($fixtures);
        }

        $this->setAuthenticationRole('admin');
        $filter = $this->getBuilder()->getInputFilterBuilder()->createFilter();
        $filter->setData($data);
        $filter->isValid($context);
        $filteredData = $filter->getValues();
        $this->assertCount(
            count($expected),
            $filteredData,
            'Ключи всех данных: '.print_r(array_keys($filteredData), true)
        );
        $this->assertRecursiveValues($expected, $filteredData);
        $this->assertTrue($filter->isValid($context), "Данные не валидны\n\n".print_r($filter->getMessages(), true));
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    abstract public function dataNotValid();

    /**
     * @dataProvider dataNotValid
     *
     * @param       $data
     * @param       $messageKeys
     * @param array $context
     * @param array $fixtures
     */
    public function testFalse($data, $messageKeys, array $context = null, array $fixtures = null)
    {
        if (is_null($data) && is_null($messageKeys)) {
            $this->markTestSkipped('Нет данных');
        }

        if (!is_null($fixtures)) {
            $this->loadFixtures($fixtures);
        }

        $filter = $this->getBuilder()->getInputFilterBuilder()->createFilter();
        $filter->setData($data);

        $this->assertFalse($filter->isValid($context), 'Данные валидны');
        $this->assertRecursiveKeys(
            $messageKeys,
            $filter->getMessages(),
            $this->createMessage($messageKeys, $filter->getMessages())
        );
    }

    protected function createMessage($expected, $actual)
    {
        return "Ожтдаемый:\n".print_r($expected, true)."\n\n\nРеальный:\n ".print_r($actual, true);
    }

    protected function assertRecursiveValues($expected, $filteredData)
    {
        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $filteredData);
            if ($value instanceof InstanceMarkerInterface) {
                $value->apply($this, $filteredData[$key], sprintf('Значение ключа %s не совпали', $key));
            } elseif (is_array($value)) {
                $this->assertRecursiveValues(
                    $value,
                    $filteredData[$key]
                );
            } else {
                $this->assertSame($value, $filteredData[$key], sprintf('Значение ключа %s не совпали', $key));
            }
        }
    }

    protected function assertRecursiveKeys($expected, $filtered, $message)
    {
        $this->assertCount(count($expected), $filtered, "Не совпадает количество.\n\n".$message);
        foreach ($expected as $key => $value) {
            if (is_array($value)) {
                $this->assertArrayHasKey($key, $filtered, $message);
                $this->assertRecursiveKeys($value, $filtered[$key], $message);
            } else {
                $this->assertArrayHasKey($value, $filtered, $message);
            }
        }
    }

    /**
     * @param          $name
     * @param callable $selector
     *
     * @throws \Exception
     * @return \Zend\Filter\FilterInterface
     */
    protected function findFilter($name, Closure $selector)
    {
        $filter = $this->getBuilder()->getInputFilterBuilder()->createFilter();
        $found = null;
        foreach ($filter->get($name)->getFilterChain()->getFilters()->toArray('EXTR_DATA') as $filter) {
            if ($selector($filter)) {
                $found = $filter;
                break;
            }
        }

        if (is_null($found)) {
            throw new \Exception(sprintf('Filter "%s" not found by closure', $name));
        }

        return $found;
    }
}
