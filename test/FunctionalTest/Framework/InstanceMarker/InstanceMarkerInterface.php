<?php

namespace FunctionalTest\Framework\InstanceMarker;

use PHPUnit\Framework\TestCase;

/**
 * Interface InstanceMarkerInterface
 *
 * @author Shahnovsky Alex
 */
interface InstanceMarkerInterface
{
    public function apply(TestCase $phpunit, $value, $message);
}
