<?php
namespace FunctionalTest\Framework\Authentication;

use Application\Service\Auth\AuthenticationService;
use CirclicalUser\Exception\NoSuchUserException;
use CirclicalUser\Provider\UserInterface;

/**
 * Class AuthService
 *
 * @author Shahnovsky Alex
 */
class AuthService extends AuthenticationService
{
    /**
     * Passed in by a successful form submission, should set proper auth cookies if the identity verifies.
     * The login should work with both username, and email address.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     * @throws NoSuchUserException Thrown when the user can't be identified
     */
    public function authenticateByEntity(UserInterface $user)
    {
        $auth = $this->getAuthProvider()->findByUserId($user->getId());

        if (!$auth) {
            throw new NoSuchUserException();
        }
        $reflection = new \ReflectionClass(\CirclicalUser\Service\AuthenticationService::class);

        $this->callPrivateMethod($reflection, $this, 'resetAuthenticationKey', $auth);
        $this->callPrivateMethod($reflection, $this, 'setIdentity', $user);

        return $user;
    }

    /**
     * Logout.  Reset the user authentication key, and delete all cookies.
     */
    public function clearIdentity()
    {
        $reflection = new \ReflectionClass(\CirclicalUser\Service\AuthenticationService::class);
        $property = $reflection->getProperty('identity');
        $property->setAccessible(true);
        $property->setValue($this, null);
        $property->setAccessible(false);
    }
}
