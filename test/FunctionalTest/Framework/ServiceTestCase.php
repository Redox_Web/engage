<?php
namespace FunctionalTest\Framework;

use Closure;
use Doctrine\ORM\Query;
use FunctionalTest\Framework\InstanceMarker\InstanceMarkerInterface;
use FunctionalTest\TestCase as AppTestCase;
use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\Exception\ValidationException;

/**
 * Class ServiceTestCase
 *
 * @author Shahnovsky Alex
 */
abstract class ServiceTestCase extends AppTestCase
{
    /**
     * Возвращает имя сервиса в ServiceManager
     *
     * @return string
     */
    abstract protected function getServiceName();

    /**
     * Возвращает тип сервиса (имя класса)
     *
     * @return string
     */
    abstract protected function getServiceType();

    /**
     * Проверка на регистрацию сервиса
     */
    public function testSuccessCreate()
    {
        $this->assertInstanceOf($this->getServiceType(), $this->getService($this->getServiceName()));
    }

    /**
     * @param string $entityName
     * @param int    $beforeCount
     */
    protected function assertSingleRecordCreated($entityName, $beforeCount = 0)
    {
        $result = $this->executeCount($entityName);
        $expected = 1 + $beforeCount;
        $this->assertSame(
            $expected,
            $result,
            sprintf('Найдено %d записей, ожидалось: %d  в %s', $expected, $result, $entityName)
        );
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'data' => [....],
     *      'fixtures' => array()
     *      'entityName' => string,
     *      'callback' => Closure|null
     *      'role' => array|string|null
     * ]
     *
     * @return array
     */
    abstract public function dataCreateSuccess();

    /**
     * @dataProvider dataCreateSuccess
     * @group        createSuccess
     *
     * @param array   $data
     * @param array   $fixtures
     * @param string  $entityName
     * @param Closure $callback
     * @param string    $role
     */
    public function testCreateSuccess($data, $fixtures, $entityName, Closure $callback = null, $role = 'admin')
    {
        $this->assertCreateSuccess('create', $data, $fixtures, $entityName, $callback, $role);
    }

    /**
     * Выполнить ассреты для создания сущности
     *
     * @param         $methodName
     * @param         $data
     * @param         $fixtures
     * @param         $entityName
     * @param Closure $callback
     * @param null|string    $role
     *
     * @throws \Exception
     */
    protected function assertCreateSuccess(
        $methodName,
        $data,
        $fixtures,
        $entityName,
        Closure $callback = null,
        $role = null
    ) {
        $this->loadFixtures($fixtures);
        $beforeRecordsCount = $this->executeCount($entityName);
        $this->setAuthenticationRole($role);
        $service = $this->getService($this->getServiceName());
        try {
            $result = $service->$methodName($data);
        } catch (ValidationException $e) {
            $this->fail('Сущность не создана. Ошибка валидации: '.var_export($e->getValidationMessages(), true));
        }
        $this->getEntityManager()->refresh($result);
        $this->assertInstanceOf($entityName, $result);
        $this->assertSingleRecordCreated($entityName, $beforeRecordsCount);
        if ($callback) {
            $callback($result, $this);
        }
    }

    /**
     * @param $entityName
     *
     * @return int
     */
    protected function executeCount($entityName)
    {
        $result = current(
            $this->executeQuery("SELECT COUNT(*) as count FROM " . $this->getTableName($entityName), array())
        );

        return (int) $result['count'];
    }

    /**
     * dataProvider for testCreateFailed
     *
     * Format: [
     *  'data' => array(invalid data),
     *  'fixtures' => array(),
     *  'messageKeys' => array(список всех ожидаемых ключей)
     * ]
     *
     * @return array
     */
    abstract public function dataCreateFailed();

    /**
     * @dataProvider dataCreateFailed
     * @group        createFailed
     *
     * @param array  $data
     * @param array  $fixtures
     * @param array  $messageKeys
     * @param string $role
     *
     * @throws \Exception
     */
    public function testCreateFailed($data, $fixtures, $messageKeys, $role = 'admin')
    {
        if (is_null($data) && is_null($fixtures) && is_null($messageKeys)) {
            $this->markTestSkipped('Нет данных');
        }

        $this->loadFixtures($fixtures);
        $this->setAuthenticationRole($role);
        $service          = $this->getService($this->getServiceName());
        $validationFailed = false;
        try {
            $service->create($data);
        } catch (ValidationException $e) {
            $validationFailed = true;
            $this->assertErrorKeysValid(
                $messageKeys,
                $e->getValidationMessages(),
                $this->createMessage($messageKeys, $e->getValidationMessages())
            );
        }

        $this->assertTrue($validationFailed, 'Успешно создан Entity, вместо ошибки');

    }

    /**
     * @param array $expected
     * @param array $messages
     */
    protected function assertErrorKeysValid(array $expected, array $messages, $message)
    {
        foreach ($expected as $messageKey => $messageValue) {
            if (is_array($messageValue)) {
                $this->assertArrayHasKey(
                    $messageKey,
                    $messages,
                    var_export($messages, true).var_export($messageValue, true)
                );
                $this->assertErrorKeysValid($messageValue, $messages[$messageKey], $message);
            } else {
                $this->assertArrayHasKey($messageValue, $messages, var_export($messages, true));
            }
        }

        $this->assertCount(count($expected), $messages, $message);
    }

    protected function createMessage($expected, $actual)
    {
        return "Ожтдаемый:\n" . print_r($expected, true) . "\n\n\nРеальный:\n " . print_r($actual, true);
    }

    /**
     * dataProvider For testUpdateSuccess
     *
     * Format: [
     *  'id' => int,
     *  'data' => array(...),
     *  'entityName' => 'имя entity',
     *  'fixtureName' => array(список классов fixtures),
     *  'expectedChanges' => array(список колонок в бд и их значения)
     *  'callback' => function($updateEntity, $this) {}  - если нужны доп проверки
     * ]
     *
     * @return array
     */
    abstract public function dataUpdateSuccess();

    /**
     * @dataProvider dataUpdateSuccess
     * @group        updateSuccess
     *
     * @param int              $identity
     * @param array            $data
     * @param string           $entityName
     * @param string|array     $fixtureName
     * @param                  $expectedChanges
     * @param callable|Closure $callback
     * @param array           $auth
     *
     * @throws \Exception
     */
    public function testUpdateSuccess(
        $identity,
        $data,
        $entityName,
        $fixtureName,
        $expectedChanges,
        Closure $callback = null,
        $auth = ['role' => 'admin']//['user' => 1]
    ) {
        $this->assertUpdateSuccess($identity, $data, $entityName, $fixtureName, $expectedChanges, $callback, $auth);
    }

    /**
     * @param       $entityName
     * @param       $identity
     * @param array $data
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\Mapping\MappingException
     * @throws \Exception
     * @return array
     */
    protected function getRowMergedWithData($entityName, $identity, $data = array())
    {
        $idField = $this->getIdField($entityName);
        $row = $this->executeQuery(
            "Select * from ".$this->getTableName($entityName)." where ".$idField.' = ?',
            array($identity)
        );
        $row = reset($row);
        $this->assertInternalType('array', $row, "Нет записей удовлетворяющих запросу [$idField=$identity]");
        return array_merge($row, $data);
    }

    /**
     * @param $entityName
     *
     * @return string
     * @throws \Exception
     */
    protected function getIdField($entityName)
    {
        $metadata = $this->getEntityManager()->getClassMetadata($entityName);
        if (count($metadata->getIdentifier()) > 1) {
            throw new \Exception('Данный тест не может проверить сложные id. Нужно реализовать');
        }

        return $metadata->getColumnName(current($metadata->getIdentifier()));
    }

    /**
     * @param $entityName
     *
     * @return string
     */
    protected function getTableName($entityName)
    {
        $metadata = $this->getEntityManager()->getClassMetadata($entityName);
        return $metadata->getTableName();
    }

    /**
     * @param $identity
     * @param $entityName
     *
     * @return null|object
     */
    protected function getEntity($identity, $entityName)
    {
        return $this->getRepository($entityName)->find($identity);
    }

    protected function assertRecursiveValues(array $updatedData, array $expected)
    {
        foreach ($expected as $key => $value) {
            $this->assertArrayHasKey($key, $updatedData);
            if ($value instanceof InstanceMarkerInterface) {
                $value->apply($this, $updatedData[$key], sprintf('Значение ключа %s не совпали', $key));
            } elseif (is_array($value)) {
                $this->assertRecursiveValues(
                    $updatedData[$key],
                    $value
                );
            } else {
                $this->assertSame($value, $updatedData[$key], sprintf('Значение ключа %s не совпали', $key));
            }
        }

        $this->assertCount(count($expected), $updatedData);
    }

    /**
     * dataProvider For testDeleteSuccess
     *
     * Format: [
     *  'identity' => int,
     *  'fixtures' => array(список классов fixtures),
     *  'asserts' => Closure
     * ]
     *
     * @return array
     */
    abstract public function dataDeleteSuccess();

    /**
     * @dataProvider dataDeleteSuccess
     * @group        deleteSuccess
     *
     * @param int          $identity
     * @param string|array $fixtures
     * @param Closure      $callback
     * @param string       $role
     *
     * @throws \Exception
     */
    public function testDeleteSuccess($identity, $fixtures, Closure $callback = null, $role = 'admin')
    {
        $this->loadFixtures($fixtures);
        $this->setAuthenticationRole($role);
        /** @var CrudInterface $service */
        $service = $this->getService($this->getServiceName());
        $entity = $service->find($identity);
        $delete = $service->delete($identity);
        $this->assertTrue($delete);
        if ($callback) {
            $callback($entity, $this);
        }
    }

    /**
     * @param $identity
     * @param $data
     * @param $entityName
     * @param $fixtureName
     * @param $expectedChanges
     * @param Closure $callback
     * @param array $auth
     * @param string $method
     *
     * @throws \Exception
     */
    protected function assertUpdateSuccess(
        $identity,
        $data,
        $entityName,
        $fixtureName,
        $expectedChanges,
        Closure $callback = null,
        $auth = ['role' => 'admin'],
        $method = 'update'
    ) {
        if ($identity === null) {
            $this->markTestSkipped($data);
        }

        $this->loadFixtures($fixtureName);

        $expected = $this->getRowMergedWithData($entityName, $identity, $expectedChanges);

        $service = $this->getService($this->getServiceName());
        $this->authenticate($auth);

        try {
            $updatedEntity = $service->$method($identity, $data);
        } catch (ValidationException $e) {
            $this->fail(var_export($e->getValidationMessages(), true));
        }

        $this->getEntityManager()->refresh($updatedEntity);
        $updated = $this->getRowMergedWithData($entityName, $identity, array());

        $this->assertRecursiveValues($updated, $expected);

        if ($callback) {
            $callback($updatedEntity, $this);
        }
    }
}
