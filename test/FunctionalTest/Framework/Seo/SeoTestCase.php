<?php
namespace FunctionalTest\Framework\Seo;

use FunctionalTest\TestCase as AppTestCase;
use RedoxWeb\WTL\Dependency\RootAble\RootAbleInterface;
use Seo\Factory\Service\ProviderFactory;
use Seo\Manager\SeoManager;
use Seo\Service\Provider;

/**
 * Class SeoTestCase
 *
 * @author Shahnovsky Alex
 */
abstract class SeoTestCase extends AppTestCase
{
    /**
     * @return string
     */
    abstract protected function getProviderAlias();

    /**
     * @return SeoManager
     */
    protected function getSeoManager()
    {
        return $this->getService('seoManager');
    }

    /**
     * @param string $expected
     * @param Provider $provider
     * @param string $seoKey
     */
    protected function assertSeoProperty($expected, Provider $provider, $seoKey)
    {
        $methodName = 'get'.$seoKey;

        $this->assertSame($expected, $provider->$methodName());
    }

    /**
     * Test Provider creation
     */
    public function testCreateProvider()
    {
        $provider = $this->getSeoManager()->get(ProviderFactory::PREFIX.$this->getProviderAlias());

        $this->assertInstanceOf(Provider::class, $provider);
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'expected' => array(),
     *      'object' => RootAbleInterface, Object transfered to seo system.
     *      'extra' => array, Some extra data that can be used in seo system.
     *      'fixtures' => array,
     * ]
     *
     * @return array
     */
    abstract public function dataGetSeoData();

    /**
     * @dataProvider dataGetSeoData
     *
     * @param array $expected
     * @param null|RootAbleInterface $object
     * @param array $extra
     * @param array $fixtures
     */
    public function testGetSeoData(array $expected, $object = null, $extra = [], $fixtures = [])
    {
        $this->loadFixtures($fixtures);
        $provider = $this->getSeoManager()->get(
            ProviderFactory::PREFIX.$this->getProviderAlias(),
            [
                'object' => $object,
                'extra' => $extra,
            ]
        );

        foreach ($expected as $key => $value) {
            $this->assertSeoProperty($value, $provider, $key);
        }
    }
}
