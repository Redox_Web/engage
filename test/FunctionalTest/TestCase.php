<?php
namespace FunctionalTest;

use Application\Service\User\User;
use CirclicalUser\Mapper\RoleMapper;
use CirclicalUser\Service\AccessService;
use CirclicalUser\Service\AuthenticationService;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use FunctionalTest\Framework\Authentication\UserMock;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use Zend\Config\Config;
use Zend\Mvc\Application;

/**
 * Class TestCase
 *
 * @author Shahnovsky Alex
 */
abstract class TestCase extends \PHPUnit\Framework\TestCase
{

    protected static $schemaCreated = false;

    protected static $copyDbName = '';

    protected $backupDataBase = '';

    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @var Application
     */
    protected $application;

    protected $currentTimeZone;

    protected function setUp()
    {
        $this->iniSet('memory_limit', '512M');
        global $createDb, $generateProxy;
        parent::setUp();
        $this->currentTimeZone = date_default_timezone_get();
        date_default_timezone_set('Europe/Chisinau'); //Любая, но не UTC

        $this->application = $this->initApplication();

        if (!$this->hasDatabaseCopy()) {
            if ($generateProxy !== false) {
                $this->generateProxy();
            }

            if ($createDb !== false) {
                $this->createSchema();
                $this->loadFixtures(
                    [
                        \FunctionalTest\Fixture\Roles::class,
                        \FunctionalTest\Fixture\Auth::class,
                    ]
                );
            }
            $this->createCopyDatabase();
        }

        $this->getEntityManager()->clear();
    }

    /**
     * @param Config $config
     *
     * @return Config
     */
    protected function prepareToParallel(Config $config)
    {
        $env = getenv('DB_CONFIG_ENV');
        if (empty($env)) {
            return $config;
        }

        $parallelReadFrom = 'test/config/'.$env.'/autoload/{,*.}test.php';

        $toMerge = new Config(
            [
                'module_listener_options' => [
                    'config_glob_paths' => [
                        'parallel' => $parallelReadFrom
                    ]
                ]
            ]
        );

        $config->merge($toMerge);

        return $config;
    }

    /**
     * @return Application
     */
    protected function getApplication()
    {
        return $this->application;
    }

    protected function generateProxy()
    {
        $entityManager = $this->getEntityManager();
        $destinationPath = $entityManager->getConfiguration()->getProxyDir();
        $metadata = $entityManager->getMetadataFactory()->getAllMetadata();
        $entityManager->getProxyFactory()->generateProxyClasses($metadata, $destinationPath);
    }

    protected function hasDatabaseCopy()
    {
        return !empty(self::$copyDbName);
    }

    protected function createCopyDatabase()
    {
        $connection = $this->getEntityManager()->getConnection();
        if ($connection->getDriver()->getName() == 'pdo_sqlite') {
            $currentDb = $connection->getDatabase();
            self::$copyDbName = $currentDb.'.copy';
            copy($currentDb, self::$copyDbName);
        }
    }

    protected function reloadDatabase()
    {
        $connection = $this->getEntityManager()->getConnection();
        if ($connection->getDriver()->getName() == 'pdo_sqlite') {
            $currentDb = $connection->getDatabase();
            copy(self::$copyDbName, $currentDb);
        }
    }

    protected function getBackupDataBase()
    {
        if (empty($this->backupDataBase)) {
            $connection = $this->getEntityManager()->getConnection();
            $currentDb = $connection->getDatabase();
            return $currentDb.'.backup';
        }
        return $this->backupDataBase;
    }

    protected function createBackup()
    {
        $connection = $this->getEntityManager()->getConnection();
        if ($connection->getDriver()->getName() == 'pdo_sqlite') {
            $currentDb = $connection->getDatabase();
            copy($currentDb, $this->getBackupDataBase());
        }
    }

    /**
     * @param $query
     * @param $params
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function executeQuery($query, $params)
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery($query, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @return void
     */
    protected function tearDown()
    {
        if ($this->application !== null) {
            $this->createBackup();
            $this->reloadDatabase();
        }
        date_default_timezone_set($this->currentTimeZone);
        parent::tearDown();
    }

    /**
     * @param $name
     *
     * @return object|array
     */
    protected function getService($name)
    {
        return $this->application->getServiceManager()->get($name);
    }

    /**
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    protected function createSchema($force = false)
    {
        if ($force || !self::$schemaCreated) {
            $this->dropSchema();
            $this->getSchemaTools()->createSchema($this->getAllMetaData());
            self::$schemaCreated = true;
        }
    }

    /**
     * @return void
     */
    protected function dropSchema()
    {
        self::$schemaCreated = false;
        $this->getSchemaTools()->dropSchema($this->getAllMetaData());
    }

    /**
     * Очищает б.д. от данных
     */
    protected function purgeDatabase()
    {
        $purger = new ORMPurger($this->getEntityManager());
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
    }

    /**
     * @return SchemaTool
     */
    protected function getSchemaTools()
    {
        if (is_null($this->schemaTool)) {
            $this->schemaTool = new SchemaTool($this->getEntityManager());
        }

        return $this->schemaTool;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getService('Doctrine\ORM\EntityManager');
    }

    /**
     * @return array
     */
    protected function getAllMetaData()
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->getMetadataFactory()->getAllMetadata();
    }

    /**
     * @param string $entityName
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository($entityName)
    {
        return $this->getEntityManager()->getRepository($entityName);
    }

    /**
     * @param string|array $fixtureName
     *
     * @param bool $append
     *
     * @throws \Exception
     */
    protected function loadFixtures($fixtureName, $append = true)
    {
        $purger = new ORMPurger();
        $loader = new Loader();
        if (is_string($fixtureName)) {
            $loader->loadFromDirectory($fixtureName);
        } elseif (is_array($fixtureName)) {
            foreach ($fixtureName as $fixture) {
                $fixture = new $fixture();
                if ($fixture instanceof EntityManagerAwareInterface) {
                    $fixture->setEntityManager($this->getEntityManager());
                }
                /** @var FixtureInterface $fixture */
                $loader->addFixture($fixture);
            }
        } else {
            throw new \Exception(
                sprintf('Fixtures must be array or string. %s given', $fixtureName)
            );
        }

        $executor = new ORMExecutor($this->getEntityManager(), $purger);
        $executor->execute($loader->getFixtures(), $append);
    }

    /**
     * @return Application
     */
    protected function initApplication()
    {
        $appConfig = new Config(require 'config/application.config.php', true);
        $testConfig = new Config(require 'test/config/test.config.php', true);
        $testConfig = $this->prepareToParallel($testConfig);

        $appConfig->merge($testConfig);
        return Application::init($appConfig->toArray());
    }

    /**
     * Задать роль от которой запущен тест
     * @param string|string[] $roles
     */
    protected function setAuthenticationRole($roles)
    {
        if (!is_array($roles)) {
            $roles = (array) $roles;
        }
        $roleProvider = $this->getService(RoleMapper::class);

        $user = new UserMock($roleProvider, $roles);
        $this->setAuthenticatedUser($user);
    }

    /**
     * Указать явно пользователя, от котрого запущен тест
     * @param $user
     */
    protected function setAuthenticatedUser($user)
    {
        /** @var \Application\Service\Auth\AuthenticationService $authService */
        $authService = $this->getService(AuthenticationService::class);

        /** @var AccessService $accessService */
        $accessService = $this->getService(AccessService::class);
        $accessService->setUser($user);

        $authService->authenticateByEntity($user);
    }

    /**
     * Разлогинить юзера
     */
    protected function logout()
    {
        /** @var \Application\Service\Auth\AuthenticationService $authService */
        $authService = $this->getService(AuthenticationService::class);
        $authService->clearIdentity();

        /** @var AccessService $accessService */
        $accessService = $this->getService(AccessService::class);
        $reflection = new \ReflectionClass(AccessService::class);
        $property = $reflection->getProperty('user');
        $property->setAccessible(true);
        $property->setValue($accessService, null);
        $property->setAccessible(false);
    }

    /**
     * Authenticating User or UserMock with roles
     * @param array $auth => array(
     *  'role' => ('value' OR ['value1', 'value2']) OR
     *  'user' => 'userIdValue'
     *  )
     * @throws \Exception
     */
    protected function authenticate(array $auth)
    {
        if (array_key_exists('role', $auth) && array_key_exists('user', $auth)) {
            throw new \Exception('Invalid array format for authentication given');
        }

        if (array_key_exists('role', $auth)) {
            $this->setAuthenticationRole($auth['role']);
        } elseif (array_key_exists('user', $auth)) {
            $this->setAuthenticatedUser(
                $this->getService(User::class)->find($auth['user'])
            );
        } else {
            throw new \Exception('Invalid array format for authentication given');
        }
    }
}
