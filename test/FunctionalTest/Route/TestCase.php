<?php
namespace FunctionalTest\Route;

use FunctionalTest\TestCase as AppTestCase;
use Zend\Http\Request;
use Zend\Router\RouteStackInterface;

/**
 * Class TestCase
 *
 * @author Shahnovsky Alex
 */
abstract class TestCase extends AppTestCase
{
    /**
     * @var RouteStackInterface
     */
    protected $router;

    public function setUp()
    {
        parent::setUp();
        $this->router = $this->application->getServiceManager()->get('Router');
    }

    /**
     * @return RouteStackInterface
     */
    protected function getRouter()
    {
        return $this->router;
    }

    protected function createRequest($requestString)
    {
        return Request::fromString($requestString);
    }

    /**
     * example:
     *
     * array(
     *   'request' => 'GET http://www.engage.local/',
     *   'routeName' => 'front-end/index',
     *   'params' => array(
     *     'language' => 'ru'
     *   )
     * ),
     *
     *
     * @return array
     */
    abstract public function dataMatchingRoutes();

    /**
     * array(
     *   'request' => 'GET http://www.engage.local/'
     * ),
     * @return array
     */
    abstract public function dataNotMatchingRoutes();

    /**
     * Какого namespace будут контроллеры
     *
     * @return string
     */
    abstract public function getNamespace();

    /**
     * @medium
     * @dataProvider dataMatchingRoutes
     */
    public function testMatchingRoutes($request, $routeName, $params)
    {
        $params['__NAMESPACE__'] = $this->getNamespace();
        $result = $this->getRouter()->match($this->createRequest($request));
        $this->assertInstanceOf('Zend\Router\RouteMatch', $result, 'Не найдено совпадений');
        $this->assertSame($routeName, $result->getMatchedRouteName(), 'Неверное совпадение');
        $this->assertEquals(
            $params,
            $result->getParams(),
            "Не совпали параметры \nОжидаемые\\n\n"
            . print_r($params, true)
            . "\nРеальные:\n" . print_r($result->getParams(), true)
        );
    }

    /**
     * @dataProvider dataNotMatchingRoutes
     * @param string $request
     */
    public function testNotMatchingRoutes($request)
    {
        $result = $this->getRouter()->match($this->createRequest($request));
        $this->assertNull($result, sprintf('Matched route from request "%s"', $request));
    }
}
