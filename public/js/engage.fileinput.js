;(function($){
    $.fileInputEngage = {
        initAvatar: function(options){
            var settings = $.extend({
                    selector : "",
                    activeImgSrc: "",
                    caption: "",
                    size:""
                },
                options || {});

            if(settings.selector!=""){
                var initialPreviewArr = [];
                var initialPreviewConfigArr = [];
                var id = settings.activeImgSrc;

                if(settings.activeImgSrc!=""){
                    initialPreviewArr = [
                        "<img style='height:160px' src='"+settings.activeImgSrc+"'>",
                    ];
                    initialPreviewConfigArr = [
                        {caption: settings.caption, width: "300px", url: settings.activeImgSrc},
                    ]
                }
                $(settings.selector).fileinput({
                    uploadUrl: '/file-upload-batch/2',
                    uploadAsync: false,
                    autoReplace: true,
                    maxFileCount: 1,
                    //allowedFileExtensions: ["jpg", "png", "gif"],
                    overwriteInitial: true,
                    initialPreview: initialPreviewArr,
                    initialPreviewConfig: initialPreviewConfigArr
                });
            }
        },
        initBackend: function(options){
            var settings = $.extend({
                    selector : "",
                    activeImgSrc: "",
                    caption: "",
                    size:""
                },
                options || {});

            if(settings.selector!=""){
                var initialPreviewArr = [];
                var initialPreviewConfigArr = [];
                var id = settings.activeImgSrc;

                if(settings.activeImgSrc!=""){
                    initialPreviewArr = [
                        "<img style='height:160px' src='"+settings.activeImgSrc+"'>",
                    ];
                    initialPreviewConfigArr = [
                        {caption: settings.caption, width: "300px", url: settings.activeImgSrc},
                    ]
                }
                $(settings.selector).fileinput({
                    uploadUrl: '/file-upload-batch/2',
                    uploadAsync: false,
                    autoReplace: true,
                    maxFileCount: 1,
                    //allowedFileExtensions: ["jpg", "png", "gif"],
                    overwriteInitial: true,
                    initialPreview: initialPreviewArr,
                    initialPreviewConfig: initialPreviewConfigArr
                });
            }
        },
        initPersonalOfficeProfile: function(options){
            var settings = $.extend({
                    selector : "",
                    valueInputSelector: "",
                    activeImgSrc: "",
                    caption: "",
                    size:"",
                    previewArr : []
                },
                options || {});

            if(settings.selector!=""){

                var initialPreviewArr = [];
                var initialPreviewConfigArr = [];
                var src = settings.previewArr.src || "";
                var size = settings.previewArr.size || "";
                var mimeType = settings.previewArr.mimeType || "";
                var caption = settings.previewArr.caption || "";
                var initialPreview = settings.previewArr.initialPreview || "";
                var fileInputParams = {
                    initialPreviewAsData: true,
                    uploadUrl: '/file-upload-batch/2',
                    uploadAsync: false,
                    autoReplace: true,
                    maxFileCount: 1,
                    overwriteInitial: true,
                    allowedPreviewTypes: ['image', 'html', 'text', 'video', 'audio', 'flash'],
                    maxFileSize: 200000000,
                    // customLayoutTags : {
                    //     '{download}': '<button type="button" title="Download file" data-url="<?php echo $model->$attribute; ?>" class="kv-file-download btn btn-xs btn-default"><i class="glyphicon glyphicon-download"></i></button>',
                    //     '{download-bottom}' : '<button type="button" title="Download file" data-url="<?php echo $model->$attribute; ?>" class="btn btn-default kv-file-download"><i class="glyphicon glyphicon-download"></i></button>',
                    //     '{zoom-bottom}' : '<?php echo $zoom_preview; ?>'
                    // },
                    // layoutTemplates: {actions: actionsT, main1: main1T,  size:'<samp>{sizeText}</samp>'}, // footer: footerT, progress: progressT
                    // initialCaption: "<?php echo $caption; ?>",
                    previewClass: ""
                  //  allowedFileExtensions : ['mp4', 'avi']
                }

                var initialPreviewConfig = [];
                if(src != ""){
                    var defObj = $.fn.fileinput.defaults.fileTypeSettings;
                    var type = "object";
                    for(var fnct in defObj){
                        if(defObj[fnct](mimeType, src)){
                            type = fnct;
                            break;
                        }
                    }
                    initialPreviewConfig = [{type:type, filetype: mimeType, caption: caption, size: size, width: "400px"}];
                    fileInputParams["initialPreview"] = [initialPreview];
                    fileInputParams["initialPreviewConfig"] = initialPreviewConfig;
                }

                $(settings.selector).fileinput(fileInputParams).on('filecleared', function(event) {
                    $(settings.valueInputSelector).val("");
                });
            }
        }
    }
})(jQuery);