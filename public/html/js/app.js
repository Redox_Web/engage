/**
 *  Name: Engage
 *  Author: newmexicano@gmail.com
 */
$(function() {
    FastClick.attach(document.body);
});

angular.module('engage', []).controller('mainController', function ($scope) {
    $(document).ready(function(){
        mobileMenu();
        addQuestion();
        $('select').material_select();
		$('.modal').modal();
        $(".dropdown-button").dropdown();

		$( "#click-me" ).click(function() {
			$(this).toggleClass('opened');
			var $sectionToggle = $( "#show-toggle" );
			$sectionToggle.slideToggle( "slow");
			console.log('complete');
			var elementClick = $($sectionToggle);
			var destination = ($(elementClick).offset().top)-10;
			$('html,body').animate( { scrollTop: destination }, 1000 );
		});
		$( ".answers-block-outer .show-answer" ).click(function() {
			console.log('answer opened');
			$(this).toggleClass('clicked');
			$(this).parent('').toggleClass('button-clicked');
			$(this).closest('.reviews-block').find('.answer-block').slideToggle( "slow");
		});

		$( ".answers-block-outer .delete-answer" ).click(function() {
			console.log('answer deleted');
			$(this).toggleClass('clicked');
			$(this).closest('.page-block').fadeOut('300');
		});

		$( "#showReviews" ).click(function() {
			var $sectionToggle = $( "#moreReviews" );
			$sectionToggle.slideToggle( "slow");
			console.log('complete');
			var elementClick = $($sectionToggle);
			var destination = ($(elementClick).offset().top)-10;
			$('html,body').animate( { scrollTop: destination }, 1000 );
			$(this).fadeOut('100');
		});

        $('.owl-courses').owlCarousel({
            loop:true,
            dots: false,
            margin:29,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                767: {
                    items:3
                },
                1000:{
                    items:4
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
        });
        $('#latestNews').owlCarousel({
            loop:false,
            dots: false,
            margin:30,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                767: {
                    items:3
                },
                1000:{
                    items:3
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
        });

		$(function(){

			$("#mb-tabs").on("click", ".tab-item", function(){

				var tabs = $("#mb-tabs .tab-item"),
					cont = $("#mb-tabs .tab-cont");

				// Удаляем классы active
				tabs.removeClass("active");
				cont.removeClass("active");
				// Добавляем классы active
				$(this).addClass("active");
				cont.eq($(this).index()).addClass("active");

				return false;
			});
		});

		countDown();

    });



    function countDown(){
    	if($('#countDownBlock').length > 0){
			var countDownDate = new Date("Dec 3, 2017 15:37:25").getTime(); // Set the date we're counting down to
			var x = setInterval(function() {
				console.log('countDown');
				var now = new Date().getTime();
				var distance = countDownDate - now;
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				document.getElementById("countDownBlock").innerHTML = "<div><div><div>" + days + "</div><div>days</div></div></div><div><div><div>" + hours + "</div><div>hours</div></div></div><div><div><div>"
					+ minutes + "</div><div>minutes</div></div></div><div><div><div>" + seconds + "</div><div>seconds</div></div></div>";

				// If the count down is over, write some text
				if (distance < 0) {
					clearInterval(x);
					document.getElementById("countDownBlock").innerHTML = "EXPIRED";
				}
			}, 1000);
		}
	}

    //mobile menu
    function mobileMenu() {
        $(".open-mobile-menu").click(function(){
            $('.mobile-menu').slideToggle();
        });
    }
    // $(function() {
    //     $('#sortItems').sortable({
    //         placeholder: "ui-state-highlight"
    //     });
    // });
    function addQuestion() {
        var question = '.question-item';
        $('.add-question').click(function (e) {
            e.preventDefault();
            $('.templates '+question).clone().appendTo('#sortItems');
        });
        $('body').on('click', '.remove-question', function (e) {
            e.preventDefault();
            $(this).closest(question).remove();
        });
    }
});