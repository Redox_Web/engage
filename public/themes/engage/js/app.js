/**
 *  Name: Engage
 *  Author: newmexicano@gmail.com
 */
$(function () {
    FastClick.attach(document.body);
});


$(document).ready(function () {
    mobileMenu();
    addQuestion();
    selectTab();
    $('select').material_select();
    $('.modal').modal();
    $(".dropdown-button").dropdown();


    $("#click-me").click(function () {
        $(this).toggleClass('opened');
        var $sectionToggle = $("#show-toggle");
        $sectionToggle.slideToggle("slow");
        console.log('complete');
        var elementClick = $($sectionToggle);
        var destination = ($(elementClick).offset().top) - 10;
        $('html,body').animate({scrollTop: destination}, 1000);
    });
    $('.owl-courses').owlCarousel({
        loop: false,
        dots: false,
        margin: 30,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            767: {
                items: 3
            },
            1000: {
                items: 4
            }
        },
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });
    $('#latestNews').owlCarousel({
        loop: false,
        dots: false,
        margin: 30,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            767: {
                items: 3
            },
            1000: {
                items: 3
            }
        },
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });


    var countDownDate = new Date("Dec 3, 2017 15:37:25").getTime(); // Set the date we're counting down to
    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        window.onload = function what() {
            document.getElementById("countDown").innerHTML = "<div><div><div>" + days + "</div><div>days</div></div></div><div><div><div>" + hours + "</div><div>hours</div></div></div><div><div><div>"
                + minutes + "</div><div>minutes</div></div></div><div><div><div>" + seconds + "</div><div>seconds</div></div></div>";
        };
        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countDown").innerHTML = "EXPIRED";
        }
    }, 1000);
});

//mobile menu
function mobileMenu() {
    $(".open-mobile-menu").click(function () {
        $('.mobile-menu').slideToggle();
    });
}

function addQuestion() {
    var question = '.question-item';
    $('.add-question').click(function (e) {
        e.preventDefault();
        var pos = $('.question-item').length - 1;
        var questions = $('.templates ' + question).clone();

        questions.find(".quizQuestion").attr("name", "quiz[questions]["+ pos +"][question]");
        questions.find(".quizAnswers").attr("name", "quiz[questions]["+ pos +"][answers][]");
        questions.find(".correctAnswer").attr("name", "quiz[questions]["+ pos +"][correctAnswer]");
        questions.find(".correctAnswer").each(function() {
            $newId = $(this).attr("id").replace(/^val\-\d+\-/, "val-" + pos + "-");
            $(this).closest(".pb-1em").find("label").attr("for", $newId);
            $(this).attr("id", $newId);
        });

        questions.appendTo('#sortItems');
    });
    $('body').on('click', '.add-option', function (e) {
        e.preventDefault();
        var pos = $(this).closest('.question-item').find('.items-center').length;
        var lastQuestion = $(this).closest('.question-item').find('.items-center').last().clone();
        $newId = lastQuestion.find(".correctAnswer").attr("id").replace(/\-\d+$/, "-" + pos);

        lastQuestion.find("label").attr("for", $newId);
        lastQuestion.find(".correctAnswer").attr("id", $newId).attr("value", pos);
        lastQuestion.find(".quizAnswers").val("");

        lastQuestion.appendTo($(this).closest(".question-item").find('.options-list'));
    });
    $('body').on('click', '.remove-option', function (e) {
        e.preventDefault();
        $(this).closest('.question-item').find('.items-center').last().remove();
    });
    $('body').on('click', '.remove-question', function (e) {
        e.preventDefault();
        $(this).closest(question).remove();
    });
}

function selectTab(){
    $('.select-tab').on('change', function(e){
        e.stopPropagation();
        var self = $(this);
        var value = self.val();
        $('.select-tab-content').css('display', 'none');
        $('#'+value).css('display', 'block');
    });
}