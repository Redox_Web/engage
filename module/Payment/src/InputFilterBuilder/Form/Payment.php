<?php

namespace Payment\InputFilterBuilder\Form;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class PaymentForm
 * @author nonick <web@nonick.name>
 */
class Payment extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('signed_date_time', true, 'payment.signed.date');
        $this->getInputFilterBuilder()->add('currency', true, 'payment.currency');
        $this->getInputFilterBuilder()->add('transaction_type', true, 'payment.transaction.type');
        $this->getInputFilterBuilder()->add('locale', true, 'payment.locale');
        $this->getInputFilterBuilder()->add('access_key', true, 'payment.access.key');
        $this->getInputFilterBuilder()->add('profile_id', true, 'payment.profile.id');
        $this->getInputFilterBuilder()->add('amount', true, 'base.float');
        $this->getInputFilterBuilder()->add('transaction_uuid', true, 'base.string');
        $this->getInputFilterBuilder()->add('reference_number', true, 'base.digits');
        $this->getInputFilterBuilder()->add('signed_field_names', true, 'base.long_text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
        // TODO: Implement buildInvariant() method.
    }
}
