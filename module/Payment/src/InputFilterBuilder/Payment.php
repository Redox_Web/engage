<?php

namespace Payment\InputFilterBuilder;

use Application\Entity\User\UserAbstract;
use Payment\Entity\PaymentStatus;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Payment
 *
 * @author Topuria Evgheni
 */
class Payment extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'user',
            true,
            null,
            [],
            [
                [
                    'name'    => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => UserAbstract::class
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add(
            'amount',
            true,
            'base.float'
        );

        $this->getInputFilterBuilder()->add('created', false, 'base.datetime');

        $this->getInputFilterBuilder()->add(
            'status',
            true,
            null,
            [],
            [
                [
                    'name'    => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => PaymentStatus::class
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add(
            'dataLog',
            true,
            null,
            [],
            [
                [
                    'name'    => 'IsJson',
                    'options' => [
                        'allowNull' => true,
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add('order', false, 'base.text');
        $this->getInputFilterBuilder()->add('transactionTime', false, 'base.datetime');
        $this->getInputFilterBuilder()->add('transaction', false, 'base.text');
        $this->getInputFilterBuilder()->add('responseMessage', false, 'base.text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}