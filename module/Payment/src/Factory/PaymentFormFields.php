<?php

namespace Payment\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PaymentFormFields
 * @author nonick <web@nonick.name>
 */
class PaymentFormFields implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Payments\Service\PaymentFormFields $service */
        $service = new \Payment\Service\PaymentFormFields();

        $config = $container->get('configuration');
        if (!isset($config['cyberSource'])) {
            throw new \Exception('Не указан конфиг CyberSource клиента');
        }
        $config = $config['cyberSource'];
        $service->setAccessKey($config['accessKey']);
        $service->setProfileId($config['profileId']);
        $service->setSecretKey($config['secretKey']);
        $service->setSecureAlgorithm($config['secureAlgorithm']);
        $service->setEndpoint($config['endpoint']);

        return $service;
    }
}
