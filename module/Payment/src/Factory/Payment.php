<?php

namespace Payment\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class Payment
 *
 * @author Palavi Vitalie
 */
class Payment implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var \Payment\Service\Payment $paymentService */
        $paymentService = $callback();
        $paymentService->setAuthService($container->get(AuthenticationService::class));

        return $paymentService;
    }
}
