<?php

namespace Payment\Form;

use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Class PaymentForm
 * @author nonick <web@nonick.name>
 */
class PaymentForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return '\\Payment\\InputFilter\\Form\\Payment';
    }

    public function __construct()
    {
        parent::__construct('payment-form');

        //Set POST method for
        $this->setAttribute('method', 'post');
        $this->addElements();
    }

    public function addElements()
    {
        $this->add(
            [
                'type' => 'text',
                'name' => 'amount',
                'attributes' => [
                    'id' => 'payment_amount'
                ],
                'options' => [
                    'label' => 'Amount',
                ],
            ]
        );

        $this->add([
            'type' => 'hidden',
            'name' => 'currency',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'transaction_type',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'reference_number',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'signed_date_time',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'unsigned_field_names',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'signed_field_names',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'transaction_uuid',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'profile_id',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'access_key',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'locale',
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'signature',
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm payment',
                'class' => 'button button-green-out up',
                'disabled'
            ],
        ]);
    }
}
