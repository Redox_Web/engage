<?php

namespace Payment\Service;

use Payment\Entity\PaymentStatus;
use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\Inheritance\Inheritance;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class Payment
 * @method Inheritance getInheritanceResolver()
 */
class Payment implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Payment\Entity\Payment::class;
    }

    /**
     * @param array $data
     * @return object
     */
    public function createEmptyEntity(array $data)
    {
        return new \Payment\Entity\Payment();
    }

    /**
     * @param $data
     * @return object
     */
    public function insertPaymentLog($data)
    {
        $status = PaymentStatus::createFromScalar($data['decision']);
        $data = [
            'user' => $this->getAuthService()->getIdentity(),
            'status' => $status,
            'amount' => $data['auth_amount'],
            'order' => $data['req_reference_number'],
            'transaction' => $data['transaction_id'],
            'transactionTime' => new \DateTime($data['auth_time']),
            'responseMessage' => $data['message'],
            'dataLog' => json_encode($data),
        ];

        return $this->create($data);
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthService(): AuthenticationService
    {
        return $this->authService;
    }

    /**
     * @param AuthenticationService $authService
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Find payment by order
     * @param string $order
     * @return null|object
     */
    public function findByOrder(string $order)
    {
        return $this->getInheritanceResolver()->getRepository()->findOneBy(['order' => $order]);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->getInheritanceResolver()->getRepository();
    }

    public function getLastPayment($user)
    {
        return current($this->getRepository()->findBy(
            [
                'user' => $user,
                'status.status' => PaymentStatus::STATUS_ACCEPT,
            ],
            ['identity' => 'DESC'],
            1
        ));
    }

    public function getAllPaymentTransactions()
    {
        return $this->getInheritanceResolver()->getRepository()->findAll();
    }
}
