<?php
/**
 * @author Melnic Oleg
 */

namespace Payment\Entity;

use Application\Entity\Exception\InvalidArgument;
use \RedoxWeb\WTL\Filter\From\FromScalar\FactoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PaymentStatus
 * @ORM\Embeddable
 */
class PaymentStatus implements FactoryInterface
{
    const STATUS_ACCEPT = 'ACCEPT';
    const STATUS_DECLINE = 'DECLINE';
    const STATUS_REVIEW = 'REVIEW';
    const STATUS_ERROR = 'ERROR';
    const STATUS_CANCEL = 'CANCEL';

    /**
     * @var String
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=false, options={"fixed": true})
     */
    private $status;

    public function __construct($status)
    {
        if (!in_array($status, $this->getConstValues())) {
            throw new InvalidArgument('Value is not valid');
        }

        $this->status = $status;
    }

    /**
     * Creating an object from scalar value
     *
     * @param $data
     *
     * @return PaymentStatus
     */
    public static function createFromScalar($data)
    {
        return new self($data);
    }

    /**
     * @return array
     */
    public function getConstValues()
    {
        return [
            self::STATUS_ACCEPT,
            self::STATUS_DECLINE,
            self::STATUS_REVIEW,
            self::STATUS_ERROR,
            self::STATUS_CANCEL
        ];
    }

    /**
     * @return String
     */
    public function getValue()
    {
        return $this->status;
    }

    /**
     * Is status success?
     * @return bool
     */
    public function isSuccess()
    {
        return $this->getValue() === self::STATUS_ACCEPT;
    }
}
