<?php

namespace Payment\Entity;

use Application\Entity\User\UserAbstract;
use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @author Topuria Evgheni <jeneat@redox.ca>
 * @version 1.0
 *
 * @ORM\Table(
 *     name="payment"
 * )
 * @ORM\Entity
 */
class Payment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $identity;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Entity\User\UserAbstract")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=false, precision=5, scale=2)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="data_log", type="text", nullable=true)
     */
    private $dataLog;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORm\Column(name="reference_number", type="string", length=50, nullable=false)
     */
    private $order;

    /**
     * @var PaymentStatus
     *
     * @ORM\Embedded(class="\Payment\Entity\PaymentStatus")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=26, nullable=true)
     */
    private $transaction;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="transaction_time", type="datetime", nullable=true)
     */
    private $transactionTime;

    /**
     * @var string
     *
     * @ORM\Column(name="response_message", type="string", length=255)
     */
    private $responseMessage;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getIdentity(): int
    {
        return $this->identity;
    }

    /**
     * Get related student
     *
     * @return UserAbstract
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserAbstract $user
     */
    public function setUser(UserAbstract $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getDataLog(): string
    {
        return $this->dataLog;
    }

    /**
     * @param string $dataLog
     */
    public function setDataLog(string $dataLog)
    {
        $this->dataLog = $dataLog;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return clone $this->created;
    }

    /**
     * @return string
     */
    public function getNextPaid()
    {
        $date = $this->getCreated();
        $start = new \DateTime($date->format('Y-m-d'), new \DateTimeZone("UTC"));
        $month_later = clone $start;
        $month_later->add(new \DateInterval("P1M"));

        return $month_later->format('F d, Y');
    }

    /**
     * @return PaymentStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param PaymentStatus $status
     */
    public function setStatus(PaymentStatus $status = null)
    {
        $this->status = $status;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->getStatus()->isSuccess();
    }

    /**
     * @return string
     */
    public function getTransaction(): string
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     */
    public function setTransaction(string $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder(string $order)
    {
        $this->order = $order;
    }

    /**
     * @return \DateTime
     */
    public function getTransactionTime(): \DateTime
    {
        return $this->transactionTime;
    }

    /**
     * @param \DateTime $transactionTime
     */
    public function setTransactionTime(\DateTime $transactionTime)
    {
        $this->transactionTime = $transactionTime;
    }

    /**
     * @return string
     */
    public function getResponseMessage(): string
    {
        return $this->responseMessage;
    }

    /**
     * @param string $responseMessage
     */
    public function setResponseMessage(string $responseMessage)
    {
        $this->responseMessage = $responseMessage;
    }
}
