<?php
namespace Payment\Controller;

use Payment\Entity\PaymentStatus;
use Application\Entity\User\UserAbstract;
use Payment\Service\Payment;
use Payment\Form\PaymentForm;
use Payment\Service\PaymentFormFields;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class PaymentController
 *
 * @author nonick <web@nonick.name>
 */
class IndexController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function indexAction()
    {
        return $this->notFoundAction();
    }

    public function confirmationAction()
    {
        /** @var PaymentForm $paymentForm */
        $paymentForm = $this->getServiceLocator()->get(PaymentForm::class);
        /** @var PaymentFormFields $paymentFormFields */
        $paymentFormFields = $this->getServiceLocator()->get(PaymentFormFields::class);

        $paymentData = [
            'access_key' => $paymentFormFields->getAccessKey(), // R - ключ доступа к платежному терминалу
            'profile_id' => $paymentFormFields->getProfileId(), // R - id активного профиля
            'transaction_uuid' => uniqid(), // R - уникальный ключ для каждой транзакци создаваемый на нашей стороне, для проверки повторных попыток оплаты (на стороне cybersource)
            'signed_field_names' => $paymentFormFields->getSignedFields(), // R - список полей учавствующих в подписи
            'unsigned_field_names' => '', // поля исключаемые из подписи
            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"), // R - дата и время генерации подписи, учавствует в проверке кол-ва попытак повторных оплат транзакци
            'transaction_type' => 'sale', // R - тип транзакции, authorization - как бы отложенная покупка, sale - мгновеная продажа товара, мануал стр. 14 пункт 4 (для уточнения)
            'locale' => 'en', // R - локаль
            'reference_number' => time(), // R - номер заказа, уникальный, по большому счету id счета в нашей системе
            'amount' => 19.00, // R - float - сумма транзакции
            'currency' => 'USD', // R - валюта транзакции
            'signature' => '', // R - подпись, хеш по подписываемым полям
        ];
        $paymentData['signature'] = $paymentFormFields->getSignature($paymentData);

        $paymentForm->setData($paymentData);
        $paymentForm->setAttribute('action', $paymentFormFields->getEndpoint());

        if (!$paymentForm->isValid()) {
            return $this->redirect()->toRoute('payment/invalidForm');
        }

        $viewModel = new ViewModel();
        $viewModel->setVariable('form', $paymentForm);

        return $viewModel;
    }

    public function receiptAction()
    {
        $writer = new \Zend\Log\Writer\Stream(getcwd().'/data/logs/payments.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        /**
         * 1. Проверяем ответ от сервера
         * 1.1 Оплата прошла успешно - изменяем счет и пользователя (создаем пользователя)
         * 1.2 От оплаты отказались - переводим счет в отмененный
         * 2. Перенаправляем пользователя в ЛК
         */
        $paymentsParams = $this->params()->fromPost();
        /** @var PaymentFormFields $paymentFormFields */
        $paymentFormFields = $this->getServiceLocator()->get(PaymentFormFields::class);

        $params = [];
        foreach ($paymentsParams as $key => $value) {
            $params[$key] = $value;
        }

        $signature = $paymentFormFields->getSignature($params);

        if ($params['signature'] != $signature) {
            return $this->redirect()->toRoute('payment/paymentError');
        }
        /** @var Payment $paymentService */
        $paymentService = $this->getServiceLocator()->get(Payment::class);

        /** @var PaymentStatus $status */
        $status = PaymentStatus::createFromScalar($params['decision']);
        $payment = $paymentService->insertPaymentLog($params);



        switch($status->getValue()) {
            case PaymentStatus::STATUS_ACCEPT:
                /** @var UserAbstract $user */
//                $user = $this->auth()->getIdentity();
//                $user->setPremium();
                break;
            case PaymentStatus::STATUS_CANCEL: // пользователь отменил транзакцию
            case PaymentStatus::STATUS_DECLINE: // транзакция отклонена (не заполнены все обязательные поля или проблемы при оплате)
            case PaymentStatus::STATUS_ERROR: // доступ запрещен или страница не найдена
            case PaymentStatus::STATUS_REVIEW: // транзакция отменена, но "оплата" все ещё может прийти (скорее всего, что-то типа задержки ответа, вероятно ответ от сервера может прийти на этот коллбэк постом в любое время, нужно разобраться и обработаь ситуевину)
                                        // Authorization was declined; however, the capture may still be possible. Review payment details.
//                по идее у нас все залогировано, сообщаем пользователю что произошла ошибка, выводим id транзакции и счета, пусть свяжеться с сапортом
                break;
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(
            [
                'result' => $status->getValue(),
                'payment' => $payment,
            ]
        );

        return $viewModel;
    }
}
