<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

use Payment\Controller\IndexController;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'payment' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/payment/',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                    'route_plugins' => array(),
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'action' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => ':action/',
                            'defaults' => [
                                'controller' => IndexController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            'common_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__.'/../src/Entity',]
            ],
            'orm_default' => [
                'drivers' => [
                    'Payment\Entity' => 'common_driver',
                ]
            ],
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    \Gedmo\Tree\TreeListener::class,
                    \Gedmo\Sluggable\SluggableListener::class,
                    \Gedmo\Sortable\SortableListener::class,
                    \Gedmo\Timestampable\TimestampableListener::class,
                    \Application\Listener\User\User::class,
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'payment/index/index' => __DIR__ . '/../view/payment/index/index.phtml',
            'user/401' => __DIR__ . '/../view/error/401.phtml'
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'filters' => [
        'invokables' => [],
        'factories' => [],
        'shared' => [],
    ],
    'validators' => [
        'invokables' => [],
        'factories' => [],
    ],
    'service_manager' => [
        'factories' => [
            \Payment\Service\PaymentFormFields::class => \Payment\Factory\PaymentFormFields::class,
        ],
        'delegators' => [
            \Payment\Service\Payment::class => [
                \Payment\Factory\Payment::class,
            ],
        ]
    ],
    'cyberSource' => [
        'accessKey' => '690e6f4392913c22ac182ad952770907', // test engage
        'profileId' => '8FB8BEF0-58BB-4468-B9FF-546ED26EE41B', // test engage
        'secretKey' => '760b4f5e63724528b48928276aa04587c184463dfdd24f539201a198676b67d4a2e512cedea0409a9834421231cd77d1aa6441d1e1b8432a9c171b0afb7502213b9dae5bcb334a119242cb67f817660259b25bec682c45b6a167dfc6d8290883334e83f8e5bc4f148ef3c00642407aefc00bf6f96c144cb99b2b6c07260d0f46', // test engage
        'secureAlgorithm' => 'sha256',
        'endpoint' => 'https://testsecureacceptance.cybersource.com/pay'
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'Payments' => [
                    "controllers" => [
                        \Payment\Controller\IndexController::class => [
                            'default' => [
                                'user'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'inputFilterBase' => [
        'payment.profile.id' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 36,
                        'max' => 36
                    ]
                ],
                'regexp' => [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[A-Za-z0-9-]+$~'
                    ]
                ],
            ]
        ],
        'payment.access.key' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 32,
                        'max' => 32
                    ]
                ],
                'regexp' => [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[A-Za-z0-9]+$~'
                    ]
                ],
            ]
        ],
        'payment.locale' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
//                'not_empty' => [
//                    'name' => 'not_empty',
//                ],
                'stringLength' => [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 2,
                        'max' => 3
                    ]
                ],
            ],
        ],
        'payment.transaction.type' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'inArray',
                    'options' => [
                        'haystack' => [
                            'authorization',
                            'sale',
                            'create_payment_token',
                            ',update_payment_token',
                        ],
                        'strict' => true
                    ]
                ]
            ]
        ],
        'payment.currency' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
//                'not_empty' => [
//                    'name' => 'not_empty',
//                ],
                'stringLength' => [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'max' => 3
                    ]
                ],
            ],
        ],
        'payment.signed.date' => [
            'filters' => [
                ['name' => 'ToNull'],
                'datetime' => [
                    'name' => \RedoxWeb\WTL\Filter\DateTime::class
                ],
            ],
            'validators' => [
                [
                    'name' => 'Instance',
                    'options' => [
                        'AllowNull' => true,
                        'className' => 'DateTime',
                    ],
                ],
            ],
        ],
    ]
];
