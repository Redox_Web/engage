<?php
namespace Backend;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\TreeRouteStack;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'router_class' => TreeRouteStack::class,
        'routes' => [
            'backend' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/backend/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                    'route_plugins' => array(),
                ],
                'may_terminate' => true,
                'child_routes'=>[
                    'articleUpdate' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'article/update/[:id/]',
                            'defaults' => [
                                'id' => 0,
                                'controller' => Controller\ArticleItemController::class,
                                'action' => 'update',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'articleCreate' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'article/create/',
                            'defaults' => [
                                'id' => 0,
                                'controller' => Controller\ArticleItemController::class,
                                'action' => 'create',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'packageUpdate' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'update/[:id/]',
                            'defaults' => [
                                'id' => 0,
                                'controller' => Controller\SubscriptionPackageController::class,
                                'action' => 'update',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'packageCreate' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'create/',
                            'defaults' => [
                                'id' => 0,
                                'controller' => Controller\SubscriptionPackageController::class,
                                'action' => 'create',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'packageDelete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'delete/[:id/]',
                            'defaults' => [
                                'id' => 0,
                                'controller' => Controller\SubscriptionPackageController::class,
                                'action' => 'delete',
                            ],
                            'constraints' => array(
                                'id' => '[0-9]+'
                            ),
                        ],
                    ],
                    'courses-categories' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'courses-categories/',
                            'defaults' => [
                                'controller' => Controller\CourseCategoryController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'course-category' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => 'course-category/[:action/[:id/]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller'    => Controller\CourseCategoryController::class,
                                'action'        => 'edit',
                                'id'            => 0,
                            ],
                        ],
                    ],
                    'courses-chapters' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'courses-chapters/',
                            'defaults' => [
                                'controller' => Controller\CourseChapterController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'course-chapter' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => 'course-chapter/[:action/[:id/]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller'    => Controller\CourseChapterController::class,
                                'action'        => 'edit',
                                'id'            => 0,
                            ],
                        ],
                    ],
                    'courses' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'courses/',
                            'defaults' => [
                                'controller' => Controller\CoursesController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'=>[
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => 'create/',
                                    'defaults' => [
                                        'controller' => Controller\CoursesController::class,
                                        'action' => 'create',
                                    ],
                                ],
                            ],
                            'actions' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:action/:id/]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                        'controller'    => Controller\CoursesController::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'item' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'item/',
                            'defaults' => [
                                'controller' => Controller\Course\ItemController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'=>[
                            'actions' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:action/[:id/]]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                        'controller'    => Controller\Course\ItemController::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'users' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'users/',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'=>[
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => 'create/',
                                    'defaults' => [
                                        'controller' => Controller\UserController::class,
                                        'action' => 'create',
                                    ],
                                ],
                            ],
                            'actions' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:action/:id/]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                        'controller' => Controller\UserController::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'payments' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => 'payments/',
                            'defaults' => [
                                'controller'    => Controller\PaymentController::class,
                                'action'        => 'index',
                            ],
                        ],
                    ],
                    'static-pages' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'static-pages/',
                            'defaults' => [
                                'controller' => Controller\StaticPages\StaticPages::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'=>[
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => 'create/',
                                    'defaults' => [
                                        'controller' => Controller\StaticPages\StaticPages::class,
                                        'action' => 'create',
                                    ],
                                ],
                            ],
                            'actions' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '[:action/:id/]',
                                    'constraints' => [
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                                    'defaults' => [
                                        'controller'    => Controller\StaticPages\StaticPages::class,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'mail' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => 'mail/',
                            'defaults' => [
                                'controller'    => Controller\Mail::class,
                                'action'        => 'index',
                            ],
                        ],
                    ],
                    'migartion_users' => [
                        'type'    => Literal::class,
                        'options' => [
                            'route'    => 'migrationUsers/',
                            'defaults' => [
                                'controller'    => Controller\Mail::class,
                                'action'        => 'migrationUsers',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\CourseCategoryController::class => InvokableFactory::class,
            Controller\CourseChapterController::class => InvokableFactory::class,
            Controller\CoursesController::class => InvokableFactory::class,
            Controller\SubscriptionPackageController::class => InvokableFactory::class,
            Controller\UserController::class => InvokableFactory::class,
            Controller\ArticleItemController::class => InvokableFactory::class,
            Controller\PaymentController::class => InvokableFactory::class,
            \Backend\Controller\StaticPages\StaticPages::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'view_helpers' => [
        'invokables' => [],
        'factories' => [
        ],
        'delegators' => [
        ],
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'Backend' => [
                    "controllers" => [
                        \Backend\Controller\IndexController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\CourseCategoryController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\CourseChapterController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\CoursesController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\SubscriptionPackageController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\UserController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\ArticleItemController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\PaymentController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\StaticPages\StaticPages::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\Mail::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\CourseController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\Course\ItemController::class => [
                            'default' => ['admin'],
                        ],
                        \Backend\Controller\Mail::class => [
                            'default' => ['admin'],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
