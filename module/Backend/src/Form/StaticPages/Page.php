<?php
namespace Backend\Form\StaticPages;

use RedoxWeb\WTL\Form\Form;
use StaticPages\Entity\Site;
use StaticPages\Entity\State;
use StaticPages\Entity\Template;

/**
 * Class Page
 *
 * @author Shahnovsky Alex
 */
class Page extends Form
{
    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('static-pages-form');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Enter Name',
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'alias',
            'attributes' => [
                'id' => 'alias',
                'placeholder'=>'Enter alias',
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Alias',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'content',
            'attributes' => [
                'id' => 'content',
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Content',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'javascript',
            'attributes' => [
                'id' => 'javascript',
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'JavaScript',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'url',
            'attributes' => [
                'id' => 'url',
                'placeholder'=>'Enter url',
            ],
            'options' => [
                'label' => 'Url'
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'state',
            'attributes' => [
                'id' => 'state',
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'State',
                'value_options' => [
                    State::ACTIVE => 'Active',
                    State::INACTIVE => 'Inactive',
                ],
                'disable_inarray_validator' => true,
            ],
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'group',
            'attributes' => [
                'id' => 'group'
            ],
            'options' => [
                'label' => 'Group',
                'value_options' => [
                    'nullGroup' => 'No Group',
                ],
                'disable_inarray_validator' => true,
            ],
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'template',
            'attributes' => [
                'id' => 'template'
            ],
            'options' => [
                'label' => 'Template',
                'value_options' => [
                    Template::FULL_PAGE => 'Full Page',
                ],
                'disable_inarray_validator' => true,
            ],
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'site',
            'attributes' => [
                'id' => 'site'
            ],
            'options' => [
                'label' => 'Site',
                'value_options' => [
                    Site::FRONT_END => 'Frontend',
                ],
                'disable_inarray_validator' => true,
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
                'class'=>'button button-green-out',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'StaticPages\InputFilter\Page';
    }
}
