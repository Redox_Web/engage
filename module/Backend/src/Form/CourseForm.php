<?php

namespace Backend\Form;

use Application\Entity\Course\Difficulty;
use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class CourseForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('course-form');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "email" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'class'=>'validate',
            ],
            'options' => [
                'label' => 'Name',
                'label_attributes' => [
                    'requiredSuffix' => '*****',
                ],
            ]
        ]);

        // Add "description" field
        $this->add([
            'type' => 'textarea',
            'name' => 'description',
            'attributes' => [
                'id' => 'description',
                'class'=>'materialize-textarea',
            ],
            'options' => [
                'label' => 'Course description',
            ]
        ]);

        // Add "alias" field
        $this->add([
            'type' => 'text',
            'name' => 'alias',
            'attributes' => [
                'id' => 'alias'
            ],
            'options' => [
                'label' => 'Alias'
            ]
        ]);

        // Add "categories" field
        $this->add([
            'type' => 'select',
            'name' => 'categories',
            'attributes' => [
                'id' => 'categories',
                'multiple' => true,
            ],
            'options' => [
                'label' => 'Category',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "execution" field
        $this->add([
            'type' => 'select',
            'name' => 'execution',
            'attributes' => [
                'id' => 'execution',
            ],
            'options' => [
                'label' => 'Execution',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "howPass" field
        $this->add([
            'type' => 'text',
            'name' => 'howPass',
            'attributes' => [
                'id' => 'howPass',
                'class' => 'materialize-textarea',
            ],
            'options' => [
                'label' => 'Course graduation percent',
            ]
        ]);

        // Add "institution" field
        $this->add([
            'type' => 'textarea',
            'name' => 'institution',
            'attributes' => [
                'id' => 'howPass',
                'class' => 'materialize-textarea',
            ],
            'options' => [
                'label' => 'Preliminary Requirements',
            ]
        ]);

        // Add "skills_to_achieve" field
        $this->add([
            'type' => 'textarea',
            'name' => 'skillsToAchieve',
            'attributes' => [
                'id' => 'skillsToAchieve',
                'class' => 'materialize-textarea',
            ],
            'options' => [
                'label' => 'Achieving skills',
            ]
        ]);

        // Add "teacher" field
        $this->add([
            'type' => 'select',
            'name' => 'author',
            'attributes' => [
                'id' => 'author',
            ],
            'options' => [
                'label' => 'Author',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "teacher" field
        $this->add([
            'type' => 'select',
            'name' => 'teacher',
            'attributes' => [
                'id' => 'teacher',
            ],
            'options' => [
                'label' => 'Teacher',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "difficulty" field
        $this->add([
            'type' => 'select',
            'name' => 'difficulty',
            'attributes' => [
                'id' => 'difficulty',
            ],
            'options' => [
                'label' => 'Difficulty',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Course\Course';
    }
}

