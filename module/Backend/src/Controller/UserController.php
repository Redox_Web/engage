<?php
namespace Backend\Controller;

use Backend\Form\UserForm;
use RedoxWeb\WTL\Crud\Exception\ValidationException;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Application\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\User\User as UserService;
use Zend\Mvc\MvcEvent;
use RedoxWeb\WTL\Entity\File\ObjectType;
use RedoxWeb\WTL\Entity\File\Image as ImageFile;

class UserController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * We override the parent class' onDispatch() method to
     * set an alternative layout for all actions in this controller.
     */
    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and grab the response
        $response = parent::onDispatch($e);

        // Set alternative layout
        $this->layout()->setTemplate('backend/layout/layout');

        // Return the response
        return $response;
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $users = $this->getServiceLocator()->get(UserService::class)
            ->getAllUsers();
        return new ViewModel(
            [
                'usersList' => $users
            ]
        );
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function createAction()
    {
        /** @var UserForm $form */
        $form = $this->getServiceLocator()->get(UserForm::class);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            // Create new user.
            $result = $this->getServiceLocator()->get(UserService::class)->createNew($data);
            if (is_array($result) && isset($result['errors']) && $result['errors']) {
                $form->setData($data);
                $form->setMessages($result['errors']);
            } else {
                // Save user avatar
                $avatarInfo = $this->getRequest()->getFiles('avatar');
                /** @var ImageFile $savedImage */
                $savedImage = $this->image()->uploadImage($avatarInfo, ObjectType::TYPE_CLIENT_AVATAR);
                $data['avatar'] = $savedImage;
                $this->getServiceLocator()->get(UserService::class)->saveAvatar($result->getIdentity(), $data);
                return $this->redirect()->toRoute('backend/users');
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        /** @var UserForm $form */
        $form = $this->getServiceLocator()->get(UserForm::class);
        $userId = (int) $this->params()->fromRoute('id', -1);
        if ($userId<0) {
            return $this->notFoundAction();
        }
        /** @var UserService $form */
        $user = $this->getServiceLocator()->get(UserService::class)
            ->getOne(array('identity' => $userId));
        if (is_null($user)) {
            return $this->notFoundAction();
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $data['identity'] = $user->getIdentity();
            // Update existing user.
            $result = $this->getServiceLocator()->get(UserService::class)->changeData($user->getIdentity(), $data);
            if (is_array($result) && isset($result['errors']) && $result['errors']) {
                $form->setData($data);
                $form->setMessages($result['errors']);
            } else {
                // Save user avatar
                $avatarInfo = $this->getRequest()->getFiles('avatar');
                /** @var ImageFile $savedImage */
                $savedImage = $this->image()->uploadImage($avatarInfo, ObjectType::TYPE_CLIENT_AVATAR);
                $data['avatar'] = $savedImage;
                $this->getServiceLocator()->get(UserService::class)->saveAvatar($result->getIdentity(), $data);
                return $this->redirect()->toRoute('backend/users');
            }
        } else {
            $data = [
                'email' => $user->getEmail(),
                'fullName' => $user->getFullName(),
                'description' => $user->getDescription(),
                'state' => $user->getState(),
                'birthday' => $user->getBirthday()->format('Y-m-d'),
                'address' => $user->getAddress(),
                'timezone' => $user->getTimezone(),
                'sex' => $user->getSex(),
                'state' => $user->getState(),
                'type' => $this->getServiceLocator()->get(UserService::class)->getUserType($user),
            ];
            $form->setData($data);
        }

        return new ViewModel([
            'form' => $form,
            'user' => $user
        ]);
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $userId = (int) $this->params()->fromRoute('id', -1);

        if ($userId<0) {
            return $this->notFoundAction();
        }

        $this->getServiceLocator()->get(UserService::class)->delete($userId);

        return $this->redirect()->toRoute('backend/users');
    }
}
