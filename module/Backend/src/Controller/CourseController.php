<?php
namespace Backend\Controller;

use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Application\Service\Course\Course as CourseService;
use Backend\Form\CourseForm;


class CourseController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * We override the parent class' onDispatch() method to
     * set an alternative layout for all actions in this controller.
     */
    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and grab the response
        $response = parent::onDispatch($e);

        // Set alternative layout
        $this->layout()->setTemplate('backend/layout/layout_main');

        // Return the response
        return $response;
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $courses = $this->getServiceLocator()->get(CourseService::class)->getAllCourses();
        return new ViewModel(
            [
                'courses' => $courses
            ]
        );
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function createAction()
    {
        $form = $this->getServiceLocator()->get(CourseForm::class);
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            // Create course
            $result = $this->getServiceLocator()->get(CourseService::class)->createNew($data);
            if (is_array($result) && isset($result['errors']) && $result['errors']) {
                $form->setData($data);
                $form->setMessages($result['errors']);
            } else {
                return $this->redirect()->toRoute('backend/courses');
            }
        }
        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        $form = $form = $this->getServiceLocator()->get(CourseForm::class);
        $courseId = (int)$this->params()->fromRoute('id', -1);
        if ($courseId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $course = $this->getServiceLocator()->get(CourseService::class)
            ->getOne(array('identity' => $courseId));
        if ($course == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $data['identity'] = $course->getIdentity();

            // Update course
            $result = $this->getServiceLocator()->get(CourseService::class)->changeData($course->getIdentity(), $data);
            if (is_array($result) && isset($result['errors']) && $result['errors']) {
                $form->setData($data);
                $form->setMessages($result['errors']);
            } else {
                return $this->redirect()->toRoute('backend/courses');
            }
        } else {
            $data = [
                'name' => $course->getName(),
                'description' => $course->getDescription(),
                'alias' => $course->getAlias(),
                'execution' => $course->getExecution(),
                'howPass' => $course->getHowPass(),
                'difficulty' => $course->getDifficulty(),
                'author' => $course->getAuthor(),
                'teacher' => $course->getTeacher(),
                'categories' => $course->getCategories(),
                'institution' => $course->getInstitution(),
            ];

            $form->setData($data);
        }

        return new ViewModel([
            'form' => $form,
            'course' => $course
        ]);
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $courseId = (int)$this->params()->fromRoute('id', -1);
        if ($courseId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $course = $this->getServiceLocator()->get(CourseService::class)
            ->getOne(array('identity' => $courseId));
        if ($course == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $this->getServiceLocator()->get(CourseService::class)->delete($course);

        return $this->redirect()->toRoute('backend/courses');
    }

    public function itemCreate()
    {

    }
}