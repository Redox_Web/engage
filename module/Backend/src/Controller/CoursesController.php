<?php
namespace Backend\Controller;

use Application\Form\CourseCreateForm;
use Application\Form\CourseForm;
use Application\Service\Course\Course;
use Application\Controller\AbstractActionController;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use RedoxWeb\WTL\Crud\Exception\ValidationException;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Course as CourseService;
use Application\Service\Course\Chapter as CourseChapterService;
use Zend\Mvc\MvcEvent;

/**
 * Class CoursesController
 * Class for manage courses
 */
class CoursesController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * We override the parent class' onDispatch() method to
     * set an alternative layout for all actions in this controller.
     */
    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and grab the response
        $response = parent::onDispatch($e);

        // Set alternative layout
        $this->layout()->setTemplate('backend/layout/layout');

        // Return the response
        return $response;
    }

    public function indexAction()
    {
        $items = $this->getServiceLocator()->get(CourseService::class)
            ->getAllCourses();
        return new ViewModel(
            [
                'items' => $items,
            ]
        );
    }

    public function createAction()
    {
        /** @var CourseCreateForm $form */
        $form = $this->getServiceLocator()->get(CourseCreateForm::class);

        /** @var Course $service */
        $service = $this->getServiceLocator()->get(Course::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $data['teacher'] = $this->auth()->getIdentity();
                $data['author'] = $this->auth()->getIdentity();
                $entity = $service->firstSave($data);

                return $this->redirect()->toRoute(
                    'backend/courses',
                    ['id' => $entity->getIdentity()]
                );
            }
        }

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        /** @var CourseCreateForm $form */
        $form = $this->getServiceLocator()->get(CourseForm::class);
        /** @var Course $service */
        $service = $this->getServiceLocator()->get(Course::class);

        // Getting course id
        $itemId = $this->params()->fromRoute('id', -1);
        $chapters = $this->getServiceLocator()->get(CourseChapterService::class)->getAllChaptersByCourse($itemId);

        // Course finding.
        /**
         * @var \Application\Entity\Course\Course $course
         */
        $course = $service->find($itemId);
        if (!$course) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['teacher'] = $course->getTeacher()->getIdentity();
            $data['author'] = $course->getAuthor()->getIdentity();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $service->update($itemId, $data);

                return $this->redirect()->toRoute(
                    'backend/courses',
                    [
                        'id' => $itemId,
                    ]
                );
            }
        } else {
            $data = [
                'name' => $course->getName(),
                'description' => $course->getDescription(),
                'categories' => $service->convertCategoriesToArray($course),
                'execution' => $course->getExecution()->getValue(),
                'howPass' => $course->getHowPass(),
                'institution' => $course->getInstitution(),
                'teacher' => $course->getTeacher(),
                'difficulty' => $course->getDifficulty()->getValue(),
                'skillsToAchieve' => $course->getSkillsToAchieve(),
            ];

            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
            'course' => $course,
            'chapters' => $chapters,
        ]);

        return $view;
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $courseId = (int) $this->params()->fromRoute('id', -1);

        // Validate input parameter
        if ($courseId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $course = $this->getServiceLocator()->get(CourseService::class)
            ->getOneCourse(array('identity' => $courseId));
        if ($course == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->getServiceLocator()->get(CourseService::class)->delete($course);

        // Redirect the list of courses.
        return $this->redirect()->toRoute('backend/courses');
    }

}
