<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/27/17
 */
namespace Backend\Controller\Course;

use Application\Controller\AbstractActionController;
use Application\Service\Course\Item\Item;
use Doctrine\ORM\EntityManager;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Item\Item as ItemService;
use Backend\Form\Course\Item\ItemAbstractForm;

/**
 * Class ItemController
 * Class for manage items
 */
class ItemController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $templateMapper = [
        'video' => 'backend/course/item',
    ];

    private $formMapper = [
        'article' => \Backend\Form\Item\Strategy\ArticleForm::class,
        'audio' => \Backend\Form\Item\Strategy\AudioForm::class,
        'video' => \Backend\Form\Item\Strategy\VideoForm::class,
        'quiz' => \Backend\Form\Item\Strategy\QuizForm::class,
        'image' => \Backend\Form\Item\Strategy\ImageForm::class,
        'document' => \Backend\Form\Course\Item\Strategy\DocumentForm::class,
    ];

    /**
     * item action
     * page for viewing one item
     */
    public function itemAction()
    {
        $alias = $this->params()->fromRoute('alias');
        $item = $this->getServiceLocator()->get(ItemService::class)
            ->getOne(array('alias' => $alias));

        if (is_null($item)) {
           $this->notFoundAction();
        }

        $this->leftMenu('courseLeftMenu', ['course' => $item->getChapter()->getCourse()]);

        return new ViewModel([
            'item' => $item
        ]);
    }

    public function createAction()
    {
        /** @var Chapter $courseService */
        $chapterService = $this->getServiceLocator()->get(Chapter::class);

        // Getting chapter id
        $chapterId = $this->params()->fromRoute('id', -1);

        /**
         * @var \Application\Entity\Course\Chapter $chapter
         */
        $chapter = $chapterService->find($chapterId);
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
        } else {
            $data['type'] = 'article';
        }

        /** @var Form $form */
        $form = $this->getServiceLocator()->get($this->getFormMapper($data['type']));

        /** @var Item $service */
        $service = $this->getServiceLocator()->get(Item::class);

        if ($this->getRequest()->isPost()) {
            $type = $data['type'];
            $file = $this->params()->fromFiles();
            if ($file[$data['type']]) {
                $data[$data['type']] = $this->S3file()->uploadFile($file[$data['type']]);
                $data["{$type}_file"] = $file["{$type}_file"];
            }

            $form->setData($data);

            if ($form->isValid()) {
                /** remove from filter input file field */
                $form->getInputFilter()->remove("{$type}_file");
                $data = $form->getData();
                $data['type'] = $type;
                $entity = $service->create($data);
                return $this->redirect()->toRoute(
                    'backend/item/actions',
                    [
                        'action' => 'edit',
                        'id' => $entity->getIdentity(),
                    ]
                );
            } else {
                pred($form->getMessages());
            }
        }

        $form->setData($data);

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $items = $this->getServiceLocator()->get(Item::class)->getAllItems();
        return new ViewModel(
            [
                'items' => $items
            ]
        );
    }

    public function editAction()
    {
        /** @var Item $service */
        $service = $this->getServiceLocator()->get(Item::class);
        $itemId = $this->params()->fromRoute('id', -1);

        /**
         * @var \Application\Entity\Course\Item\ItemAbstract $item
         */
        $item = $service->find($itemId);

        /** @var EntityManager $entityManager */
        $entityManager = $this->getServiceLocator()->get(EntityManager::class);
        $entityType = $entityManager->getClassMetadata(get_class($item))->discriminatorValue;

        /** @var Form $form */
        $form = $this->getServiceLocator()->get($this->getFormMapper($entityType));

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $type = $data['type'] = $entityType;
            $file = $this->params()->fromFiles();

            if ($file[$type]) {
                $data[$type] = $this->S3file()->uploadFile($file[$type]);
                $data["{$type}_file"] = $file["{$type}_file"];
            }

            if (empty($data[$type])) {
                $function = 'get'.$type;
                $data[$type] = $item->$function();
            }

            $data['type'] = $entityType;
            $data['chapter'] = $item->getChapter();

            $form->setData($data);

            if ($form->isValid()) {
                /** remove from filter input file field */
                $form->getInputFilter()->remove("{$type}_file");
                $data = $form->getData();
                $data['type'] = $type;

                $service->update($itemId, $data);

                return $this->redirect()->toRoute(
                    'backend/item/actions',
                    [
                        'id' => $item->getIdentity(),
                    ]
                );
            }
        } else {
            $data = $service->extract($itemId);
            $data['startDate'] = $data['startDate']->format('d-m-Y');
            $data['type'] = $entityType;
            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
            'item' => $item,
        ]);

        return $view;
    }

    public function deleteAction()
    {
        $postId = $this->params()->fromRoute('id', -1);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(Article::class);
        // Subscription finding.
        $subscriptionPackage = $service->find($postId);
        if ($subscriptionPackage == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $service->delete($postId);

        // Перенаправляем пользователя на страницу "index".
        return $this->redirect()->toRoute('frontend');
    }

    private function getTemplateMapper($type)
    {
        return $this->templateMapper[$type];
    }

    private function getFormMapper($type)
    {
        return $this->formMapper[$type];
    }
}
