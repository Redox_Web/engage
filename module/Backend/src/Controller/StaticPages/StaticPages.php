<?php
namespace Backend\Controller\StaticPages;

use Application\Controller\AbstractActionController;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use StaticPages\Service\Page;
use Zend\View\Model\ViewModel;

/**
 * Class StaticPages
 *
 * @author Shahnovsky Alex
 */
class StaticPages extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        /** @var Page $service */
        $service = $this->getServiceLocator()->get(Page::class);
        $pages = $service->findAll();
        $viewModel = new ViewModel([
            'pages' => $pages
        ]);
        $viewModel->setTemplate('backend/static-pages/index');

        return $viewModel;
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function createAction()
    {
        $form = $this->getServiceLocator()->get(\Backend\Form\StaticPages\Page::class);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                /** @var Page $service */
                $service = $this->getServiceLocator()->get(Page::class);
                $service->create($data);

                return $this->redirect()->toRoute('backend/static-pages');
            }
        }

        $viewModel = new ViewModel([
            'form' => $form
        ]);
        $viewModel->setTemplate('backend/static-pages/create');

        return $viewModel;
    }


    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        $form = $form = $this->getServiceLocator()->get(\Backend\Form\StaticPages\Page::class);

        $pageId = $this->params()->fromRoute('id');
        /** @var Page $service */
        $service = $this->getServiceLocator()->get(Page::class);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $service->update($pageId, $data);

                return $this->redirect()->toRoute('backend/static-pages');
            }
        } else {
            $form->setData($service->extract($pageId));
        }

        $viewModel = new ViewModel([
            'form' => $form
        ]);
        $viewModel->setTemplate('backend/static-pages/edit');

        return $viewModel;
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $pageId = $this->params()->fromRoute('id');
        /** @var Page $service */
        $service = $this->getServiceLocator()->get(Page::class);
        $service->delete($pageId);

        // Redirect the list of course categories.
        return $this->redirect()->toRoute('backend/static-pages');
    }
}
