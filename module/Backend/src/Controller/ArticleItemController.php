<?php
namespace Backend\Controller;

use Application\Form\Course\Item\Strategy\ArticleForm as ArticleItemForm;
use Application\Form\SubscriptionPackageForm;
use Application\Service\Course\Item\Strategy\Article;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ArticleItemController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function createAction()
    {
        /** @var SubscriptionPackageForm $form */
        $form = $this->getServiceLocator()->get(ArticleItemForm::class);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(Article::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $entity = $service->create($data);

                return $this->redirect()->toRoute(
                    'backend/articleUpdate',
                    [
                        'id' => $entity->getIdentity(),
                    ]
                );
            }
        }

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    public function updateAction()
    {
        /** @var SubscriptionPackageForm $form */
        $form = $this->getServiceLocator()->get(ArticleItemForm::class);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(Article::class);

        // Getting post id
        $articleId = $this->params()->fromRoute('id', -1);

        // Subscription finding.
        $articleItem = $service->find($articleId);
        if ($articleItem == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $service->update($articleId, $data);

                return $this->redirect()->toRoute(
                    'backend/articleUpdate',
                    [
                        'id' => $articleId,
                    ]
                );
            }
        } else {
            $state_value = $articleItem->getState()->getState();
            $data = [
                'name' => $articleItem->getName(),
                'description' => $articleItem->getDescription(),
                'content' => $articleItem->getContent(),
                'isFree' => $articleItem->isFree(),
                'startDate' => $articleItem->getStartDate()->format('d-m-y'),
                'state' => $state_value,
            ];

            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
        ]);
        $view->setTemplate('backend/article-item/create');

        return $view;
    }

    public function deleteAction()
    {
        $postId = $this->params()->fromRoute('id', -1);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(Article::class);
        // Subscription finding.
        $subscriptionPackage = $service->find($postId);
        if ($subscriptionPackage == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $service->delete($postId);

        // Перенаправляем пользователя на страницу "index".
        return $this->redirect()->toRoute('frontend');
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }
}
