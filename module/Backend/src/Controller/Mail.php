<?php
namespace Backend\Controller;

use Application\Controller\AbstractActionController;
use Application\Service\User\User;
use DrewM\MailChimp\MailChimp;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Application\Service\User\User as UserService;

/**
 * Class Mail
 *
 * @copyright (c) 2017, Redox
 */
class Mail extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        /** @var \Backend\Form\Mail $form */
        $form = $this->getServiceLocator()->get(\Backend\Form\Mail::class);
        $config = $this->getServiceLocator()->get('Config');

        /** @var MailChimp $service */
        $service = new MailChimp($config['mailchimp']['api-key']);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                $listID = $config['mailchimp']['listIDs']['general'];

                $compaign = $service->post("campaigns", [
                    'type' => 'regular',
                    'recipients' => [
                        'list_id' => $listID
                    ]
                ]);

                $batch = $service->new_batch();
                $batch->put( 'op1', "campaigns/".$compaign['id']."/content", [
                    'html' => $data['mail-body'],
                    'plain_text' => $data['mail-body'],
                ]);
                $batch->patch( 'op2', "campaigns/".$compaign['id'], [
                    'recipients' => [
                        'list_id' => $listID
                    ],
                    'settings' => [
                        'subject_line' => 'Engage Global Mail',
                        'from_name' => 'Engage Portal',
                        'reply_to' => 'office@redox.ca'
                    ]
                ]);
                $batch->post('op3', "campaigns/".$compaign['id']."/actions/send", []);

                $result = $batch->execute();

                if ($service->success()) {
                    $this->flashMessenger()->addMessage('Your message has been successfully sent!');
                } else {
                    $this->flashMessenger()->addMessage($service->getLastError());
                }
            } else {
                foreach ($form->getInputFilter()->getMessages() as $error) {
                    $this->flashMessenger()->addMessage($error);
                }
            }
        }

        return new ViewModel(
            [
                'form' => $form
            ]
        );
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function migrationUsersAction()
    {
        /** @var UserService $serviceUsers */
        $serviceUsers = $this->getServiceLocator()->get(User::class);
        $students = $serviceUsers->getAll(User::TYPE_STUDENT);
        $config = $this->getServiceLocator()->get('Config');
        $service = new MailChimp($config['mailchimp']['api-key']);
        $batch = $service->new_batch();

        foreach ($students as $key => $student) {
            try {
                $paramArr = [
                    'email_address' => $student->getEmail(),
                    'first_name' => $student->getFullName(),
                    'status' => 'subscribed',
                    'merge_fields' => [
                        "FNAME" => $student->getFullName()
                    ],
                ];

                $listID = $config['mailchimp']['listIDs']['general'];
                $batch->post("op" . $key, "lists/$listID/members", $paramArr);
            } catch (\ZfrMailChimp\Exception\Ls\AlreadySubscribedException $e) {
                vred($e->getMessage());
            } catch (\ZfrMailChimp\Exception\Ls\DoesNotExistException $e) {
                vred($e->getMessage());
            } catch (\ZfrMailChimp\Exception\ExceptionInterface $e) {
                vred($e->getMessage());
            }
        }
        try {
            $batch->execute();
        } catch (\Exception $e) {
            vred($e->getMessage());
        }
    }
}
