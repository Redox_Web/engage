<?php
namespace Backend\Controller;

use Application\Entity\ValueObject\Period;
use Application\Form\SubscriptionPackageForm;
use Application\Service\Subscription\SubscriptionPackage;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SubscriptionPackageController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function createAction()
    {
        /** @var SubscriptionPackageForm $form */
        $form = $this->getServiceLocator()->get(SubscriptionPackageForm::class);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(SubscriptionPackage::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $service->create($data);

                return $this->redirect()->toRoute(
                    'frontend'
                );
            }
        }

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    public function updateAction()
    {
        /** @var SubscriptionPackageForm $form */
        $form = $this->getServiceLocator()->get(SubscriptionPackageForm::class);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(SubscriptionPackage::class);

        // Getting post id
        $postId = $this->params()->fromRoute('id', -1);

        // Subscription finding.
        $subscriptionPackage = $service->find($postId);
        if ($subscriptionPackage == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $service->update($postId, $data);

                return $this->redirect()->toRoute(
                    'frontend'
                );
            }
        } else {
            $reriod_value = $subscriptionPackage->getPeriod()->getValue();
            $data = [
                'name' => $subscriptionPackage->getName(),
                'price' => $subscriptionPackage->getPrice(),
                'period' => $reriod_value,
            ];

            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
        ]);
        $view->setTemplate('backend/subscription-package/create');

        return $view;
    }

    public function deleteAction()
    {
        $postId = $this->params()->fromRoute('id', -1);

        /** @var SubscriptionPackage $service */
        $service = $this->getServiceLocator()->get(SubscriptionPackage::class);
        // Subscription finding.
        $subscriptionPackage = $service->find($postId);
        if ($subscriptionPackage == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $service->delete($postId);

        // Перенаправляем пользователя на страницу "index".
        return $this->redirect()->toRoute('frontend');
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }
}
