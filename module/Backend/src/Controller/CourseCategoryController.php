<?php
namespace Backend\Controller;

use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Category as CourseCategoryService;
use Application\Form\CourseCategoryForm;

class CourseCategoryController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $categories = $this->getServiceLocator()->get(CourseCategoryService::class)
            ->getAllCourseCategories();

        return new ViewModel(
            [
                'categoriesList' => $categories
            ]
        );
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function createAction()
    {
        // Create the form.
        $form = $this->getServiceLocator()->get(CourseCategoryForm::class);

        // Check whether this post is a POST request.
        if ($this->getRequest()->isPost()) {

            // Get POST data.
            $data = $this->params()->fromPost();

            // Fill form with data.
            $form->setData($data);
            if ($form->isValid()) {

                // Get validated form data.
                $data = $form->getData();

                // Use post manager service to add new post to database.
                $this->getServiceLocator()->get(CourseCategoryService::class)->create($data);

                // Redirect the user to "index" page.
                return $this->redirect()->toRoute('backend/courses-categories');
            }
        }

        // Render the view template.
        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        // Create form.
        $form = $form = $this->getServiceLocator()->get(CourseCategoryForm::class);

        // Get course category ID.
        $categoryId = (int)$this->params()->fromRoute('id', -1);

        // Validate input parameter
        if ($categoryId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $category = $this->getServiceLocator()->get(CourseCategoryService::class)
            ->getOne(array('identity' => $categoryId));
        if ($category == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getServiceLocator()->get(CourseCategoryService::class)->update($categoryId, $data);

                return $this->redirect()->toRoute('backend/courses-categories');
            }
        } else {
            $data = [
                'name' => $category->getName(),
                'alias' => $category->getAlias(),
                'state' => $category->getState()
            ];

            $form->setData($data);
        }

        // Render the view template.
        return new ViewModel([
            'form' => $form,
            'category' => $category
        ]);
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $categoryId = (int)$this->params()->fromRoute('id', -1);

        // Validate input parameter
        if ($categoryId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $category = $this->getServiceLocator()->get(CourseCategoryService::class)
            ->getOne(array('identity' => $categoryId));
        if ($category == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->getServiceLocator()->get(CourseCategoryService::class)->delete($category);

        // Redirect the list of course categories.
        return $this->redirect()->toRoute('backend/courses-categories');
    }
}
