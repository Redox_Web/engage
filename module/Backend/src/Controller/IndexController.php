<?php
namespace Backend\Controller;

use Application\Entity\User\Sex;
use Application\Entity\User\State;
use Application\Entity\User\UserAbstract;
use Application\Service\User\User;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    public function createAdminAction()
    {
        /** @var User $service */
        $service = $this->getServiceLocator()->get(User::class);

            $data['email'] = 'admin@engage.dev';
            $data['state'] = State::ACTIVE;
            $data['identity'] = '';
            $data['fullName'] = 'Admin';
            $data['password'] = 'admin777';
            $data['birthday'] = new \DateTime('17-10-1977');
            $data['type'] = $service::TYPE_ADMIN;
            $data['sex'] = Sex::MALE;

                /** @var UserAbstract $user */
                $user = $service->create($data);

                return $this->redirect()->toRoute(
                    'frontend'
                );
    }
}
