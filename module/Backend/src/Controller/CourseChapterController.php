<?php
namespace Backend\Controller;

use Application\Controller\AbstractActionController;
use Application\Form\ChapterCreateForm;
use Application\Service\Course\Chapter;
use Application\Service\Course\Course;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Chapter as ChapterService;
use Zend\Mvc\MvcEvent;

class CourseChapterController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;


    /**
     * We override the parent class' onDispatch() method to
     * set an alternative layout for all actions in this controller.
     */
    public function onDispatch(MvcEvent $e)
    {
        // Call the base class' onDispatch() first and grab the response
        $response = parent::onDispatch($e);

        // Set alternative layout
        $this->layout()->setTemplate('backend/layout/layout');

        // Return the response
        return $response;
    }

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $chapters = $this->getServiceLocator()->get(CourseChapterService::class)
            ->getAllCourseChapters();
        return new ViewModel(
            [
                'chaptersList' => $chapters
            ]
        );
    }

    /**
     * Create action
     *
     * @return ViewModel
     */
    public function createAction()
    {
        /** @var Course $courseService */
        $courseService = $this->getServiceLocator()->get(Course::class);

        // Getting course id
        $courseId = $this->params()->fromRoute('id', -1);

        /**
         * @var \Application\Entity\Course\Course $chapter
         */
        $course = $courseService->find($courseId);

        if (!$course) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->personalOfficeMenu('professorTopMenu', ['course' => $course]);
        $this->personalOfficeLeftMenu('professorLeftMenu', ['course' => $course]);


        /** @var ChapterCreateForm $form */
        $form = $this->getServiceLocator()->get(ChapterCreateForm::class);

        /** @var Chapter $chapterService */
        $chapterService = $this->getServiceLocator()->get(Chapter::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['course'] = $course;
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();

                /** @var \Application\Entity\Course\Chapter $chapterEntity */
                $chapterEntity = $chapterService->create($data);

                return $this->redirect()->toRoute(
                    'backend/courses/actions',
                    [
                        'action' => 'edit',
                        'id' => $courseId,
                    ]
                );
            }
        }

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    /**
     * Edit action
     *
     * @return ViewModel
     */
    public function editAction()
    {
        /** @var ChapterCreateForm $form */
        $form = $this->getServiceLocator()->get(ChapterCreateForm::class);


        /** @var Chapter $service */
        $service = $this->getServiceLocator()->get(Chapter::class);
        $chapterId = $this->params()->fromRoute('id', -1);

        /** @var \Application\Entity\Course\Chapter $chapter */
        $chapter = $service->find($chapterId);

        $this->personalOfficeMenu('professorTopMenu', ['course' => $chapter->getCourse()]);
        $this->personalOfficeLeftMenu('professorLeftMenu', ['course' => $chapter->getCourse()]);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['course'] = $chapter->getCourse()->getIdentity();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $chapter = $service->update($chapterId, $data);

                return $this->redirect()->toRoute(
                    'backend/courses/actions',
                    [
                        'action' => 'edit',
                        'id' => $chapter->getCourse()->getIdentity(),
                    ]
                );
            }
        } else {
            $data = [
                'name' => $chapter->getName(),
                'description' => $chapter->getDescription(),
                'state' => $chapter->getState(),
            ];

            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
            'chapterId' => $chapter->getIdentity(),
        ]);

        return $view;
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $chapterId = (int)$this->params()->fromRoute('id', -1);

        // Validate input parameter
        if ($chapterId<0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $chapter = $this->getServiceLocator()->get(Chapter::class)
            ->getOne(array('identity' => $chapterId));
        if ($chapter == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->getServiceLocator()->get(Chapter::class)->delete($chapter);

        // Redirect the list of course chapters.
        return $this->redirect()->toRoute(
            'backend/courses/actions',
            [
                'action' => 'edit',
                'id' => $chapter->getCourse()->getIdentity(),
            ]
        );
    }
}
