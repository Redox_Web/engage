<?php
namespace EngageApi\Service\AWS\S3\Factory;

use Aws\Sdk;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use RedoxWeb\WTL\Crud\Factory\Exception\InvalidConfig;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class S3
 *
 * @author Oleg Melnic
 */
class S3 implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return $service
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        if (!isset($config['aws'])) {
            throw new InvalidConfig('Amazon AWS service config required.');
        }

        $service = new \EngageApi\Service\AWS\S3\S3();
        /** @var Sdk $aws */
        $sdk = $container->get(Sdk::class);
        /** @var \Aws\S3\S3Client $client */
        $client = $sdk->createS3([
            'region' => $config['aws']['region'],
            'version' => $config['aws']['version']
        ]);
        $service->setClient($client);
        //$service->setMimeTypeValidator($container->get(\Zend\Validator\File\MimeType::class));
        $service->setBucket($config['aws']['bucket-media']);
        $service->setDirectories($config['aws']['directories']);

        return $service;
    }
}
