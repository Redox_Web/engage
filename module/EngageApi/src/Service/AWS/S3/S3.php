<?php
namespace EngageApi\Service\AWS\S3;

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

/**
 * Class S3
 *
 * @author Oleg Melnic
 */
class S3
{
    /**
     * @var S3Client
     */
    protected $client;

    /** @var  string */
    private $bucket;

    /** @var  array */
    private $directories;

    /**
     * @return S3Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param S3Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @param array $fileInfo
     * @throws AwsException
     *
     * @return string
     */
    public function saveFile(array $fileInfo)
    {
        $client = $this->getClient();
        $directory = false;
        $result = null;

        try {
            foreach ($this->getDirectories() as $type => $dir) {
                if ($this->getMimeTypeValidator(array($type))->isValid($fileInfo['tmp_name'])) {
                    $directory = $dir;
                }
            }
            $directory = $directory ? $directory : 'docs/';

            $fileName = $directory . md5($fileInfo['name']);
            $client->putObject([
                'Bucket'     => $this->getBucket(),
                'Key'        => $fileName,
                'SourceFile' => $fileInfo['tmp_name'],
                'ContentType' => $fileInfo['type'],
            ]);
        } catch (AwsException $e) {
            throw $e;
        }

        return $fileName;
    }

    /**
     * @param $fileName
     * @param $content
     *
     * @return string
     */
    public function saveContent($fileName, $content) : string
    {
        $client = $this->getClient();

        try {
            $client->putObject([
                'Bucket'     => $this->getBucket(),
                'Key'        => $fileName,
                'Body' => $content,
            ]);
        } catch (AwsException $e) {
            throw $e;
        }

        return $fileName;
    }

    /**
     * @param string $fileName
     * @throws AwsException
     *
     * @return \Aws\Result|null
     */
    public function openFile($fileName)
    {
        $client = $this->getClient();
        $result = null;

        try {
            $result = $client->getObject(array(
                'Bucket' => $this->getBucket(),
                'Key'    => $fileName,
            ));
        } catch (AwsException $e) {
            throw $e;
        }

        return $result;
    }

    /**
     * @param string $fileName
     * @throws AwsException
     * @return \Aws\Result|null
     */
    public function deleteFile($fileName)
    {
        $client = $this->getClient();
        $result = null;

        try {
            $result = $client->deleteObject(array(
                'Bucket' => $this->getBucket(),
                'Key'    => $fileName,
            ));
        } catch (AwsException $e) {
            throw $e;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getBucket()
    {
        return $this->bucket;
    }

    /**
     * @param string $bucket
     */
    public function setBucket(string $bucket)
    {
        $this->bucket = $bucket;
    }

    /**
     * @return array
     */
    public function getDirectories()
    {
        return $this->directories;
    }

    /**
     * @param array $directories
     */
    public function setDirectories(array $directories)
    {
        $this->directories = $directories;
    }

    /**
     * @param $type
     * @return \Zend\Validator\File\MimeType
     */
    public function getMimeTypeValidator($type)
    {
        return new \Zend\Validator\File\MimeType($type);
    }
}
