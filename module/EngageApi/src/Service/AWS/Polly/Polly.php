<?php
namespace EngageApi\Service\AWS\Polly;

use Aws\Polly\PollyClient;
use EngageApi\Service\TextToVoiceInterface;
use GuzzleHttp\Psr7\Stream;

/**
 * Class Polly
 *
 * @author Shahnovsky Alex
 */
class Polly implements TextToVoiceInterface
{
    /**
     * @var PollyClient
     */
    protected $client;

    /**
     * @return PollyClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param PollyClient $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @param $text
     *
     * @param string $voiceId
     *
     * @return array
     */
    public function getPhonemes($text, $voiceId)
    {
        $client = $this->getClient();

        $result = $client->synthesizeSpeech([
            'OutputFormat' => 'json', // REQUIRED
            'SpeechMarkTypes' => ['viseme'],
            'Text' => $text, // REQUIRED
            'VoiceId' => $voiceId, // REQUIRED
        ]);

        /** @var Stream $stream */
        $stream = $result->get('AudioStream');
        $data = $this->decodeJsonStream($stream);
        $data = array_map(
            function ($value) {
                $result = [
                    'time' => $value['time'],
                    'viseme' => $value['value'],
                ];

                return $result;
            },
            $data
        );

        return $data;
    }

    /**
     * @param string $text
     *
     * @param string $voiceId
     *
     * @return array
     */
    public function textToVoice($text, $voiceId)
    {
        $client = $this->getClient();

        $result = $client->synthesizeSpeech([
            'OutputFormat' => 'pcm', // REQUIRED
            'SpeechMarkTypes' => [],
            'Text' => $text, // REQUIRED
            'VoiceId' => $voiceId, // REQUIRED
        ]);

        /** @var Stream $stream */
        $stream = $result->get('AudioStream');

        $file = $stream->getContents();

        return $file;
    }

    /**
     * @param Stream $stream
     *
     * @return array
     */
    protected function decodeJsonStream(Stream $stream)
    {
        $regex = '/\{[^\{\}]*\}/m';
        preg_match_all($regex, $stream->getContents(), $matches);
        $json = '['.implode(',', current($matches)).']';

        return json_decode($json, true);
    }
}
