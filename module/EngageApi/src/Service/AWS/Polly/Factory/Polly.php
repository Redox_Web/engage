<?php
namespace EngageApi\Service\AWS\Polly\Factory;

use Aws\Sdk;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Polly
 *
 * @author Shahnovsky Alex
 */
class Polly implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return $service
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new \EngageApi\Service\AWS\Polly\Polly();
        /** @var Sdk $aws */
        $sdk = $container->get(Sdk::class);
        /** @var \Aws\Polly\PollyClient $client */
        $client = $sdk->createPolly();
        $service->setClient($client);

        return $service;
    }
}
