<?php
namespace EngageApi\Service;

/**
 * Interface TextToVoiceInterface
 *
 * @author Shahnovsky Alex
 */
interface TextToVoiceInterface
{
    /**
     * @param string $message
     *
     * @param string $voiceId
     *
     * @return string
     */
    public function textToVoice($message, $voiceId);
}
