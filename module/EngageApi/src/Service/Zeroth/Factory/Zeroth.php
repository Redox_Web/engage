<?php
namespace EngageApi\Service\Zeroth\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\Http\Client;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Zeroth
 *
 * @author Shahnovsky Alex
 */
class Zeroth implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \EngageApi\Service\Zeroth\Zeroth()
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new \EngageApi\Service\Zeroth\Zeroth();
        $client = new Client();
        $client->setAdapter(Client\Adapter\Curl::class);
        $service->setHttpClient($client);

        return $service;
    }
}
