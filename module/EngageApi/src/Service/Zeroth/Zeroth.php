<?php
namespace EngageApi\Service\Zeroth;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Uri\Http;

/**
 * Class Zeroth
 *
 * @author Shahnovsky Alex
 */
class Zeroth
{
    /**
     * Url to ask Zeroth AI
     *
     * @var string
     */
    private $url = 'https://prod.zerothindustries.com/api/characters/34bd64b75f24d52d01826c822c4647ee/ask';

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param Client $httpClient
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $question
     *
     * @return array
     */
    public function getAnswer($question)
    {
        $requestData = [];
        $requestData['question'] = $question;
        $requestData['questionId'] = '';
        $requestData['source'] = 'player';
        $requestData['options'] = [
            'confidence' => '0.5',
            'include_alts' => true,
            'personality' => false,
        ];
        $requestData['previous'] = [];

        $uri = new Http($this->getUrl());
        $request = new Request();
        $request->setUri($uri);
        $request->setMethod(Request::METHOD_POST);
        $request->setPost(new \Zend\Stdlib\Parameters($requestData));

        $response = $this->getHttpClient()->send($request);
        $responseData = (array)json_decode($response->getBody());

        return $responseData;
    }
}
