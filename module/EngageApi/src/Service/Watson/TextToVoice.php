<?php
namespace EngageApi\Service\Watson;

use EngageApi\Service\TextToVoiceInterface;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Uri\Http;

/**
 * Class Watson
 *
 * @author Shahnovsky Alex
 */
class TextToVoice implements TextToVoiceInterface
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param Client $httpClient
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $message
     *
     * @param string $voiceId
     *
     * @return string
     */
    public function textToVoice($message, $voiceId)
    {
        $url = 'https://stream.watsonplatform.net/text-to-speech/api/v1/synthesize?accept=audio/wav&text='.$message.'&voice='.$voiceId;
        $uri = new Http($url);
        $request = new Request();
        $request->setUri($uri);
        $request->getHeaders()->addHeaders(array(
            'X-Watson-Learning-Opt-Out' => 'true',
        ));
        $request->setMethod(Request::METHOD_GET);

        return $this->getHttpClient()->send($request)->getBody();
    }
}
