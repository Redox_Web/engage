<?php
namespace EngageApi\Service\Watson;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Uri\Http;

/**
 * Class Watson
 *
 * @author Shahnovsky Alex
 */
class VoiceToText
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param Client $httpClient
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $fileContent
     *
     * @return \Zend\Http\Response
     */
    public function voiceToText($fileContent)
    {
        $url = 'https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?timestamps=true&word_alternatives_threshold=0.9';
        $uri = new Http($url);
        $request = new Request();
        $request->setUri($uri);
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->buffer($fileContent);
        $mimeType = 'audio/wav';
        $request->getHeaders()->addHeaders(array(
            'Content-Type' => $mimeType,
        ));
        $request->setMethod(Request::METHOD_POST);
        $request->setContent($fileContent);

        return $this->getHttpClient()->send($request);
    }
}
