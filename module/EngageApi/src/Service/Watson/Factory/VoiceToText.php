<?php
namespace EngageApi\Service\Watson\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use RedoxWeb\WTL\Crud\Factory\Exception\InvalidConfig;
use Zend\Http\Client;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Watson
 *
 * @author Shahnovsky Alex
 */
class VoiceToText implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \EngageApi\Service\Watson\VoiceToText
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new \EngageApi\Service\Watson\VoiceToText();
        $client = new Client();
        $client->setAdapter(Client\Adapter\Curl::class);
        $config = $container->get('Config');
        if (!isset($config['watson']['voiceToText'])) {
            throw new InvalidConfig('Watson voice to text service config required.');
        }
        $client->getAdapter()->setCurlOption(CURLOPT_USERNAME, $config['watson']['voiceToText']['username']);
        $client->getAdapter()->setCurlOption(CURLOPT_PASSWORD, $config['watson']['voiceToText']['password']);

        $service->setHttpClient($client);

        return $service;
    }
}
