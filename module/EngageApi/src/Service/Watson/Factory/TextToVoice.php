<?php
namespace EngageApi\Service\Watson\Factory;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use RedoxWeb\WTL\Crud\Factory\Exception\InvalidConfig;
use Zend\Http\Client;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Watson
 *
 * @author Shahnovsky Alex
 */
class TextToVoice implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \EngageApi\Service\Watson\TextToVoice
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new \EngageApi\Service\Watson\TextToVoice();
        $client = new Client();
        $client->setAdapter(Client\Adapter\Curl::class);
        $config = $container->get('Config');
        if (!isset($config['watson']['textToVoice'])) {
            throw new InvalidConfig('Watson text to voice service config required.');
        }
        $client->getAdapter()->setCurlOption(CURLOPT_USERNAME, $config['watson']['textToVoice']['username']);
        $client->getAdapter()->setCurlOption(CURLOPT_PASSWORD, $config['watson']['textToVoice']['password']);

        $service->setHttpClient($client);

        return $service;
    }
}
