<?php
namespace EngageApi\Controller;

use EngageApi\Service\AWS\Polly\Polly;
use EngageApi\Service\TextToVoiceInterface;
use EngageApi\Service\Watson\TextToVoice;
use EngageApi\Service\Watson\VoiceToText;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class IndexController
 *
 * @author Shahnovsky Alex
 */
class IndexController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * TtV strategies
     * @var array
     */
    private $ttvStrategies = [
        'watson' => TextToVoice::class,
        'polly' => Polly::class,
    ];

    private $ttvDefaultVoices = [
        'watson' => 'en-US_AllisonVoice',
        'polly' => 'Joanna',
    ];

    /**
     * Polly Text length limit
     * @var int
     */
    private $polyPhonemesLimit = 1400;

    /**
     * @param string $strategy
     *
     * @return TextToVoiceInterface
     */
    private function getTTVStrategy($strategy)
    {
        $service = $this->getServiceLocator()->get($this->ttvStrategies[$strategy]);
        return $service;
    }

    /**
     * @param string $strategy
     *
     * @return string
     */
    private function getTTVVoiceId($strategy)
    {
        return $this->ttvDefaultVoices[$strategy];
    }

    /**
     * @return JsonModel
     */
    public function voiceToTextAction()
    {
        $voice = file_get_contents('php://input');

        /** @var VoiceToText $service */
        $service = $this->getServiceLocator()->get(VoiceToText::class);
        $json = $service->voiceToText($voice)->getBody();

        $this->getResponse()->getHeaders()->addHeaderLine(
            "Access-Control-Allow-Origin: *"
        );

        return new JsonModel((array)json_decode($json));
    }

    /**
     * @return JsonModel
     */
    public function askAIAction()
    {
        $strategy = 'polly';
        $disablePhonemes = false;
        $json = file_get_contents('php://input');
        $decoded = (array)json_decode($json);
        $message = $decoded['message'];
        if (isset($decoded['strategy']) && in_array($decoded['strategy'], array_keys($this->ttvStrategies))) {
            $strategy = $decoded['strategy'];
        }
        if (isset($decoded['disablePhonemes']) && $decoded['disablePhonemes'] == 'true') {
            $disablePhonemes = true;
        }

        /** @var \EngageApi\Service\Zeroth\Zeroth $zerothService */
        $zerothService = $this->getServiceLocator()->get(\EngageApi\Service\Zeroth\Zeroth::class);
        $answer = $zerothService->getAnswer($message);

        if (strlen($answer['answer']) > $this->polyPhonemesLimit) {
            $disablePhonemes = true;
        }

        if (isset($decoded['ttvVoice']) && $decoded['ttvVoice']) {
            $ttvVoice = $decoded['ttvVoice'];
        } else {
            $ttvVoice = $this->getTTVVoiceId($strategy);
        }

        if ($strategy == 'polly' && strlen($answer['answer']) > $this->polyPhonemesLimit) {
            $file = '';
        } else {
            $textToVoiceService = $this->getTTVStrategy($strategy);
            $file = $textToVoiceService->textToVoice($answer['answer'], $ttvVoice);
        }

        if ($disablePhonemes) {
            $phonemes = [];
        } else {
            if (isset($decoded['phomenesVoice']) && $decoded['phomenesVoice']) {
                $phomenesVoice = $decoded['phomenesVoice'];
            } else {
                $phomenesVoice = $this->getTTVVoiceId('polly');
            }

            /** @var Polly $pollyService */
            $pollyService = $this->getServiceLocator()->get(Polly::class);
            $phonemes = $pollyService->getPhonemes($answer['answer'], $phomenesVoice);
        }

        $responseData = [
            'strategy' => $strategy,
            'message' => $answer['answer'],
            'phonemes' => $phonemes,
            'animation' => '',
            'audio_stream' => base64_encode($file),
        ];
        $this->getResponse()->getHeaders()->addHeaderLine(
            "Access-Control-Allow-Origin: *"
        );

        return new JsonModel($responseData);
    }
}
