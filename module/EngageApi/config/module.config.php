<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace EngageApi;

use EngageApi\Service\AWS\Polly\Polly;
use EngageApi\Service\AWS\S3\S3;
use EngageApi\Service\TextToVoiceInterface;
use EngageApi\Service\Watson\TextToVoice;
use EngageApi\Service\Watson\VoiceToText;
use EngageApi\Service\Zeroth\Zeroth;
use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'engage-api-voice-to-text' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/voiceToText',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'voiceToText',
                    ],
                    'route_plugins' => [],
                ],
                'may_terminate' => true,
            ],
            'engage-api-ask-ai' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/askAI',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'askAI',
                    ],
                    'route_plugins' => [],
                ],
                'may_terminate' => true,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            \EngageApi\Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => [
        'initializers' => [],
        'abstract_factories' => [
        ],
        'aliases' => [
            TextToVoiceInterface::class => TextToVoice::class,
        ],
        'factories' => [
            TextToVoice::class => \EngageApi\Service\Watson\Factory\TextToVoice::class,
            VoiceToText::class => \EngageApi\Service\Watson\Factory\VoiceToText::class,
            Zeroth::class => \EngageApi\Service\Zeroth\Factory\Zeroth::class,
            Polly::class => \EngageApi\Service\AWS\Polly\Factory\Polly::class,
            S3::class => \EngageApi\Service\AWS\S3\Factory\S3::class,
        ],
        'delegators' => [
        ],
        'shared' => [],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'EngageApi' => [
                    "controllers" => [
                        \EngageApi\Controller\IndexController::class => [
                            'default' => [], // anyone can access
                        ],
                    ],
                ],
            ],
        ],
    ],
];
