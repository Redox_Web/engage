<?php
/**
 * @author Oleg Melnic
 */

namespace PersonalOffice\Factory\Controller\Plugin;

use Application\Factory\Navigation\Partial;
use Application\Navigation\ContainerProvider;
use Application\Controller\Plugins\InjectMenu;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class TopMenu
 * @package FrontEnd\Factory\Controller\Plugin
 */
class PersonalOfficeMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     * @return InjectMenu
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Application\Navigation\PluginManager $pluginManager */
        $pluginManager = $container->get('injectedNavigationPluginManager');
        $service = new InjectMenu();
        $service->setMenuContainer($this->getMenuContainer($container));
        $service->setPluginManager($pluginManager);

        return $service;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return null|\Zend\Navigation\Page\AbstractPage
     */
    private function getMenuContainer(ContainerInterface $container)
    {
        $mainNavigationName = Partial::MAIN_NAVIGATION_PREFIX.'aep';

        /** @var \Zend\Navigation\AbstractContainer $containerMenu */
        $containerMenu = $container->get($mainNavigationName);

        return $containerMenu->findBy('id', ContainerProvider::PERSONAL_OFFICE_MENU);
    }
}
