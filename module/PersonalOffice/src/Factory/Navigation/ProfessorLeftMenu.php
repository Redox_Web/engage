<?php
/**
 * @author Oleg Melnic
 */

namespace PersonalOffice\Factory\Navigation;

use Application\Factory\Exception\InvalidArgument;
use Application\Navigation\Menu\ProfessorLeft;
use Application\Service\Course\Chapter;
use Application\Service\Course\Item\Item;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class HelpLeftMenu
 * @package FrontEnd\Factory\Navigation
 */
class ProfessorLeftMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProfessorLeft();

        if (isset($options['course']) && $options['course'] instanceof \Application\Entity\Course\Course) {
            $service->setCourse($options['course']);
        } else {
            throw new InvalidArgument(
                'Course param must be set and implement "\Application\Entity\Course\Course"'
            );
        }

        $service->setServiceChapter($container->get(Chapter::class));
        $service->setServiceItem($container->get(Item::class));

        return $service($container, \Zend\Navigation\Navigation::class);
    }
}
