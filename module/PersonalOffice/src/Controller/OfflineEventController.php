<?php
namespace PersonalOffice\Controller;

use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\Event\Offline as OfflineEventService;
use Application\Service\User\User as UserService;

/**
 * Class OfflineEventController
 * Class for manage offline events
 */
class OfflineEventController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * item action.
     *
     * @return ViewModel
     */
    public function itemAction()
    {
        $alias = $this->params()->fromRoute('alias');
        $event = $this->getServiceLocator()->get(OfflineEventService::class)
            ->findByAlias($alias);

        if (is_null($event)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $userId = $this->auth()->getIdentity()->getIdentity();
        $users = $this->getServiceLocator()->get(UserService::class)
            ->getUsersByOfflineEvents($event->getIdentity(), $userId);
        $isSubscribed = $this->getServiceLocator()->get(UserService::class)
            ->isSubscribedToOfflineEvent($event->getIdentity(), $userId);

        return new ViewModel([
            'event' => $event,
            'users' => $users,
            'isSubscribed' => $isSubscribed
        ]);
    }

    /**
     * subscribe action
     */
    public function subscribeAction()
    {
        $id = $this->params()->fromRoute('id');
        $event = $this->getServiceLocator()->get(OfflineEventService::class)
            ->find($id);

        if (is_null($event)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $userId = $this->auth()->getIdentity()->getIdentity();
        $this->getServiceLocator()->get(UserService::class)
             ->subscribeOfflineEvent($event, $userId);

        $this->redirect()->toRoute('personal-office/offline-event',
                        ['alias' => $event->getAlias()]);
    }

    /**
     * unsubscribe action
     */
    public function unsubscribeAction()
    {
        $id = $this->params()->fromRoute('id');
        $event = $this->getServiceLocator()->get(OfflineEventService::class)
            ->find($id);

        if (is_null($event)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $userId = $this->auth()->getIdentity()->getIdentity();
        $this->getServiceLocator()->get(UserService::class)
            ->unsubscribeOfflineEvent($event, $userId);

        $this->redirect()->toRoute('personal-office/offline-event',
            ['alias' => $event->getAlias()]);
    }

    /**
     * list action
     * list of personal offline events that user is subscribed
     *
     */
    public function listAction()
    {
        $userId = $this->auth()->getIdentity()->getIdentity();
        $upcomingEvents = $this->getServiceLocator()->get(OfflineEventService::class)
            ->getUpcomingOfflineEvents($userId);
        $pastEvents = $this->getServiceLocator()->get(OfflineEventService::class)
            ->getPastOfflineEvents($userId);

        return new ViewModel([
            'upcomingEvents' => $upcomingEvents,
            'pastEvents' => $pastEvents,
        ]);
    }
}
