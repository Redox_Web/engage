<?php
namespace PersonalOffice\Controller;

use Application\Form\CourseCreateForm;
use Application\Form\CourseForm;
use Application\Service\Course\Course;
use Application\Controller\AbstractActionController;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Course as CourseService;

/**
 * Class CourseController
 * Class for manage courses
 */
class CourseController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function indexAction()
    {
        $this->topMenu('courseTopMenu');
        return new ViewModel();
    }

    /**
     *
     * professors courses
     *
     */
    public function myCoursesAction()
    {
        $courses = $this->getServiceLocator()->get(CourseService::class)
            ->getAllCoursesByParam(['author' => $this->auth()->getIdentity()]);

        return new ViewModel([
            'courses' => $courses
        ]);
    }

    /**
     * item action
     * page for viewing one course
     *
     */
    public function itemAction()
    {
        $alias = $this->params()->fromRoute('alias');
        $course = $this->getServiceLocator()->get(CourseService::class)
            ->getOne(array('alias' => $alias));

        if (is_null($course)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->leftMenu('courseLeftMenu', ['course' => $course]);

        return new ViewModel([
            'course' => $course
        ]);
    }

    public function courseInfoAction()
    {
        /** @var CourseCreateForm $form */
        $form = $this->getServiceLocator()->get(CourseForm::class);

        /** @var Course $service */
        $service = $this->getServiceLocator()->get(Course::class);

        // Getting course id
        $itemId = $this->params()->fromRoute('id', -1);

        // Course finding.
        /**
         * @var \Application\Entity\Course\Course $course
         */
        $course = $service->find($itemId);
        if (!$course) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $this->personalOfficeMenu('professorTopMenu', ['course' => $course]);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['teacher'] = $course->getTeacher()->getIdentity();
            $data['author'] = $course->getAuthor()->getIdentity();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $service->update($itemId, $data);

                return $this->redirect()->toRoute(
                    'personal-office/course/info',
                    [
                        'id' => $itemId,
                    ]
                );
            }
        } else {
            $data = [
                'name' => $course->getName(),
                'description' => $course->getDescription(),
                'categories' => $service->convertCategoriesToArray($course),
                'execution' => $course->getExecution()->getValue(),
                'howPass' => $course->getHowPass(),
                'institution' => $course->getInstitution(),
                'teacher' => $course->getTeacher(),
                'difficulty' => $course->getDifficulty()->getValue(),
                'skillsToAchieve' => $course->getSkillsToAchieve(),
            ];

            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
        ]);

        return $view;
    }

    public function courseCreateAction()
    {
        /** @var CourseCreateForm $form */
        $form = $this->getServiceLocator()->get(CourseCreateForm::class);

        /** @var Course $service */
        $service = $this->getServiceLocator()->get(Course::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $form->setData($data);

            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $data['teacher'] = $this->auth()->getIdentity();
                $data['author'] = $this->auth()->getIdentity();
                $entity = $service->firstSave($data);

                return $this->redirect()->toRoute(
                    'personal-office/course/info',
                    ['id' => $entity->getIdentity()]
                );
            }
        }

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }
}
