<?php
namespace PersonalOffice\Controller;

use Application\Service\Subscription\SubscriptionPackage;
use Payment\Service\Payment;
use CirclicalUser\Service\AccessService;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class SubscriptionController
 * Class for manage profile subscriptions
 * @package Frontend\Controller
 */
class SubscriptionController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $accessService;

    /**
     * profilePlans action.
     *
     * @return ViewModel
     */
    public function profilePlansAction()
    {
        $this->accessService = $this->getServiceLocator()->get(AccessService::class);
        /** @var Payment $payment */
        $payment = $this->getServiceLocator()->get(Payment::class);

        /** @var SubscriptionPackage $subscriptionPackages */
        $subscriptionPackages = $this->getServiceLocator()->get(SubscriptionPackage::class);
        $allPackages = $subscriptionPackages->getAllPackages();

        /** @var \Payment\Entity\Payment $paymentUser */
        $paymentUser = $payment->getLastPayment($this->auth()->getIdentity());

        if ($this->accessService->isAllowed('user', 'logout') && $paymentUser) {
            $view = new ViewModel([
                'nextPaid' => $paymentUser->getNextPaid(),
            ]);
            $view->setTemplate('personal-office/subscription/paid-plans');
            return $view;
        } else {
            return new ViewModel([
                'allPackages' => $allPackages,
            ]);
        }
    }
}
