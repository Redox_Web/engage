<?php
namespace PersonalOffice\Controller;

use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\User\User as UserService;
use Application\Form\UserForm;
use Application\Form\PasswordChangeForm;
use Application\Service\User\Social\Social as SocialService;
use Application\Service\User\Social\User as SocialUserService;
use Application\Entity\User\State as UserState;
use RedoxWeb\WTL\Entity\File\ObjectType;
use RedoxWeb\WTL\Entity\File\Image as ImageFile;

/**
 * Class ProfileController
 *
 * @method \Zend\Http\Request getRequest()
 */
class ProfileController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * personalData action.
     *
     * @return ViewModel
     */
    public function personalDataAction()
    {
        // Create form.
        $form = $form = $this->getServiceLocator()->get(UserForm::class);

        // Get current user ID.
        $userId = (int) $this->auth()->getIdentity()->getIdentity();

        // Validate input parameter
        if ($userId<0) {
            $this->notFoundAction();
        }

        // Find the existing user in the database.
        $user = $this->getServiceLocator()->get(UserService::class)
            ->getOne(array('identity' => $userId));
        if ($user == null) {
            $this->notFoundAction();
        }

        // Check whether this user is a POST request.
        if ($this->getRequest()->isPost()) {

            // Get POST data.
            $data = $this->params()->fromPost();
            $data['identity'] = $user->getIdentity();

            // Update existing user.
            $result = $this->getServiceLocator()->get(UserService::class)->changeData($user->getIdentity(), $data);
            if (is_array($result) && isset($result['errors']) && $result['errors']) {
                $form->setData($data);
                $form->setMessages($result['errors']);
            } else {
                // Save user avatar
                $avatarInfo = $this->getRequest()->getFiles('avatar');
                /** @var ImageFile $savedImage */
                $savedImage = $this->image()->uploadImage($avatarInfo, ObjectType::TYPE_CLIENT_AVATAR);
                $data['avatar'] = $savedImage;
                $this->getServiceLocator()->get(UserService::class)->saveAvatar($result->getIdentity(), $data);
                return $this->redirect()->toRoute('personal-office/personal-data');
            }

        } else {
            $data = [
                'email' => $user->getEmail(),
                'fullName' => $user->getFullName(),
                'description' => $user->getDescription(),
                'birthday' => $user->getBirthday()->format('Y-m-d'),
                'address' => $user->getAddress(),
                'timezone' => $user->getTimezone(),
                'sex' => $user->getSex(),
                'description' => $user->getDescription(),
            ];

            $form->setData($data);
        }

        // Render the view template.
        return new ViewModel([
            'form' => $form,
            'user' => $user
        ]);
    }

    /**
     * changePassword action.
     *
     * @return ViewModel
     */
    public function changePasswordAction()
    {
        /** @var PasswordChangeForm $form */
        $form = $form = $this->getServiceLocator()->get(PasswordChangeForm::class);

        // Get current user.
        $user = $this->auth()->getIdentity();

        // Validate input parameter
        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Check whether this user is a POST request.
        if ($this->getRequest()->isPost()) {

            // Get POST data.
            $data = $this->params()->fromPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getServiceLocator()->get(UserService::class)->changePassword($user, $data);

                return $this->redirect()->toRoute('personal-office/personal-data');
            }
        }

        // Render the view template.
        return new ViewModel([
            'form' => $form,
        ]);
    }

    /**
     * linkedAccounts action.
     *
     * @return ViewModel
     */
    public function linkedAccountsAction()
    {
        $socialAccounts = $this->getServiceLocator()->get(SocialService::class)
            ->getAllSocialServices();

        // Render the view template.
        return new ViewModel([
            'socialAccounts' => $socialAccounts,
        ]);
    }

    /**
     * deleteAccount action.
     * Delete current user account
     */
    public function deleteAccountAction()
    {
        // Get current user.
        $user = $this->auth()->getIdentity();

        // Validate input parameter
        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Update existing user.
        $data['state'] = UserState::createFromScalar(UserState::INACTIVE);
        $result = $this->getServiceLocator()->get(UserService::class)->deleteAccount($user, $data);
        if (is_array($result) && isset($result['errors']) && $result['errors']) {
            $this->getResponse()->setStatusCode(404);
            return;
        } else {
            // Redirect the user to "index" page.
            return $this->redirect()->toRoute('frontend/login');
        }
    }
}
