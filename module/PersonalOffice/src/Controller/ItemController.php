<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/27/17
 */
namespace PersonalOffice\Controller;

use Application\Controller\AbstractActionController;
use Application\Service\Course\Chapter;
use Application\Service\Course\Item\Item;
use Doctrine\ORM\EntityManager;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;
use Application\Service\Course\Item\Item as ItemService;
use Application\Form\Course\Item\ItemAbstractForm;

/**
 * Class ItemController
 * Class for manage items
 */
class ItemController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $formMapper = [
        'article' => \Application\Form\Course\Item\Strategy\ArticleForm::class,
        'video' => \Application\Form\Course\Item\Strategy\VideoForm::class,
        'audio' => \Application\Form\Course\Item\Strategy\AudioForm::class,
        'quiz' => \Application\Form\Course\Item\Strategy\QuizForm::class,
        'image' => \Application\Form\Course\Item\Strategy\ImageForm::class,
    ];

    /**
     * item action
     * page for viewing one item
     */
    public function itemAction()
    {
        $alias = $this->params()->fromRoute('alias');
        $item = $this->getServiceLocator()->get(ItemService::class)
            ->getOne(array('alias' => $alias));

        if (is_null($item)) {
            $this->notFoundAction();
        }

        $this->leftMenu('courseLeftMenu', ['course' => $item->getChapter()->getCourse()]);

        return new ViewModel([
            'item' => $item
        ]);
    }

    public function createAction()
    {
        /** @var Chapter $courseService */
        $chapterService = $this->getServiceLocator()->get(Chapter::class);

        // Getting chapter id
        $chapterId = $this->params()->fromRoute('id', -1);

        /**
         * @var \Application\Entity\Course\Chapter $chapter
         */
        $chapter = $chapterService->find($chapterId);
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
        } else {
            $data['type'] = 'article';
        }


        $this->personalOfficeMenu('professorTopMenu', ['course' => $chapter->getCourse()]);
        $this->personalOfficeLeftMenu('professorLeftMenu', ['course' => $chapter->getCourse()]);

        /** @var ItemAbstractForm $form */
        $form = $this->getServiceLocator()->get($this->getFormMapper($data['type']));

        /** @var Item $service */
        $service = $this->getServiceLocator()->get(Item::class);

        if ($this->getRequest()->isPost()) {
            $type = $data['type'];

            $file = $this->params()->fromFiles();
            if ($file[$type]) {
                $data[$type] = $this->S3file()->uploadFile($file[$type]);
            }
            $data['chapter'] = $chapter;

            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                $data['type'] = $type;
                $entity = $service->create($data);

                return $this->redirect()->toRoute(
                    'personal-office/item/update',
                    [
                        'id' => $entity->getIdentity(),
                    ]
                );
            } else {
                pred($form->getMessages());
            }
        }

        $form->setData($data);

        $view = new ViewModel([
            'form' => $form
        ]);
        $view->setTemplate('personal-office/item/create');

        return $view;
    }

    public function updateAction()
    {
        /** @var Item $service */
        $service = $this->getServiceLocator()->get(Item::class);
        $itemId = $this->params()->fromRoute('id', -1);

        /**
         * @var \Application\Entity\Course\Item\ItemAbstract $item
         */
        $item = $service->find($itemId);

        /** @var EntityManager $entityManager */
        $entityManager = $this->getServiceLocator()->get(EntityManager::class);
        $entityType = $entityManager->getClassMetadata(get_class($item))->discriminatorValue;

        /** @var Form $form */
        $form = $this->getServiceLocator()->get($this->getFormMapper($entityType));

        $this->personalOfficeMenu('professorTopMenu', ['course' => $item->getChapter()->getCourse()]);
        $this->personalOfficeLeftMenu('professorLeftMenu', ['course' => $item->getChapter()->getCourse()]);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();
            $type = $data['type'] = $entityType;
            $file = $this->params()->fromFiles();

            if ($file[$type]) {
                $data[$type] = $this->S3file()->uploadFile($file[$type]);
            }

            if (empty($data[$type])) {
                $function = 'get'.$type;
                $data[$type] = $item->$function();
            }

            $data['chapter'] = $item->getChapter();
            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                $data['type'] = $type;
                $service->update($itemId, $data);

                return $this->redirect()->toRoute(
                    'personal-office/item/update',
                    [
                        'id' => $item->getIdentity(),
                    ]
                );
            }
        } else {
            $data = $service->extract($itemId);
            $data['startDate'] = $data['startDate']->format('d-m-Y');
            $data['type'] = $entityType;
            $form->setData($data);
        }

        $view = new ViewModel([
            'form' => $form,
            'item' => $item,
        ]);
       // $view->setTemplate($this->getTemplateMapper($entityType).'/update');

        return $view;
    }

    public function deleteAction()
    {
        $postId = $this->params()->fromRoute('id', -1);
        if ($postId<1) {
            return $this->notFoundAction();
        }
        $this->getServiceLocator()->get(Item::class)->delete($postId);
        return $this->redirect()->toRoute('frontend');
    }

    private function getFormMapper($type)
    {
        return $this->formMapper[$type];
    }
}
