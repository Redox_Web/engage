<?php
/**
 * Фабрика для создание обьекта Сервиса для отправки емаила
 */

namespace Application\Factory\ServiceMail;

use Application\ServiceMail\Send as SendMail;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Mail\Transport\TransportInterface;

class Send implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var TransportInterface $transport */
        $transport = $container->get('mail.transport');

        $sendMail = new SendMail();
        $sendMail->setTransport($transport);

        return $sendMail;
    }
}
