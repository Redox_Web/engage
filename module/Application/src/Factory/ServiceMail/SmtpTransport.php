<?php
/**
 * Фабрика для создание обьекта SmtpTransport для отправки емаила
 */
namespace Application\Factory\ServiceMail;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Factory\Exception\InvalidConfig;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

class SmtpTransport implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        if (!isset($config['redox']['mail']['transport']['smtp'])) {
            throw new InvalidConfig('Неверный конфиг. Нет индексов redox.mail.transport.smtp');
        }

        $mailConfig = $config['redox']['mail']['transport']['smtp'];
        if (!is_array($mailConfig)) {
            throw new InvalidConfig(
                sprintf('Неверный конфиг. mailConfig должен быть массивом. Передан "%s"', $mailConfig)
            );
        }

        if (!isset($mailConfig['name']) || !isset($mailConfig['host']) || !isset($mailConfig['port'])) {
            throw new InvalidConfig('Неверный конфиг. Опции должны содержать ключи name, host, port');
        }
        $transport = new Smtp();
        $options   = new SmtpOptions($mailConfig);
        $transport->setOptions($options);
        return $transport;
    }
}
