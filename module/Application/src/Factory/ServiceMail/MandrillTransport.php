<?php
/**
 * @author Oleg Melnic
 */

namespace Application\Factory\ServiceMail;

use Application\ServiceMail\Transport\Mandrill;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Factory\Exception\InvalidConfig;

/**
 * Class MandrillTransport
 *
 * @copyright (c) 2017, Redox
 */
class MandrillTransport implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        if (!isset($config['redox']['mail']['transport']['mandrill'])) {
            throw new InvalidConfig('Неверный конфиг. Нет индексов redox.mail.transport.mandrill');
        }

        $mailConfig = $config['redox']['mail']['transport']['mandrill'];
        if (!is_array($mailConfig)) {
            throw new InvalidConfig(
                sprintf('Неверный конфиг. mailConfig должен быть массивом. Передан "%s"', $mailConfig)
            );
        }

        $mandrill = new \Mandrill($mailConfig['api-key']);

        $transport = new Mandrill();
        $transport->setServiceMandrill($mandrill);

        return $transport;
    }
}
