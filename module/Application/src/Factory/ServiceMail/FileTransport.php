<?php
/**
 * @author Oleg Melnic
 */

namespace Application\Factory\ServiceMail;

use Interop\Container\ContainerInterface;
use Zend\Mail\Transport\File;
use Zend\Mail\Transport\FileOptions;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Factory\Exception\InvalidConfig;

class FileTransport implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        if (!isset($config['redox']['mail']['transport']['file'])) {
            throw new InvalidConfig('Неверный конфиг. Нет индексов redox.mail.transport.file');
        }

        $mailConfig = $config['redox']['mail']['transport']['file'];
        if (!is_array($mailConfig)) {
            throw new InvalidConfig(
                sprintf('Неверный конфиг. mailConfig должен быть массивом. Передан "%s"', $mailConfig)
            );
        }
        if (!isset($mailConfig['path'])) {
            throw new InvalidConfig('Неверный конфиг. Опции должны содержать ключ path');
        }
        $transport = new File();
        $options   = new FileOptions($mailConfig);
        $transport->setOptions($options);

        return $transport;
    }
}
