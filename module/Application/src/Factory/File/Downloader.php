<?php
namespace Application\Factory\File;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Downloader
 *
 * @author Shahnovsky Alex
 */
class Downloader implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \RedoxWeb\WTL\File\Transfer\Downloader\Downloader
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new \RedoxWeb\WTL\File\Transfer\Downloader\Downloader();

        $md5 = $container->get('s3NameGenerator');
        $service->setGenerator($md5);

        $saver = $container->get('s3Saver');
        $service->setSaver($saver);

        $transport = new \RedoxWeb\WTL\File\Transport\Http();
        $service->setTransport($transport);

        return $service;
    }
}
