<?php
namespace Application\Factory\File\NameGenerator;

use RedoxWeb\WTL\Crud\Factory\Exception\InvalidConfig;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class S3
 *
 * @author Shahnovsky Alex
 */
class S3 implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \Application\File\NameGenerator\S3
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $generator = new \Application\File\NameGenerator\S3();
        $config = $container->get('Config');
        if (!isset($config['aws'])) {
            throw new InvalidConfig('Amazon AWS service config required.');
        }
        $generator->setDirectories($config['aws']['directories']);

        return $generator;
    }
}
