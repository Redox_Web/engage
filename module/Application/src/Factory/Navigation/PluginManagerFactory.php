<?php

namespace Application\Factory\Navigation;

use Application\Navigation\PluginManager as MenuPluginManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PluginManagerFactory
 *
 * @copyright (c) 2016, DevelopmentAid
 */
class PluginManagerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        $configArray = [];
        if (isset($config['redox_injected_navigation'])) {
            $configArray = $config['redox_injected_navigation'];
        }

        $manager = new MenuPluginManager($container, $configArray);
        return $manager;
    }
}
