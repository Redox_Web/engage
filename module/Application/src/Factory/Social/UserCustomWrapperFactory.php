<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/14/17
 */

namespace Application\Factory\Social;

use Application\Entity\Social\SocialCustomAuthUserWrapper;
use SocialConnect\Common\Entity\User;

/**
 * This class works as factory to get an Object implementing the UserInterface
 *
 * @category  HybridAuth
 * @author    Andreas Heigl<andreas@heigl.org>
 * @copyright ©2012-2013 Andreas Heigl
 * @license   http://www.opesource.org/licenses/mit-license.php MIT-License
 * @version   0.0
 * @since     11.01.13
 * @link      https://github.com/heiglandreas/HybridAuth
 */
class UserCustomWrapperFactory
{
    /**
     * Create the user-Proxy according to the given User-Object
     *
     * @return UserInterface
     * @throws \UnexpectedValueException
     */
    public function factory($userObject)
    {
        switch (get_class($userObject))
        {
            case User::class:
                return new SocialCustomAuthUserWrapper($userObject);
                break;
            case 'Hybridauth\\Entity\\Profile':
            case 'Hybridauth\\Entity\\Twitter\\Profile':
                $userProxy = new HybridAuthUserWrapper();
                $userProxy->setUser($userObject);
                return $userProxy;
                break;
            default:
                return new DummyUserWrapper();
        }

        throw new \UnexpectedValueException(sprintf(
            'The given Object could not be found. Found "%s" instead',
            get_Class($userObject)
        ));
    }
}
