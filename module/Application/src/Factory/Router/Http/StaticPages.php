<?php
namespace Application\Factory\Router\Http;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use StaticPages\Service\Page;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class StaticPages
 *
 * @author Shahnovsky Alex
 */
class StaticPages implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \Application\Router\Http\StaticPages
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options['routes'] = [];
        /** @var \StaticPages\Service\Page $pageService */
        $pageService = $container->get(Page::class);
        $pages = $pageService->getActiveBySite($options['site']);
        foreach ($pages as $page) {
            $options['routes'][$page->getAlias()] = $page->getUrl();
        }
        $router = \Application\Router\Http\StaticPages::factory($options);
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        $entityManager->clear(\StaticPages\Entity\Page::class);

        return $router;
    }
}
