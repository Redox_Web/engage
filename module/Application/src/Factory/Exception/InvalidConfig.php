<?php
/**
 * @author Melnic Oleg
 */

namespace Application\Factory\Exception;

class InvalidConfig extends \RuntimeException implements ExceptionInterface
{
}
