<?php
namespace Application\Factory\Captcha\Adapter;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use RedoxWeb\WTL\Factory\Exception\InvalidConfig;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ReCaptcha
 *
 * @author Shahnovsky Alex
 */
class ReCaptcha implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \Zend\Captcha\ReCaptcha
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        if (!isset($config['captcha']['re-captcha'])) {
            throw new InvalidConfig('ReCaptcha config required.');
        }

        $adapter = new \Zend\Captcha\ReCaptcha(
            [
                'privKey' => $config['captcha']['re-captcha']['privKey'],
                'pubKey' => $config['captcha']['re-captcha']['pubKey'],
            ]
        );

        return $adapter;
    }
}
