<?php
namespace Application\Factory\View\Helper\Image;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImageThumb implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Application\View\Helper\ImageThumb
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $helper = new \Application\View\Helper\ImageThumb();
        $helper->setImageService(
            $container->get(\RedoxWeb\WTL\Service\File\Image::class)
        );
        return $helper;
    }
}
