<?php
namespace Application\Factory\Controller\Plugin;

use EngageApi\Service\AWS\S3\S3;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class S3File
 *
 * @author Shahnovsky Alex
 */
class S3File implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return \Application\Controller\Plugins\S3File
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $plugin = new \Application\Controller\Plugins\S3File();
        $plugin->setS3Service($container->get(S3::class));
        $plugin->setDownloader($container->get('s3Downloader'));

        return $plugin;
    }
}
