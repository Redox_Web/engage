<?php

namespace Application\InputFilterBuilder\Form;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class PasswordChange
 *
 * @copyright (c) 2017, Redox
 */
class PasswordChange extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('new_password', true, 'base.password');
        $this->getInputFilterBuilder()->add('confirm_new_password', true, 'base.password',
            [],
            [
                [
                    'name'    => 'Identical',
                    'options' => [
                        'token' => 'new_password',
                    ],
                ]
            ]);

        $this->getInputFilterBuilder()->add(
            'oldPassword',
            true,
            'base.password',
            [],
            [
                [
                    'name' => 'OldPasswordCheck'
                ]
            ]);
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
