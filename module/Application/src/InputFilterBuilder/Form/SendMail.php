<?php
namespace Application\InputFilterBuilder\Form;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class SendMail
 *
 * @author Oleg Melnic
 */
class SendMail extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'mail-body',
            true,
            'base.string'
        );
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
