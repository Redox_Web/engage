<?php

namespace Application\InputFilterBuilder\Form;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class PasswordReset
 *
 * @copyright (c) 2017, Redox
 */
class PasswordReset extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('email', true, 'base.email');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
