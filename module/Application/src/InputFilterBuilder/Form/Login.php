<?php
namespace Application\InputFilterBuilder\Form;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Login
 *
 * @author Shahnovsky Alex
 */
class Login extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'email',
            true,
            'base.email'
        );

        $this->getInputFilterBuilder()->add('password', true, 'base.password');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
