<?php

namespace Application\InputFilterBuilder\User\Social;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Class Social User
 *
 * @copyright (c) 2017, Redox
 */
class Social extends BuilderAbstract implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('alias', true, 'base.text');
        $this->getInputFilterBuilder()->add('url', true, 'base.uri');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
