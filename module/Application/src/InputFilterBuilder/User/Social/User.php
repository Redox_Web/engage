<?php

namespace Application\InputFilterBuilder\User\Social;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Class Social User
 *
 * @copyright (c) 2017, Redox
 */
class User extends BuilderAbstract implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'user',
            true,
            null,
            [],
            [
                [
                    'name'    => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => \Application\Entity\User\UserAbstract::class
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add(
            'social',
            true,
            null,
            [],
            [
                [
                    'name'    => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => \Application\Entity\User\Social\Social::class
                    ]
                ]

            ]
        );
        $this->getInputFilterBuilder()->add('key', true, 'base.text');
        $this->getInputFilterBuilder()->add('createdAt', false, 'base.datetime');
        $this->getInputFilterBuilder()->add(
            'state',
            true,
            null,
            [
                [
                    'name' => 'ToNull'
                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => \Application\Entity\User\Social\State::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => \Application\Entity\User\Social\State::class
                    ]
                ]
            ]
        );
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
