<?php

namespace Application\InputFilterBuilder\User;

use DoctrineModule\Validator\UniqueObject;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;
use Zend\Filter\ToNull;

/**
 * Class User
 *
 * @copyright (c) 2017, Redox
 */
abstract class UserAbstract extends BuilderAbstract implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'email',
            true,
            'base.email',
            [],
            [
                [
                    'name' => 'UniqueObject',
                    'options' => [
                        'object_repository' => $this->getEntityManager()
                            ->getRepository('Application\Entity\User\UserAbstract'),
                        'object_manager' => $this->getEntityManager(),
                        'fields' => ['email'],
                        'use_context' => true,
                        'messages' => [
                            UniqueObject::ERROR_OBJECT_NOT_UNIQUE => "This e-mail '%value%' is already registered"
                        ],
                        'token' => array(
                            'id' => 'identity'
                        ),
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add('password', true, 'base.password');
        $this->getInputFilterBuilder()->add('fullName', false, 'base.user.fullname');

        $this->getInputFilterBuilder()->add(
            'birthday',
            true,
            'base.datetime',
            [],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \DateTime::class
                    ]
                ],
                [
                    'name' => 'AgeGreaterDiff',
                ]
            ]
        );

        $this->getInputFilterBuilder()->add('address', false, 'base.text');
        $this->getInputFilterBuilder()->add('timezone', false, 'base.string');
        $this->getInputFilterBuilder()->add('description', false, 'base.text');

        $this->getInputFilterBuilder()->add(
            'sex',
            false,
            null,
            [
                [
                    'name' => 'ToNull',
                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\User\Sex',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\User\Sex'
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add(
            'state',
            false,
            null,
            [
                [
                    'name' => 'ToNull',
                    'options' => [
                        'type' => ToNull::TYPE_INTEGER,
                    ]

                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\User\State',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\User\State'
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add(
            'avatar',
            false,
            null,
            [],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'RedoxWeb\WTL\Entity\File\Image'
                    ]
                ]
            ]
        );
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
