<?php
/**
 * Фильтра Student
 *
 * @author Олег Мельник
 */
namespace Application\InputFilterBuilder\User\Strategy;

use Application\InputFilterBuilder\User\UserAbstract;

/**
 * Class Student
 * @package Application\InputFilterBuilder\User\Strategy
 */
class Student extends UserAbstract
{
    /**
     * Подключение дополнительных инпут фильтров
     *
     * @return void
     */
    protected function customInit()
    {
    }

    /**
     * Вернуть имя сущности для parent
     *
     * @return string
     */
    protected function getParentEntityName()
    {
        return \Application\Entity\User\Student::class;
    }
}
