<?php
/**
 * Фильтра admin
 *
 * @author Олег Мельник
 */
namespace Application\InputFilterBuilder\User\Strategy;

use Application\InputFilterBuilder\User\UserAbstract;

/**
 * Class Admin
 * @package Application\InputFilterBuilder\User\Strategy
 */
class Admin extends UserAbstract
{
    /**
     * Подключение дополнительных инпут фильтров
     *
     * @return void
     */
    protected function customInit()
    {
    }

    /**
     * Вернуть имя сущности для parent
     *
     * @return string
     */
    protected function getParentEntityName()
    {
        return \Application\Entity\User\Admin::class;
    }
}
