<?php

namespace Application\InputFilterBuilder\Event;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Offline
 *
 * @copyright (c) 2017, Redox
 */
class Offline extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('heldDate', true, 'base.datetime');
        $this->getInputFilterBuilder()->add(
            'image',
            true,
            'base.text',
            [],
            [
                [
                    'name' => 'ImageExtension',
                    'options' => [
                        'extension' => ['jpg', 'jpeg', 'png', 'gif']
                    ]
                ],
                [
                    'name' => 'ImageFileSize',
                    'options' => [
                        'min' => '1kB',
                        'max' => '2MB'
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add('description', true, 'base.text');
        $this->getInputFilterBuilder()->add('price', true, 'base.float');
        $this->getInputFilterBuilder()->add(
            'state',
            true,
            null,
            [
                [
                    'name' => 'ToNull'
                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\Event\State',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\Event\State'
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add('alias', true, 'base.text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
