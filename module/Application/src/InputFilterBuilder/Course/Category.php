<?php
namespace Application\InputFilterBuilder\Course;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Category
 *
 * @copyright (c) 2017, Redox
 */
class Category extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('alias', true, 'base.alias');
        $this->getInputFilterBuilder()->add(
            'state',
            false,
            null,
            [
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\Course\CategoryState',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\Course\CategoryState'
                    ]
                ]
            ]
        );
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
