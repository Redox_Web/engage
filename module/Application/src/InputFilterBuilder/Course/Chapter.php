<?php
namespace Application\InputFilterBuilder\Course;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Chapter
 *
 * @copyright (c) 2017, Redox
 */
class Chapter extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('description', false, 'base.text');
        $this->getInputFilterBuilder()->add('alias', false, 'base.alias');
        $this->getInputFilterBuilder()->add(
            'course',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\Course\Course::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \Application\Entity\Course\Course::class,
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add(
            'state',
            false,
            null,
            [
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\Course\ChapterState',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\Course\ChapterState'
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add('position', false, 'base.position');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
