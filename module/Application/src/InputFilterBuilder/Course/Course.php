<?php
namespace Application\InputFilterBuilder\Course;

use Application\Entity\ValueObject\Period;
use RedoxWeb\WTL\Filter\ByIdentity;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Course
 *
 * @copyright (c) 2017, Redox
 */
class Course extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('description', false, 'base.text');
        $this->getInputFilterBuilder()->add('alias', false, 'base.alias');
        $this->getInputFilterBuilder()->add(
            'execution',
            true,
            null,
            [
                [
                    'name' => 'ToNull'
                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => Period::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => Period::class,
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add('howPass', false, 'base.text');
        /*$this->getInputFilterBuilder()->add(
            'categories',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\Course\Category::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \Application\Entity\Course\Category::class,
                    ]
                ]
            ]
        );*/
        $this->getInputFilterBuilder()->add(
            'author',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\User\UserAbstract::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \Application\Entity\User\Professor::class,
                    ]
                ]
            ]
        );

        $this->getInputFilterBuilder()->add(
            'teacher',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\User\UserAbstract::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \Application\Entity\User\Professor::class,
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add(
            'difficulty',
            true,
            null,
            [
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => 'Application\Entity\Course\Difficulty',
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => 'Application\Entity\Course\Difficulty'
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->add('institution', false, 'base.text');
        $this->getInputFilterBuilder()->add('skillsToAchieve', false, 'base.text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
