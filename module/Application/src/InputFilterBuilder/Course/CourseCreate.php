<?php
namespace Application\InputFilterBuilder\Course;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Course
 *
 * @copyright (c) 2017, Redox
 */
class CourseCreate extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
