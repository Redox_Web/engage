<?php
namespace Application\InputFilterBuilder\Course\Item\Strategy;

use Application\InputFilterBuilder\Course\Item\ItemAbstract;

/**
 * Class Image
 *
 * @author Paladi Vitalie
 */
class Image extends ItemAbstract
{
    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    protected function customInit()
    {
        $this->getInputFilterBuilder()->add('image', true, 'base.uri');
    }
}
