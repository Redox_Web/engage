<?php
namespace Application\InputFilterBuilder\Course\Item\Strategy;

use Application\InputFilterBuilder\Course\Item\ItemAbstract;

/**
 * Class Document
 *
 * @author Paladi Vitalie
 */
class Document extends ItemAbstract
{
    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    protected function customInit()
    {
        $this->getInputFilterBuilder()->add('document', true, 'base.uri');

        $this->getInputFilterBuilder()->add(
            'document_file',
            false,
            null,
            [],
            [
                ['name'    => 'FileUploadFile'],
                [
                    'name'    => 'FileMimeType',
                    'options' => [
                        'mimeType'  => ['application/pdf'],
                    ]
                ],
            ]
        );
    }
}
