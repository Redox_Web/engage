<?php
namespace Application\InputFilterBuilder\Course\Item\Strategy;

use Application\InputFilterBuilder\Course\Item\ItemAbstract;

/**
 * Class Article
 *
 * @author Shahnovsky Alex
 */
class Audio extends ItemAbstract
{
    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    protected function customInit()
    {
        $this->getInputFilterBuilder()->add('audio', true, 'base.uri');
    }
}
