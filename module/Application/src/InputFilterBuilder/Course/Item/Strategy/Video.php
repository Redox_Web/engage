<?php
namespace Application\InputFilterBuilder\Course\Item\Strategy;

use Application\InputFilterBuilder\Course\Item\ItemAbstract;

/**
 * Class Video
 *
 * @author Shahnovsky Alex
 */
class Video extends ItemAbstract
{
    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    protected function customInit()
    {
        $this->getInputFilterBuilder()->add(
            'video',
            true,
            'base.uri'
        );
    }
}
