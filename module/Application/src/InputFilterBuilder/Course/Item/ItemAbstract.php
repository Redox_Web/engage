<?php
namespace Application\InputFilterBuilder\Course\Item;

use Application\Entity\Course\Item\State;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class ItemAbstract
 *
 * @author Shahnovsky Alex
 */
abstract class ItemAbstract extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.string');
        $this->getInputFilterBuilder()->add('description', false, 'base.text');
        $this->getInputFilterBuilder()->add('isFree', true, 'base.boolean');
        $this->getInputFilterBuilder()->add(
            'chapter',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\Course\Chapter::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => \Application\Entity\Course\Chapter::class,
                    ]
                ]
            ]
        );
        $this->getInputFilterBuilder()->mergeRaw(
            'isFree',
            [
                'allow_empty' => true,
            ]
        );
        $this->getInputFilterBuilder()->add('startDate', true, 'base.datetime');

        $this->getInputFilterBuilder()->add(
            'state',
            true,
            null,
            [
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => State::class,
                    ],
                ],
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => State::class,
                    ],
                ],
            ]
        );

        $this->customInit();
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }

    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    abstract protected function customInit();
}
