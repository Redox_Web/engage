<?php
namespace Application\InputFilterBuilder\Subscription;

use Application\Entity\Subscription\SubscriptionPackage;
use Application\Entity\User\UserAbstract;
use Application\Service\User\User;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Class Payment
 *
 * @author Topuria Evgheni
 */
class ActiveSubscriptionPackage extends BuilderAbstract implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'user',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => UserAbstract::class,
                    ],
                ],
                array(
                    'name' => 'FilterCreateEntity',
                    'options' => array(
                        'service_name' => User::class,
                    )
                ),
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'className' => UserAbstract::class,
                    ],
                ],
            ]
        );

        $this->getInputFilterBuilder()->add(
            'subscriptionPackage',
            true,
            null,
            [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => SubscriptionPackage::class,
                    ],
                ],
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => SubscriptionPackage::class,
                    ],
                ],
            ]
        );

        $this->getInputFilterBuilder()->add('startDate', true, 'base.datetime');
        $this->getInputFilterBuilder()->add('endDate', true, 'base.datetime');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
