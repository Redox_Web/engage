<?php
namespace Application\InputFilterBuilder\Subscription;

use Application\Entity\ValueObject\Period;
use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Payment
 *
 * @author Topuria Evgheni
 */
class SubscriptionPackage extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add(
            'name',
            true,
            'base.text'
        );

        $this->getInputFilterBuilder()->add(
            'price',
            true,
            'base.float'
        );

        $this->getInputFilterBuilder()->add(
            'period',
            true,
            null,
            [
                [
                    'name' => 'ToNull'
                ],
                [
                    'name' => 'CreateFromScalar',
                    'options' => [
                        'object_name' => Period::class,
                    ]
                ]
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => false,
                        'className' => Period::class,
                    ]
                ]
            ]
        );
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
