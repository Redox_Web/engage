<?php

namespace Application\Navigation;

use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\Exception\InvalidArgumentException;

/**
 * Class PluginManager
 *
 * @copyright (c) 2016, DevelopmentAid
 */
class PluginManager extends AbstractPluginManager
{

    /**
     * Validate the plugin
     *
     * Checks that the filter loaded is either a valid callback or an instance
     * of FilterInterface.
     *
     * @param  mixed $plugin
     *
     * @return void
     * @throws \Zend\ServiceManager\Exception\ServiceNotFoundException if invalid
     */
    public function validate($plugin)
    {
        if (!$plugin instanceof \Zend\Navigation\AbstractContainer) {
            throw new InvalidArgumentException(
                sprintf(
                    'Plugin ot type %s is invalid. Must implement "\Zend\Navigation\AbstractContainer"',
                    is_object($plugin) ? get_class($plugin) : gettype($plugin)
                )
            );
        }
    }
}
