<?php
namespace Application\Navigation\Menu;

/**
 * Class CourseLeft
 * @package Application\Navigation\Menu
 */
class CourseLeft extends AbstractMenu
{

    /** @var  \Application\Entity\Course\Course */
    protected $course;

    /** @var  \Application\Service\Course\Chapter */
    protected $chapterService;

    /** @var  \Application\Service\Course\Item\Item */
    protected $itemService;

    /**
     * @return \Application\Entity\Course\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param \Application\Entity\Course\Course $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return \Application\Service\Course\Chapter
     */
    public function getChapterService()
    {
        return $this->chapterService;
    }

    /**
     * @param \Application\Service\Course\Chapter $chapterService
     */
    public function setChapterService($chapterService)
    {
        $this->chapterService = $chapterService;
    }

    /**
     * @return \Application\Service\Course\Item\Item
     */
    public function getItemService()
    {
        return $this->itemService;
    }

    /**
     * @param \Application\Service\Course\Item\Item $itemService
     */
    public function setItemService($itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * Подготовить конфигурацию меню навигации
     * @return array
     */
    protected function prepareConfiguration()
    {
        return [
            'course' => [
                'label' => $this->getCourse()->getName(),
                'id'    => $this->getCourse()->getIdentity(),
                'route' => 'personal-office/course/index',
                'pages' => $this->getChaptersStructure(),
                'institution' => $this->getCourse()->getInstitution(),
                'progress' => '75%',
            ]
        ];
    }

    /**
     * @return array
     */
    private function getChaptersStructure()
    {
        /** @var \Application\Entity\Course\Chapter[]|null $chapters */
        $chapters = $this->getChapterService()->getAllChaptersByCourse($this->getCourse()->getIdentity());
        $chapters_arr = [];
        if ($chapters) {
            foreach ($chapters as $chapter) {
                $chapters_arr['chapter-'.$chapter->getAlias()] = [
                    'label' => $chapter->getName(),
                    'id'    => "chapterIdentity-".$chapter->getIdentity(),
                    'route' => 'personal-office/chapter',
                    'params' => [
                        'alias' => $chapter->getAlias()
                    ],
                    'pages' => $this->getItemsStructure($chapter),
                ];
            }
        }

        return $chapters_arr;
    }

    /**
     * @param \Application\Entity\Course\Chapter $chapter
     * @return array
     */
    private function getItemsStructure($chapter)
    {
        $items_arr = [];
        /** @var \Application\Entity\Course\Item\Item[]|null $chapters */
        $issues = $this->getItemService()->getAllIssuesByChapter($chapter->getIdentity());
        if ($issues) {
            $i = 1;
            foreach ($issues as $item) {
                $items_arr['issue-'.$item->getAlias()] = $this->getItemData($item, $i, 'issue', $issues);;
                $i++;
            }
        }
        $quizes = $this->getItemService()->getAllQuizesByChapter($chapter->getIdentity());
        if ($quizes) {
            $i = 1;
            foreach ($quizes as $item) {
                $items_arr['quiz-'.$item->getAlias()] = $this->getItemData($item, $i, 'quiz', $quizes);
                $i++;
            }
        }

        return $items_arr;
    }

    /**
     * @param \Application\Entity\Course\Item\Item $item
     * @param integer $counter
     * @param string $type
     * @param \Application\Entity\Course\Item\Item[] $items
     * @return array
     */
    private function getItemData($item, $counter, $type, $items)
    {
        if ($type=='issue') {
            $typeName = "Issue ".$counter;
        } elseif($type=='quiz') {
            $typeName = count($items) > 1 ? "Quiz ".$counter :"Final Quiz";
        }
        return [
            'label' => $item->getName(),
            'id'    => $type."Identity-".$item->getIdentity(),
            'route' => 'personal-office/item',
            'params' => [
                'alias' => $item->getAlias()
            ],
            'pages' => [],
            'typeName' => $typeName
        ];
    }
}
