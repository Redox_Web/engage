<?php

namespace Application\Navigation\Menu;

use Interop\Container\ContainerInterface;
use Zend\Mvc\Application;
use Zend\Navigation\Service\DefaultNavigationFactory;

/**
 * Class AbstractMenu
 * @package Application\Navigation\Menu
 */
abstract class AbstractMenu extends DefaultNavigationFactory
{
    /**
     * @param ContainerInterface $container
     * @return array
     */
    protected function getPages(ContainerInterface $container)
    {
        /** @var Application $application */
        $application = $container->get('Application');
        $routeMatch = $application->getMvcEvent()->getRouteMatch();
        $router = $application->getMvcEvent()->getRouter();
        $pages = $this->getPagesFromConfig($this->prepareConfiguration());
        $request = $application->getRequest();

        return $this->injectComponents($pages, $routeMatch, $router, $request);
    }

    /**
     * Подготовить конфигурацию меню навигации
     * @return array
     */
    abstract protected function prepareConfiguration();
}
