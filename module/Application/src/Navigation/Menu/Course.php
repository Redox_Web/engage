<?php

namespace Application\Navigation\Menu;

use Application\Service\Course\Category;
use Application\Service\Course\Chapter;

/**
 * Class Help
 * @package Application\Navigation\Menu
 */
class Course extends AbstractMenu
{
    /**
     * @var Category
     */
    private $category;

    /**
     * @var Chapter
     */
    private $chapter;

    /** @var  \Application\Service\Course\Course */
    protected $courseService;

    /**
     * Подготовить конфигурацию меню навигации
     * @return array
     */
    protected function prepareConfiguration()
    {
        return [
            'course' => [
                'route' => 'personal-office/course/index',
                'pages' => $this->getCategoryStructure(),
                'params' => []
            ]
        ];
    }

    private function getCategoryStructure()
    {
        $structure = [
            'category' => [
                'label' => 'Category',
                'uri' => '/personal-office/profile-plans/',
                'params' => []
            ]
        ];

        /** @var \Application\Entity\Course\Category $category */
        /*foreach ($this->courseService->findAll() as $category) {
            $faqCategory = [
                'label' => $category->getName(),
                'route' => 'http/front-end/faq_category',
                'params' => [
                    'language' => $this->getLanguage()->getAlias(),
                    'category' => $category->getAlias()
                ],
            ];

            $structure['faq']['pages'][$category->getAlias()] = $faqCategory;
        }*/

        return $structure;
    }
}
