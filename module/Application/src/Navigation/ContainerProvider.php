<?php
/**
 * Class ContainerProvider
 *
 * @author  nonick <web@nonick.name>
 * @package Application\Navigation
 */

namespace Application\Navigation;

use Application\Factory\Navigation\Partial;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Navigation\AbstractContainer;

class ContainerProvider implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const MAIN_MENU_ALIAS = 'main_menu';
    const TOP_MENU_ALIAS = 'top_menu';
    const PERSONAL_OFFICE_MENU = 'personal_office_menu';
    const PERSONAL_OFFICE_LEFT_MENU = 'personal-office-left-menu';
    const LEFT_MENU_ALIAS = 'left_menu';
    const LEFT_MENU_PROFILE_ALIAS = 'left_menu_profile';
    const LEFT_MENU_BACKEND_ALIAS = 'left_menu_backend';

    /**
     * Получить главное меню
     * @return AbstractContainer
     */
    public function getMainMenu()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::MAIN_MENU_ALIAS);
    }

    /**
     * Получить верхнее меню
     * @return AbstractContainer
     */
    public function getTopMenu()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::TOP_MENU_ALIAS);
    }

    /**
     * Получить левое меню
     * @return AbstractContainer
     */
    public function getLeftMenu()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::LEFT_MENU_ALIAS);
    }

    /**
     * Get professors menu
     * @return AbstractContainer
     */
    public function getPersonalOfficeMenu()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::PERSONAL_OFFICE_MENU);
    }

    /**
     * Get professors left menu
     * @return AbstractContainer
     */
    public function getPersonalOfficeLeftMenu()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::PERSONAL_OFFICE_LEFT_MENU);
    }

    /**
     * Получить верхнее меню
     * @return AbstractContainer
     */
    public function getLeftMenuProfile()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::LEFT_MENU_PROFILE_ALIAS);
    }

    /**
     * @return AbstractContainer
     */
    public function getFullNavigation()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.Partial::FULL_NAVIGATION);
    }

    /**
     * Get backend left menu
     * @return AbstractContainer
     */
    public function getLeftMenuBackend()
    {
        return $this->getServiceLocator()->get(Partial::SERVICE_PREFIX.self::LEFT_MENU_BACKEND_ALIAS);
    }
}
