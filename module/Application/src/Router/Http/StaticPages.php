<?php
namespace Application\Router\Http;

use Zend\Router\Http\RouteInterface;
use Zend\Router\Http\RouteMatch;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Router\Exception;

/**
 * Class StaticPages
 *
 * @author Shahnovsky Alex
 */
class StaticPages implements RouteInterface
{
    /**
     * RouteInterface to match.
     *
     * @var string
     */
    protected $routes;

    /**
     * Default values.
     *
     * @var array
     */
    protected $defaults;

    /**
     * List of assembled parameters.
     *
     * @var array
     */
    protected $assembledParams = array();

    /**
     * Create a new literal route.
     *
     * @param  array $routes
     * @param  array  $defaults
     */
    public function __construct(array $routes, array $defaults = array())
    {
        $this->routes    = $routes;
        $this->defaults = $defaults;
    }

    /**
     * factory(): defined by RouteInterface interface.
     *
     * @see    \Zend\Router\RouteInterface::factory()
     * @param  array|\Traversable $options
     * @return StaticPages
     * @throws Exception\InvalidArgumentException
     */
    public static function factory($options = array())
    {
        if ($options instanceof \Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                __METHOD__ . ' expects an array or Traversable set of options'
            );
        }

        if (!isset($options['routes'])) {
            throw new Exception\InvalidArgumentException('Missing "routes" in options array');
        }

        if (!isset($options['defaults'])) {
            $options['defaults'] = array();
        }

        return new static($options['routes'], $options['defaults']);
    }

    /**
     * match(): defined by RouteInterface interface.
     *
     * @see    \Zend\Router\RouteInterface::match()
     * @param  Request      $request
     * @param  integer|null $pathOffset
     * @return RouteMatch|null
     */
    public function match(Request $request, $pathOffset = null)
    {
        if (!method_exists($request, 'getUri')) {
            return;
        }

        $uri  = $request->getUri();
        $path = $uri->getPath();

        foreach ($this->routes as $alias => $route) {
            if ($pathOffset !== null) {
                if ($pathOffset >= 0 && strlen($path) >= $pathOffset && !empty($route)) {
                    if (strpos($path, $route, $pathOffset) === $pathOffset
                        && strlen($path) - $pathOffset == strlen($route)) {
                        return new RouteMatch(array_merge($this->defaults, ['alias' => $alias]), strlen($route));
                    }
                }
            }

            if ($path === $route) {
                return new RouteMatch(array_merge($this->defaults, ['alias' => $alias]), strlen($route));
            }
        }

        return;
    }

    /**
     * assemble(): Defined by RouteInterface interface.
     *
     * @see    \Zend\Router\RouteInterface::assemble()
     * @param  array $params
     * @param  array $options
     * @return mixed
     */
    public function assemble(array $params = array(), array $options = array())
    {
        $this->assembledParams = array();

        return $this->routes[$params['alias']];
    }

    /**
     * getAssembledParams(): defined by RouteInterface interface.
     *
     * @see    RouteInterface::getAssembledParams
     * @return array
     */
    public function getAssembledParams()
    {
        return array();
    }
}
