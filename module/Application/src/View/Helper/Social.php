<?php
namespace Application\View\Helper;

use Application\Entity\User\Social\Social as SocialEntity;
use Application\Service\User\Social\User as SocialUserService;
use Zend\View\Helper\AbstractHelper;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class Social
 *
 * @author Paladi Vitalie
 */
class Social extends AbstractHelper
{
    /**
     * @var SocialEntity
     */
    protected $social;

    /**
     * @var SocialUserService
     */
    protected $socialUserService;

    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @return SocialEntity
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param SocialEntity $social
     */
    public function setSocial($social)
    {
        $this->social = $social;
    }

    /**
     * @return SocialUserService
     */
    public function getSocialUserService()
    {
        return $this->socialUserService;
    }

    /**
     * @param SocialUserService $socialUserService
     */
    public function setSocialUserService(SocialUserService $socialUserService)
    {
        $this->socialUserService = $socialUserService;
    }

    /**
     * @param SocialEntity $social
     * @return SocialEntity
     */
    public function __invoke($social)
    {
        $this->setSocial($social);
        return $this;
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return array
     */
    public function getSocialInfo()
    {
        $user = $this->getAuthenticationService()->getIdentity();
        $providerSocial = strtolower($this->getSocial()->getName());

        // check if exist linked account for this social service for current user
        $socialUser = $this->getSocialUserService()->findByUserAndSocial($user, $this->getSocial());
        $connected = (boolean) ($socialUser && $socialUser->getState()->getValue());

        $info = ["provider" => $providerSocial, "iconImgSrc" => "themes/engage/img/facebook.svg", "textLink" => "Facebook account", "connected" => $connected];
        if ($providerSocial=="google") {
            $info['iconImgSrc'] = "themes/engage/img/google.svg";
            $info['textLink'] = "Google account";
        }

        return $info;
    }
}
