<?php
namespace Application\View\Helper;

use Application\Entity\User\UserAbstract;
use CirclicalUser\Service\AuthenticationService;
use Zend\View\Helper\AbstractHelper;

/**
 * Class Auth
 *
 * @author Oleg Melnic
 */
class Auth extends AbstractHelper
{
    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return \CirclicalUser\Provider\UserInterface|null|UserAbstract
     */
    public function __invoke()
    {
        return $this->getAuthenticationService()->getIdentity();
    }
}
