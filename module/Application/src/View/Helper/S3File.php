<?php
namespace Application\View\Helper;

use Application\Entity\Course\Item\S3Interface;
use Zend\View\Helper\AbstractHelper;

/**
 * Class S3File
 *
 * @author Oleg Melnic
 */
class S3File extends AbstractHelper
{
    /**
     * @param S3Interface $item
     *
     * @return string
     */
    public function __invoke(S3Interface $item)
    {
        $fileName = $item->getFileName();
        $url = null;
        if ($fileName) {
            $url = $this->getView()->url('get-file', ['fileName' => base64_encode($fileName)]);
        }
        return $url;
    }
}
