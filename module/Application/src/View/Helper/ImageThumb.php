<?php
namespace Application\View\Helper;

use RedoxWeb\WTL\Entity\File\Image;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ImageThumb
 * @method \Application\View\Renderer getView()
 */
class ImageThumb extends AbstractHelper
{
    /**
     * @var \RedoxWeb\WTL\Service\File\Image
     */
    protected $imageService;

    /**
     * @param Image $image
     * @param  int  $width
     * @param  int  $height
     * @param  string  $defaultImageSrc
     * @return string
     */
    public function __invoke($image, $width, $height, $defaultImageSrc = null)
    {
        if (null === $image) {
            return $defaultImageSrc;
        }
        $thumbNail = $this->getImageService()->getThumbNail($image, $width, $height);

        if (null === $thumbNail) {
            return $defaultImageSrc;
        }

        return $this->getView()->image($thumbNail)->getImgSrc();
    }

    /**
     * @return \RedoxWeb\WTL\Service\File\Image
     */
    public function getImageService()
    {
        return $this->imageService;
    }

    /**
     * @param \RedoxWeb\WTL\Service\File\Image $imageService
     */
    public function setImageService($imageService)
    {
        $this->imageService = $imageService;
    }
}
