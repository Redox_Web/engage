<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;
use RedoxWeb\WTL\Entity\File\ImageAbstract as ImageEntity;
use RedoxWeb\WTL\StdLib\Image\ImageSrc as ImageSrcBuilder;

/**
 * Class Image
 * @method Renderer getView()
 */
class Image extends AbstractHelper
{

    /**
     * @var ImageEntity
     */
    private $image;

    /**
     * @var ImageSrcBuilder
     */
    private $imageSrcBuilder;

    /**
     * @param ImageEntity|null $image
     * @return Image
     */
    public function __invoke(ImageEntity $image = null)
    {
        $this->setImage($image);
        return $this;
    }

    /**
     * @return ImageEntity
     */
    private function getImage()
    {
        return $this->image;
    }

    /**
     * @param ImageEntity|null $image
     */
    public function setImage(ImageEntity $image = null)
    {
        $this->image = $image;
    }

    /**
     * @return ImageSrcBuilder
     */
    private function getImageSrcBuilder()
    {
        return $this->imageSrcBuilder;
    }

    /**
     * @param ImageSrcBuilder $imageSrcBuilder
     */
    public function setImageSrcBuilder(ImageSrcBuilder $imageSrcBuilder)
    {
        $this->imageSrcBuilder = $imageSrcBuilder;
    }

    /**
     * @return string
     */
    public function getImgSrc()
    {
        return $this->getImageSrcBuilder()->getImageSrc($this->getImage());
    }
}
