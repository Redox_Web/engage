<?php

namespace Application\View\Helper;

use Application\Navigation\ContainerProvider;
use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;

/**
 * Class AepNavigation
 *
 * @author  nonick <web@nonick.name>
 * @package Application\View\Helper
 *
 * @method Renderer getView()
 */
class AepNavigation extends AbstractHelper
{
    /**
     * @var ContainerProvider
     */
    protected $containerProvider;

    /**
     * @var bool
     * Defines if the helper should show breadcrumbs or not
     */
    protected $showBreadCrumbs = true;

    /**
     * @return ContainerProvider
     */
    public function getContainerProvider()
    {
        return $this->containerProvider;
    }

    /**
     * @param ContainerProvider $containerProvider
     */
    public function setContainerProvider(ContainerProvider $containerProvider)
    {
        $this->containerProvider = $containerProvider;
    }

    /**
     * @return AepNavigation
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * Получить главное меню
     * @param string $partial
     * @return string
     */
    public function getMainMenu($partial = 'main_menu')
    {
        $container = $this->getContainerProvider()->getMainMenu();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * Получить главное меню
     * @param string $partial
     * @return string
     */
    public function getTopMenu($partial = 'top_menu')
    {
        $container = $this->getContainerProvider()->getTopMenu();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * Получить главное меню
     * @param string $partial
     * @return string
     */
    public function getLeftMenu($partial = 'left_menu')
    {
        $container = $this->getContainerProvider()->getLeftMenu();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * Get professors menu
     * @param string $partial
     * @return string
     */
    public function getPersonalOfficeMenu($partial = 'professor_menu')
    {
        $container = $this->getContainerProvider()->getPersonalOfficeMenu();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * Get professors left menu
     * @param string $partial
     * @return string
     */
    public function getPersonalOfficeLeftMenu($partial = 'professor_left_menu')
    {
        $container = $this->getContainerProvider()->getPersonalOfficeLeftMenu();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * Get left menu for user profile page
     * @param string $partial
     * @return string
     */
    public function getLeftMenuProfile($partial = 'left_menu_profile')
    {
        $container = $this->getContainerProvider()->getLeftMenuProfile();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }

    /**
     * @return string
     */
    public function getBreadCrumbs($partial = 'application/menu/breadcrumbs')
    {
        if (!$this->isShowBreadCrumbs()) {
            return '';
        }
        $container = $this->getContainerProvider()->getFullNavigation();

        return $this->getView()->navigation($container)->breadcrumbs()
            ->renderPartial(null, $partial);
    }

    /**
     * @return boolean
     */
    public function isShowBreadCrumbs()
    {
        return $this->showBreadCrumbs;
    }

    /**
     * @param boolean $showBreadCrumbs
     * @return AepNavigation
     */
    public function setShowBreadCrumbs($showBreadCrumbs)
    {
        $this->showBreadCrumbs = $showBreadCrumbs;

        return $this;
    }

    /**
     * Get backend left menu
     * @param string $partial
     * @return string
     */
    public function getLeftMenuBackend($partial = 'left_menu_backend')
    {
        $container = $this->getContainerProvider()->getLeftMenuBackend();

        if (is_null($partial)) {
            return (string)$this->getView()->navigation($container)->menu();
        }
        return $this->getView()->navigation($container)->menu()->renderPartial(null, $partial);
    }
}
