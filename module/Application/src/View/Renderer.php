<?php

namespace Application\View;

use Application\Entity\Course\Item\S3Interface;
use Application\Entity\User\UserAbstract;
use Application\View\Helper\S3File;
use CirclicalUser\Provider\ResourceInterface;
use RedoxWeb\WTL\Entity\File\Image;
use Zend\View\Renderer\PhpRenderer;

/**
 * Class Renderer
 *
 * @author  nonick <web@nonick.name>
 * @package Application\View
 *
 * @method \Application\View\Helper\AepNavigation aepNavigation()
 * @method bool isAllowed(ResourceInterface|string $resource, string $action)
 * @method UserAbstract authIdentity()
 * @method string s3File(S3Interface $item)
 * @method \Seo\View\Helper\SeoHelper seoHelper(...$arguments)
 * @method string image(Image $image = null)
 * @method string imageThumb(Image $image, $width, $height, $defaultImageSrc = null)
 */
class Renderer extends PhpRenderer
{

}
