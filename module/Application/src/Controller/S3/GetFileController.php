<?php
namespace Application\Controller\S3;

use Application\Controller\AbstractActionController;
use Aws\Exception\AwsException;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;

/**
 * Class GetFileController
 *
 * @author Shahnovsky Alex
 */
class GetFileController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function getFileAction()
    {
        $encodedFileName = $this->params()->fromRoute('fileName');
        $fileName = base64_decode($encodedFileName);

        /** @var \EngageApi\Service\AWS\S3\S3 $service */
        $service = $this->getServiceLocator()->get(\EngageApi\Service\AWS\S3\S3::class);
        try {
            $result = $service->openFile($fileName);
        } catch (AwsException $e) {
            return $this->notFoundAction();
        }

        $file = $result->get('Body')->getContents();
        $mimeType = $result->get('ContentType');
        header('Content-Description: File Transfer');
        header("Content-Transfer-Encoding: binary");
        header('Content-Type: '.$mimeType);
        header('Content-length: ' . strlen($file));
        header('Content-Disposition: inline');
        echo $file;
    }
}
