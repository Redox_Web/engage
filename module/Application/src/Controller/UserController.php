<?php
namespace Application\Controller;

use Application\Entity\User\State;
use Application\Service\User\User;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\UserForm;

/**
 * This controller is responsible for user management (adding, editing,
 * viewing users and changing user's password).
 */
class UserController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * This action displays a page allowing to sign up a new user.
     */
    public function signUpAction()
    {
        // Sign up user form
        /** @var UserForm $form */
        $form = $this->getServiceLocator()->get(UserForm::class);

        /** @var User $service */
        $service = $this->getServiceLocator()->get(User::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['state'] = State::ACTIVE;
            $data['identity'] = '';

            $form->setData($data);
            $form->isValid();

            // Validate form
            if ($form->isValid()) {
                // Get filtered and validated data
                $data = $form->getData();
                $data['type'] = $service::TYPE_STUDENT;

                // Sign up user.
                $user = $service->create($data);

                // Redirect to "view" page
                return $this->redirect()->toRoute(
                    'home',
                    [
                        'id'=>$user->getIdentity()
                    ]
                );
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }
}


