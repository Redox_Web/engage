<?php
/**
 * @author Oleg Melnic
 */

namespace Application\Controller;

use Application\Controller\Plugins\S3File;
use CirclicalUser\Controller\Plugin\AuthenticationPlugin;
use Zend\Http\Request;

/**
 * Class AbstractActionController
 * @package Application\Controller
 *
 * @method \Zend\Navigation\AbstractContainer topMenu($pluginName, $options = [])
 * @method \Zend\Navigation\AbstractContainer personalOfficeMenu($pluginName, $options = [])
 * @method \Zend\Navigation\AbstractContainer leftMenu($pluginName, $options = [])
 * @method Request getRequest()
 * @method void setActiveNavigationPage($navigationKey, $pageId)
 * @method AuthenticationPlugin auth()
 * @method S3File S3file()
 */
abstract class AbstractActionController extends \Zend\Mvc\Controller\AbstractActionController
{
}
