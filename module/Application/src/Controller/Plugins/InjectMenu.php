<?php
/**
 * @author Oleg Melnic
 */

namespace Application\Controller\Plugins;

use Application\Navigation\PluginManager;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Navigation\Page\Uri;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Class InjectLeftMenu
 * @package FrontEnd\Controller\Plugins
 */
class InjectMenu extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @var Uri
     */
    private $menuContainer;

    /**
     * @var PluginManager
     */
    private $pluginManager;

    /**
     * @param string $pluginName
     * @param array $options
     *
     * @return \Zend\Navigation\AbstractContainer
     */
    public function __invoke($pluginName, $options = [])
    {
        return $this->getMenuContainer()->addPages($this->getPluginManager()->get($pluginName, $options));
    }

    /**
     * @param Uri $container
     */
    public function setMenuContainer(Uri $container)
    {
        $this->menuContainer = $container;
    }

    /**
     * @return Uri
     */
    public function getMenuContainer()
    {
        return $this->menuContainer;
    }

    /**
     * @param \Application\Navigation\PluginManager $pluginManager
     */
    public function setPluginManager(PluginManager $pluginManager)
    {
        $this->pluginManager = $pluginManager;
    }

    /**
     * @return PluginManager
     */
    private function getPluginManager()
    {
        return $this->pluginManager;
    }
}
