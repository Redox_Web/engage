<?php
/**
 * @author Шахновский Александр
 */

namespace Application\Tool\Factory;

interface FactoryInterface
{
    /**
     * @param $name
     * @return boolean
     */
    public function canCreate($name);

    /**
     * @param $data
     * @return object
     */
    public function create($data);
}
