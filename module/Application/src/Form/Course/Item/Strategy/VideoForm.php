<?php
namespace Application\Form\Course\Item\Strategy;

use Application\Form\Course\Item\ItemAbstractForm;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class VideoForm extends ItemAbstractForm implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * SubscriptionPackageForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('video-item');

        //Set POST method for
        $this->setAttribute('method', 'post');
        $this->setUseInputFilterDefaults(false);
        $this->addElements();
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Course\Item\Strategy\Video';
    }
}

