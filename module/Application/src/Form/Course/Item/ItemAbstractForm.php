<?php

namespace Application\Form\Course\Item;

use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
abstract class ItemAbstractForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * SubscriptionPackageForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('article-item');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "name" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'class' => 'validate',
            ],
            'options' => [
                'label' => 'Issue title',
            ]
        ]);

        // Add "issueType" field
        $this->add([
            'type' => 'select',
            'name' => 'type',
            'attributes' => [
                'id' => 'type',
                'class' => 'select-tab',
            ],
            'options' => [
                'label' => 'Issue Type',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "description" field
        $this->add([
            'type' => 'textarea',
            'name' => 'description',
            'attributes' => [
                'id' => 'description',
                'class' => 'materialize-textarea',
            ],
            'options' => [
                'label' => 'Description',
            ]
        ]);

        // Add "content" field
        $this->add([
            'type' => 'textarea',
            'name' => 'content',
            'attributes' => [
                'id' => 'content',
                'class' => 'materialize-textarea',
            ],
            'options' => [
                'label' => 'Content',
            ]
        ]);

        // Add "checkbox" field
        $this->add([
            'type' => 'checkbox',
            'name' => 'isFree',
            'attributes' => [
                'id' => 'isFree',
                'class' => 'filled-in',
            ],
            'options' => [
                'label' => 'Is Free',
            ]
        ]);

        // Add "startDate" field
        $this->add([
            'type' => 'text',
            'name' => 'startDate',
            'attributes' => [
                'id' => 'startDate',
            ],
            'options' => [
                'label' => 'Start Date',
            ]
        ]);

        // Add "state" field
        $this->add([
            'type' => 'select',
            'name' => 'state',
            'attributes' => [
                'id' => 'state',
            ],
            'options' => [
                'label' => 'State',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add "video" field
        $this->add([
            'type'  => 'file',
            'name' => 'video',
            'attributes' => [
                'id' => 'video',
            ],
            'options' => [
                'label' => 'Video',
            ],
        ]);

        // Add "image" field
        $this->add([
            'type'  => 'file',
            'name' => 'image',
            'attributes' => [
                'id' => 'image',
            ],
            'options' => [
                'label' => 'Image',
            ],
        ]);

        // Add "audio" field
        $this->add([
            'type'  => 'file',
            'name' => 'audio',
            'attributes' => [
                'id' => 'audio',
            ],
            'options' => [
                'label' => 'Audio',
            ],
        ]);

        // Add "quiz" field
        $this->add([
            'type' => 'text',
            'name' => 'quiz',
            'attributes' => [
                'class' => 'quizQuestion validate',
            ],
            'options' => [
                'label' => 'Question title/theme',
            ]
        ]);

        // Add "quiz" field
        $this->add([
            'type' => 'text',
            'name' => 'quiz[questions][0][question]',
            'attributes' => [
                'class' => 'quizQuestion validate',
            ],
            'options' => [
                'label' => 'Question title/theme',
            ]
        ]);

        // Add "answers" field
        $this->add([
            'type' => 'text',
            'name' => 'quiz[questions][0][answers][]',
            'attributes' => [
                'class' => 'quizAnswers validate',
            ],
            'options' => [
                'label' => 'Answers',
            ]
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
                'class' => 'button button-green',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Course\Item\ItemAbstract';
    }
}

