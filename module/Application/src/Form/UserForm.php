<?php

namespace Application\Form;

use Application\Entity\User\State;
use Application\Entity\User\Sex;
use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class UserForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    private $yearMax;

    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('user-form');

        //Set POST method for
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->getAgesOptions();
    }

    private function getAgesOptions()
    {
        $curDate = new \DateTime();
        $curDate->modify('-13 years');
        $this->yearMax = $curDate->format('Y');
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "email" field
        $this->add([
            'type' => 'text',
            'name' => 'email',
            'attributes' => [
                'id' => 'email'
            ],
            'options' => [
                'label' => 'Email'
            ]
        ]);

        // Add "fullName" field
        $this->add([
            'type' => 'text',
            'name' => 'fullName',
            'attributes' => [
                'id' => 'fullName'
            ],
            'options' => [
                'label' => 'Fullname'
            ]
        ]);

        // Add "password" field
        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password'
            ],
            'options' => [
                'label' => 'Password'
            ]
        ]);

        // Add "birthday" field
        $this->add([
            'type' => 'Zend\Form\Element\DateTime',
            'name' => 'birthday',
            'attributes' => [
                'id' => 'birthDate',
                'class' => 'required datepicker-date',
                'type' => 'text'
            ],
            'options' => [
                'label' => 'Birth date'
            ]
        ]);

        // Add "address" field
        $this->add([
            'type' => 'text',
            'name' => 'address',
            'attributes' => [
                'id' => 'address'
            ],
            'options' => [
                'label' => 'Address'
            ]
        ]);

        // Add "timezone" field
        $this->add([
            'type' => 'select',
            'name' => 'timezone',
            'attributes' => [
                'id' => 'timezone'
            ],
            'options' => [
                'label' => 'Timezone'
            ]
        ]);

        // Add "sex" field
        $this->add([
            'type'  => 'select',
            'name' => 'sex',
            'attributes' => [
                'id' => 'sex'
            ],
            'options' => [
                'label' => 'Sex',
                'value_options' => [
                    Sex::MALE => 'Male',
                    Sex::FEMALE => 'Female',
                ],
            ],
        ]);

        // Add "description" field
        $this->add([
            'type'  => 'textarea',
            'name' => 'description',
            'attributes' => [
                'id' => 'description'
            ],
            'options' => [
                'label' => 'Description',
            ],
        ]);

        // Add "state" field
        $this->add([
            'type'  => 'select',
            'name' => 'state',
            'attributes' => [
                'id' => 'state'
            ],
            'options' => [
                'label' => 'State',
                'value_options' => [
                    State::ACTIVE => 'Active',
                    State::INACTIVE => 'Inactive',
                ]
            ],
        ]);

        // Add "avatar" field
        $this->add([
            'type' => 'file',
            'name' => 'avatar',
            'attributes' => [
                'id' => 'avatar'
            ],
            'options' => [
                'label' => 'Avatar'
            ]
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\User\Strategy\Student';
    }

    /**
     * @return mixed
     */
    public function getYearMax()
    {
        return $this->yearMax;
    }
}

