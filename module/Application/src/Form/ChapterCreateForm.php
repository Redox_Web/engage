<?php

namespace Application\Form;

use Application\Entity\User\State;
use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class ChapterCreateForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('chapter-create-form');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "name" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name'
            ],
            'options' => [
                'label' => 'Name'
            ]
        ]);

        // Add "alias" field
        $this->add([
            'type' => 'text',
            'name' => 'alias',
            'attributes' => [
                'id' => 'alias',
                'class' => 'validate',
            ],
            'options' => [
                'label' => 'Alias'
            ]
        ]);

        // Add "description" field
        $this->add([
            'type' => 'textarea',
            'name' => 'description',
            'attributes' => [
                'id' => 'description',
                'class'=>'materialize-textarea',
            ],
            'options' => [
                'label' => 'Chapter description',
            ]
        ]);

        // Add "state" field
        $this->add([
            'type' => 'select',
            'name' => 'state',
            'attributes' => [
                'id' => 'state',
            ],
            'options' => [
                'label' => 'State',
                'value_options' => [
                    State::ACTIVE => 'Active',
                    State::INACTIVE => 'Inactive',
                ],
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create chapter',
                'id' => 'submitbutton',
                'class' => 'button button-green',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Course\Chapter';
    }
}

