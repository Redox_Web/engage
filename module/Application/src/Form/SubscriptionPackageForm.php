<?php

namespace Application\Form;

use Application\Entity\ValueObject\Period;
use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class SubscriptionPackageForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * SubscriptionPackageForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('subscription-package-form');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "name" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
            ],
            'options' => [
                'label' => 'Name',
            ]
        ]);

        // Add "price" field
        $this->add([
            'type' => 'number',
            'name' => 'price',
            'attributes' => [
                'id' => 'price',
            ],
            'options' => [
                'label' => 'Price',
            ]
        ]);

        // Add "period" field
        $this->add([
            'type' => 'select',
            'name' => 'period',
            'attributes' => [
                'id' => 'period',
            ],
            'options' => [
                'label' => 'Period',
                'disable_inarray_validator' => true,
            ]
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Subscription\SubscriptionPackage';
    }
}

