<?php

namespace Application\Form;

use Application\Entity\User\State;
use RedoxWeb\WTL\Form\Form;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * This form is used to collect user data.
 */
class CourseCategoryForm extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('course-category-form');

        //Set POST method for
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "email" field
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name'
            ],
            'options' => [
                'label' => 'Name'
            ]
        ]);

        // Add "email" field
        $this->add([
            'type' => 'text',
            'name' => 'alias',
            'attributes' => [
                'id' => 'alias'
            ],
            'options' => [
                'label' => 'Alias'
            ]
        ]);

        // Add "state" field
        $this->add([
            'type'  => 'select',
            'name' => 'state',
            'attributes' => [
                'id' => 'state'
            ],
            'options' => [
                'label' => 'State',
                'value_options' => [
                    State::ACTIVE => 'Active',
                    State::INACTIVE => 'Inactive',
                ]
            ],
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Course\Category';
    }
}

