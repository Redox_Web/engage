<?php
namespace Application\Validator;

use Zend\Validator\AbstractValidator;

class AgeGreaterDiff extends AbstractValidator
{
    const INVALID_AGE = 'invalid_age';

    /**
     * @var int
     */
    protected $minAge = 13;

    protected $messageTemplates = array(
        self::INVALID_AGE => "You must be older then 13 to access",
    );

    /**
     * @return int
     */
    public function getMinAge()
    {
        return $this->minAge;
    }

    /**
     * @param int $minAge
     */
    public function setMinAge(int $minAge)
    {
        $this->minAge = $minAge;
    }

    /**
     * Returns true if and only if $value is valid JSON
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {
        $curdate = new \DateTime();
        $interval = $value->diff($curdate);
        $years = $interval->format('%Y');

        if ($years <= $this->getMinAge()) {
            $this->error(self::INVALID_AGE);
            return false;
        }

        return true;
    }
}