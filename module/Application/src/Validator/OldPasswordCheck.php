<?php
namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use CirclicalUser\Service\AuthenticationService;

class OldPasswordCheck extends AbstractValidator
{
    const INVALID_OLD_PASSWORD = 'invalid_old_password';

    protected $messageTemplates = array(
        self::INVALID_OLD_PASSWORD => "Invalid current password",
    );

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * Returns true if $value is equal with current user password
     *
     * @param  string $value
     * @return bool
     */
    public function isValid($value)
    {
        if (!$this->getAuthenticationService()->verifyPassword($this->getAuthenticationService()->getIdentity(), $value)) {
            $this->error(self::INVALID_OLD_PASSWORD);
            return false;
        }

        return true;
    }
}