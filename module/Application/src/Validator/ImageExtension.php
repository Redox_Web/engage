<?php
/**
 * @author Paladi Vitalie
 */

namespace Application\Validator;

use Zend\Validator\File\Extension;

class ImageExtension extends Extension
{
    public function isValid($value)
    {
       return parent::isValid($value);
    }
}
