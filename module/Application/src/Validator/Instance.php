<?php
/**
 * @author Melnic Oleg
 */

namespace Application\Validator;

use Zend\Validator\IsInstanceOf;

class Instance extends IsInstanceOf
{

    /**
     * @var bool
     */
    protected $allowNull = false;

    /**
     * @return bool
     */
    public function getAllowNull()
    {
        return $this->allowNull;
    }

    /**
     * @param bool $allowNull
     */
    public function setAllowNull($allowNull)
    {
        $this->allowNull = $allowNull;
    }

    /**
     * Returns true if $value is instance of $this->className or is Null
     *
     * @param  mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        if (is_null($value) && $this->getAllowNull()) {
            return true;
        }
        return parent::isValid($value);
    }
}
