<?php
namespace Application\Listener\User;

use Application\Entity\User\UserAbstract;
use CirclicalUser\Mapper\RoleMapper;
use CirclicalUser\Provider\RoleProviderInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;

/**
 * Class User
 *
 * @author Shahnovsky Alex
 */
class User implements EventSubscriber, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @var RoleProviderInterface
     */
    private $roleProvider;

    /**
     * @param LifecycleEventArgs $args
     *
     * @return void
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $object = $args->getObject();
        if ($object instanceof UserAbstract) {
            $object->setRoleProvider($this->getRoleProvider());
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postLoad,
        ];
    }

    /**
     * @return RoleProviderInterface
     */
    public function getRoleProvider()
    {
        if (is_null($this->roleProvider)) {
            $this->roleProvider = $this->getServiceLocator()->get(RoleMapper::class);
        }

        return $this->roleProvider;
    }
}
