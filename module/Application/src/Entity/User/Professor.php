<?php

namespace Application\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Professors
 *
 * @author Paladi Vitalie <vitaliip@redox.ca>
 * @version 1.0
 * @ORM\Entity
 */
class Professor extends UserAbstract
{
    /**
     * Get a list of role names
     *
     * @return array
     */
    protected function getRoleNames()
    {
        return ['professor'];
    }
}
