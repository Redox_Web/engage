<?php

namespace Application\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admins
 *
 * @author Paladi Vitalie <vitaliip@redox.ca>
 * @version 1.0
 * @ORM\Entity
 */
class Admin extends UserAbstract
{
    /**
     * Get a list of role names
     *
     * @return array
     */
    protected function getRoleNames()
    {
        return ['admin'];
    }
}
