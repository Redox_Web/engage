<?php

namespace Application\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * Students
 *
 * @author Paladi Vitalie <vitaliip@redox.ca>
 * @version 1.0
 * @ORM\Entity
 */
class Student extends UserAbstract
{
    /**
     * Get a list of role names
     *
     * @return array
     */
    protected function getRoleNames()
    {
        return ['student'];
    }
}
