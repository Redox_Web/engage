<?php
namespace Application\Entity\Course;

use \RedoxWeb\WTL\Filter\From\FromScalar\FactoryInterface;
use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Exception\InvalidArgument;

/**
 * Difficulty of course
 *
 * @author Paladi Vitalie <vitaliip@redox.ca>
 * @version 1.0
 *
 * @ORM\Embeddable
 */
class Difficulty implements FactoryInterface
{
    const BEGINNER = 'beginner';
    const INTERMEDIATE = 'intermediate';
    const ADVANCED = 'advanced';

    /**
     * @var integer
     *
     * @ORM\Column(name="difficulty", type="string", nullable=true)
     */
    private $difficulty;

    public function __construct($difficulty)
    {
        if (!in_array($difficulty, self::getConstValues())) {
            throw new InvalidArgument("Incorrect difficulty for course: $difficulty");
        }
        $this->difficulty = $difficulty;
    }

    /**
    * @return integer
    */
    public function getValue()
    {
       return $this->difficulty;
    }

    /**
    * @return array
    */
    public static function getConstValues()
    {
        return [self::BEGINNER, self::INTERMEDIATE, self::ADVANCED];
    }

    /**
     * Создаем объект из скалярного значения
     * @param $data
     * @return Difficulty
     */
    public static function createFromScalar($data)
    {
        return new self($data);
    }
}
