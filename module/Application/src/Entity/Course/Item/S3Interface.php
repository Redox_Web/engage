<?php

namespace Application\Entity\Course\Item;

interface S3Interface
{
    /**
     * @return string
     */
    public function getFileName();
}
