<?php
namespace Application\Entity\Course\Item;

use Application\Entity\Course\Item\Quiz\QuizVO;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Quiz
 *
 * @author Shahnovsky Alex
 *
 * @ORM\Entity
 */
class Quiz extends ItemAbstract
{
    /**
     * @var QuizVO
     *
     * @ORM\Embedded(class="\Application\Entity\Course\Item\Quiz\QuizVO", columnPrefix = false)
     */
    protected $quiz;

    /**
     * @return QuizVO
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * @param QuizVO $quiz
     */
    public function setQuiz(QuizVO $quiz)
    {
        $this->quiz = $quiz;
    }
}
