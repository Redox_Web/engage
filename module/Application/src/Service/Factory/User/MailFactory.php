<?php

namespace Application\Service\Factory\User;

use Application\Service\Mail\User\MailService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

/**
 * Class MailFactory
 *
 * @author Oleg Melnic
 */
class MailFactory implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var MailService $mailService */
        $mailService = $callback();
        $mailService->setMailService($container->get('mail.service'));
        $mailService->setConfig($container->get('Config'));

        return $mailService;
    }
}
