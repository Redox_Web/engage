<?php
namespace Application\Service\Event;

use Application\Entity\Event\Offline as OfflineEventEntity;
use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Service for offline event
 */
class Offline implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @param array $data
     *
     * @return OfflineEventEntity
     */
    public function createEmptyEntity(array $data)
    {
        return new OfflineEventEntity();
    }

    /**
     * Получить имя сущности
     *
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Event\Offline::class;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getRepository()
    {
        return $this->getInheritanceResolver()->getRepository();
    }

    /**
     * @param $alias
     * @return OfflineEventEntity|null
     *
     */
    public function findByAlias($alias)
    {
        return $this->getRepository()->findOneByAlias($alias);
    }

    /**
     * Get list of upcoming offline events for current user
     *
     * @param integer $userId
     * @return \Application\Entity\Event\Offline[]
     */
    public function getUpcomingOfflineEvents($userId)
    {
        return $this->getRepository()->getUpcomingOfflineEvents($userId);
    }

    /**
     * Get list of past offline events for current user
     *
     * @param integer $userId
     * @return \Application\Entity\Event\Offline[]
     */
    public function getPastOfflineEvents($userId)
    {
        return $this->getRepository()->getPastOfflineEvents($userId);
    }
}
