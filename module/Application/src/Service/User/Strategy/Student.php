<?php
/**
 * @author Олег Мельник
 */

namespace Application\Service\User\Strategy;

use Application\Service\User\User;

/**
 * Class Student
 *
 * @package Application\Service\User
 *
 * @method \Application\Repository\User\Student getRepository()
 */
class Student extends UserAbstract
{
    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\User\Student::class;
    }

    /**
     * @param array $data
     *
     * @return \Application\Entity\User\Student
     */
    public function createEmptyEntity(array $data)
    {
        $entity = new \Application\Entity\User\Student();
        $entity->setRoleProvider($this->getRoleProvider());

        return $entity;
    }

    /**
     * @return \Application\Entity\User\Student[]|null
     *
     */
    public function getAll()
    {
        return $this->getRepository()->findAll(
            [
                'type' => User::TYPE_STUDENT,
            ]
        );
    }
}
