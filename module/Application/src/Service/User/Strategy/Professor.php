<?php
/**
 * @author Олег Мельник
 */

namespace Application\Service\User\Strategy;

use Application\Service\User\User;

/**
 * Class Professor
 *
 * @package Application\Service\User
 *
 * @method \Application\Repository\User\Professor getRepository()
 */
class Professor extends UserAbstract
{
    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\User\Professor::class;
    }

    /**
     * @param array $data
     *
     * @return \Application\Entity\User\Professor
     */
    public function createEmptyEntity(array $data)
    {
        $entity = new \Application\Entity\User\Professor();
        $entity->setRoleProvider($this->getRoleProvider());

        return $entity;
    }

    /**
     * @return \Application\Entity\User\Profressor[]|null
     *
     */
    public function getAll()
    {
        return $this->getRepository()->findAll(
            [
                'type' => User::TYPE_PROFESSOR,
            ]
        );
    }
}
