<?php
namespace Application\Service\Course;

use Application\Entity\Course\Category as CourseCategory;
use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;

/**
 * Service for course category
 */
class Category implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * Получить имя сущности
     *
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Course\Category::class;
    }

    /**
     * @param array $data
     *
     * @return CourseCategory
     */
    public function createEmptyEntity(array $data)
    {
        return new CourseCategory();
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getRepository()
    {
        return $this->getInheritanceResolver()->getRepository();
    }

    /**
     * @param array $params
     * @return CourseCategory|null
     */
    public function getOne(array $params)
    {
        return $this->getRepository()->findOneBy($params);
    }

    /**
     * @return CourseCategory[]|null
     *
     */
    public function getAllCourseCategories()
    {
        return $this->getRepository()->findAll();
    }
}
