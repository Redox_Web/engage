<?php
namespace Application\Service\Course;

use Application\Entity\Course\Course as CourseEntity;
use Application\Entity\Course\Chapter as ChapterEntity;
use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;
use RedoxWeb\WTL\StdLib\EntityManagerAwareInterface;
use RedoxWeb\WTL\StdLib\EntityManagerAwareTrait;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\InputFilter\InputFilterInterface;

/**
 * Service for course
 */
class Course implements
    CrudInterface,
    NoInheritanceAwareInterface,
    ServiceLocatorAwareInterface,
    EntityManagerAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;
    use ServiceLocatorAwareTrait;
    use EntityManagerAwareTrait;

    /**
     * @var \Application\Service\Course\Category $categoryService
     */
    protected $categoryService;

    /**
     * @param array $data
     *
     * @return CourseEntity
     */
    public function createEmptyEntity(array $data)
    {
        return new CourseEntity();
    }

    /**
     * Получить имя сущности
     *
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Course\Course::class;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getRepository()
    {
        return $this->getInheritanceResolver()->getRepository();
    }

    /**
     * @param array $params
     * @return CourseEntity[]|null
     *
     */
    public function getAllCourses()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param array $params
     * @return CourseEntity[]|null
     *
     */
    public function getAllCoursesByParam($params)
    {
        if (!is_array($params) || !$params) {
            throw new \Application\Service\Exception\InvalidArgument("Invalid argument params, it must be array and must contain at least one element with condition.");
        }

        return $this->getRepository()->findAll([
            array($params)
        ]);
    }

    /**
     * @param array $params
     * @return CourseEntity|null
     *
     */
    public function getOne($params)
    {
        if (!is_array($params) || !$params) {
            throw new \Application\Service\Exception\InvalidArgument("Invalid argument params, it must be array and must contain at least one element with condition.");
        }
        return $this->getRepository()->findOneBy($params);
    }

    /**
     * @param $courseId
     * @return ChapterEntity|null
     */
    public function getAllChapters($courseId)
    {
        $this->getRepository()->getAllChapters($courseId);
    }

    /**
     * @param array $data
     * @return \Application\Entity\Course\Course
     */
    public function firstSave(array $data)
    {
        $entity = $this->createEmptyEntity($data);
        $entity->setName($data['name']);
        $entity->setTeacher($data['teacher']);
        $entity->setAuthor($data['author']);
        $this->getServiceLocator()->get(\Doctrine\ORM\EntityManager::class)->persist($entity);
        $this->getServiceLocator()->get(\Doctrine\ORM\EntityManager::class)->flush();

        return $entity;
    }

    /**
     * @param array  $data
     * @param bool   $flush
     * @param array  $context
     * @param string $permission
     *
     * @return \Application\Entity\User\UserAbstract
     */
    public function create(array $data, $flush = true, array $context = [], $permission = __FUNCTION__)
    {
        $entity = $this->getInheritanceResolver()->create($data, $flush, $context, $permission);
        $this->updateCategories($entity, $data);
        return $entity;
    }

    /**
     * @param array|int $identity
     * @param array     $data
     * @param array     $context
     * @param string    $permission
     *
     * @return object
     */
    public function update($identity, array $data, array $context = [], $permission = __FUNCTION__)
    {
        $entity = $this->getInheritanceResolver()->update($identity, $data, $context, $permission);
        $this->updateCategories($entity, $data);
        return $entity;
    }

    /**
     * Метод, который обновляет связи User с Categories
     * Он отвечает за то, чтобы удалить лишние и создать новые связи
     * @param \Application\Entity\Course\Course $entity
     * @param $updateData
     */
    protected function updateCategories($entity, array $updateData)
    {
        if (isset($updateData['categories'])) {
            $categoryIds = $updateData['categories'];
        } else {
            return;
        }

        $oldCategoryArray = [];
        foreach ($entity->getCategories() as $category) {
            $oldCategoryArray[$category->getIdentity()] = $category;
        }
        $removeIds = array_diff(array_keys($oldCategoryArray), $categoryIds);
        $addIds = array_diff($categoryIds, array_keys($oldCategoryArray));
        foreach ($removeIds as $removeId) {
            $entity->removeCategory($oldCategoryArray[$removeId]);
        }
        foreach ($addIds as $addId) {
            $category = $this->getCategoryService()->find($addId);
            $entity->addCategory($category);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param \Application\Entity\Course\Course $course
     * @return array
     */
    public function convertCategoriesToArray($course)
    {
        $returnArray = [];
        $categories = $course->getCategories();

        if ($categories) {
            foreach ($categories as $category) {
                /** @var \Application\Entity\Course\Category $category */
                $returnArray[] = $category->getIdentity();
            }
        }

        return $returnArray;
    }

    /**
     * @return Category
     */
    public function getCategoryService()
    {
        return $this->categoryService;
    }

    /**
     * @param Category $categoryService
     */
    public function setCategoryService(Category $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Create new course
     *
     * @param array $data
     * @return CourseEntity
     */
    public function createNew(array $data)
    {
        $this->getInheritanceResolver()->getFilter()->setValidationGroup(
            ['name', 'description', 'execution', 'howPass', 'difficulty', 'author', 'teacher', 'institution']
        );

        try {
            $result = $this->create($data, true, [], __FUNCTION__);
        } catch (\RedoxWeb\WTL\Crud\Exception\ValidationException $e) {
            return ['errors' => $e->getValidationMessages()];
        }

        $this->getInheritanceResolver()->getFilter()->setValidationGroup(InputFilterInterface::VALIDATE_ALL);

        return $result;
    }

    /**
     * Change course data
     *
     * @param int   $identity
     * @param array $data
     *
     * @return CourseEntity
     */
    public function changeData($identity, array $data)
    {
        $this->getInheritanceResolver()->getFilter()->setValidationGroup(
            ['name', 'description', 'execution', 'howPass', 'difficulty', 'author', 'teacher', 'institution']
        );

        try {
            $result = $this->update($identity, $data);
        } catch (\RedoxWeb\WTL\Crud\Exception\ValidationException $e) {
            return ['errors' => $e->getValidationMessages()];
        }

        $this->getInheritanceResolver()->getFilter()->setValidationGroup(InputFilterInterface::VALIDATE_ALL);

        return $result;
    }
}
