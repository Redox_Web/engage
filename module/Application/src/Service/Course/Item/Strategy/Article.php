<?php
namespace Application\Service\Course\Item\Strategy;

use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;

/**
 * Class Article
 *
 * @author Shahnovsky Alex
 */
class Article implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @param array $data
     *
     * @return \Application\Entity\Course\Item\Article
     */
    public function createEmptyEntity(array $data)
    {
        return new \Application\Entity\Course\Item\Article();
    }

    /**
     * Получить имя сущности
     *
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Course\Item\Article::class;
    }
}
