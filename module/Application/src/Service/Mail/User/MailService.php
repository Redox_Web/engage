<?php
namespace Application\Service\Mail\User;

use Application\ServiceMail\Send;
use Zend\View\Model\ViewModel;

/**
 * service that send email to users
 */
class MailService
{
    /**
     * @var Send
     */
    protected $mailService;

    protected $config;

    /**
     * @param array $data
     */
    public function resetPasswordEmail($data)
    {
        $httpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
        $passwordResetUrl = 'http://' . $httpHost . '/set-password?token=' . $data['token'];

        try {
            /** @var $viewModel ViewModel */
            $viewModel = new ViewModel(['data' => $data, 'requestUrl' => $passwordResetUrl]);
            $viewModel->setTemplate('frontend/mail/reset_pwd');
            /** @var Send $mailService */
            $mailService = $this->getMailService();
            $config = $this->getConfig();

            $mailService->sendEmail(
                'Password Reset on Engagement Portal.',
                $viewModel,
                $data['email'],
                $config['mailchimp']['settings']['reply_to'],
                $config['mailchimp']['settings']['from_name']
            );
        } catch (\Exception $exception) {
            vred($exception->getMessage());
        }
    }

    /**
     * @param array $data
     */
    public function signUpEmail($data)
    {
        try {
            /** @var $viewModel ViewModel */
            $viewModel = new ViewModel(['data' => $data]);
            $viewModel->setTemplate('frontend/mail/signup');
            /** @var Send $mailService */
            $mailService = $this->getMailService();
            $config = $this->getConfig();

            $mailService->sendEmail(
                'Registration on Engagement Portal.',
                $viewModel,
                $data['email'],
                $config['mailchimp']['settings']['reply_to'],
                $config['mailchimp']['settings']['from_name']
            );
        } catch (\Exception $exception) {
            vred($exception->getMessage());
        }
    }

    /**
     * @return Send
     */
    public function getMailService()
    {
        return $this->mailService;
    }

    /**
     * @param Send $mailService
     */
    public function setMailService(Send $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }
}
