<?php
namespace Application\Service\Mail\User\Social;

use Application\ServiceMail\Send;
use Zend\View\Model\ViewModel;

/**
 * service that send email to users
 */
class MailService
{
    /**
     * @var Send
     */
    protected $mailService;

    protected $config;

    /**
     * @param array $data
     */
    public function registrationEmail($data)
    {
        try {
            /** @var $viewModel ViewModel */
            $viewModel = new ViewModel(['data' => $data]);
            $viewModel->setTemplate('frontend/mail/social');
            /** @var Send $mailService */
            $mailService = $this->getMailService();
            $config = $this->getConfig();

            $mailService->sendEmail(
                'Registration on Engagement Portal through social network.',
                $viewModel,
                $data['email'],
                $config['mailchimp']['settings']['reply_to'],
                $config['mailchimp']['settings']['from_name']
            );
        } catch (\Exception $exception) {
            vred($exception->getMessage());
        }
    }

    /**
     * @return Send
     */
    public function getMailService()
    {
        return $this->mailService;
    }

    /**
     * @param Send $mailService
     */
    public function setMailService(Send $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }
}
