<?php
namespace Application\Service\Mail\Feedback;

use Application\ServiceMail\Send;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;

/**
 * Class MailService
 * service that send email to users
 *
 * @copyright (c) 2017, Redox
 */
class MailService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @param array $data
     */
    public function sendFeedback($data)
    {
        try {
            $config = $this->getServiceLocator()->get('Config');
            /** @var $viewModel ViewModel */
            $viewModel = new ViewModel(['data' => $data]);
            $viewModel->setTemplate('frontend/mail/feedback');
            /** @var Send $mailService */
            $mailService = $this->getServiceLocator()->get('mail.service');
            $mailService->sendEmail(
                'Feedback from '.$data['name'].' ('.$data['email'].')',
                $viewModel,
                $config['redox']['feedbackEmail'],
                $data['email'],
                'Admin Engage Portal'
            );
        } catch (\Exception $exception) {
        }
    }
}
