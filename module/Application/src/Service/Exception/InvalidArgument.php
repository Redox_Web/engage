<?php

namespace Application\Service\Exception;

class InvalidArgument extends \InvalidArgumentException implements ExceptionInterface
{
}
