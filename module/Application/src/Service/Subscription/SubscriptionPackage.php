<?php

namespace Application\Service\Subscription;

use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;

/**
 * Class Payment
 */
class SubscriptionPackage implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Subscription\SubscriptionPackage::class;
    }

    /**
     * @param array $data
     * @return object
     */
    public function createEmptyEntity(array $data)
    {
        return new \Application\Entity\Subscription\SubscriptionPackage();
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->getInheritanceResolver()->getRepository();
    }

    public function getAllPackages()
    {
        return $this->getRepository()->findAll();
    }
}
