<?php

namespace Application\Service\Subscription;

use RedoxWeb\WTL\Crud\CrudInterface;
use RedoxWeb\WTL\Crud\CrudTrait;
use RedoxWeb\WTL\Crud\NoInheritanceAwareInterface;
use RedoxWeb\WTL\Crud\NoInheritanceAwareTrait;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class Payment
 */
class ActiveSubscriptionPackage implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Subscription\ActiveSubscriptionPackage::class;
    }

    /**
     * @param array $data
     * @return object
     */
    public function createEmptyEntity(array $data)
    {
        return new \Application\Entity\Subscription\ActiveSubscriptionPackage();
    }
}
