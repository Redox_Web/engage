<?php
namespace Application\File\NameGenerator;

use RedoxWeb\WTL\File\NameGenerator\GeneratorInterface;

/**
 * Class S3
 *
 * @author Shahnovsky Alex
 */
class S3 implements GeneratorInterface
{
    /**
     * @var array
     */
    private $directories;

    /**
     * @param $type
     * @return \Zend\Validator\File\MimeType
     */
    public function getMimeTypeValidator($type)
    {
        return new \Zend\Validator\File\MimeType($type);
    }

    /**
     * @return array
     */
    public function getDirectories(): array
    {
        return $this->directories;
    }

    /**
     * @param array $directories
     */
    public function setDirectories(array $directories)
    {
        $this->directories = $directories;
    }

    /**
     * @param string $url
     * @param string $content
     *
     * @return string $filename
     */
    public function generateFileName($url, $content)
    {
        $temp = tmpfile();
        fwrite($temp, $content);
        fseek($temp, 0);
        $fileName = stream_get_meta_data($temp)['uri'];

        $directory = 'docs/';
        foreach ($this->getDirectories() as $type => $dir) {
            if ($this->getMimeTypeValidator([$type])->isValid($fileName)) {
                $directory = $dir;
            }
        }
        fclose($temp);

        return $directory . md5($url);
    }
}
