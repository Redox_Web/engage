<?php
namespace Application\File\Saver;

use RedoxWeb\WTL\File\Saver\SaverInterface;

/**
 * Class S3
 *
 * @author Shahnovsky Alex
 */
class S3 implements SaverInterface
{
    /**
     * @var \EngageApi\Service\AWS\S3\S3
     */
    private $s3Service;

    /**
     * @return \EngageApi\Service\AWS\S3\S3
     */
    public function getS3Service(): \EngageApi\Service\AWS\S3\S3
    {
        return $this->s3Service;
    }

    /**
     * @param \EngageApi\Service\AWS\S3\S3 $s3Service
     */
    public function setS3Service(\EngageApi\Service\AWS\S3\S3 $s3Service)
    {
        $this->s3Service = $s3Service;
    }

    /**
     * @param string $filename
     * @param string $content
     *
     * @return string $path
     */
    public function saveToFile($filename, $content)
    {
        $this->getS3Service()->saveContent($filename, $content);

        return $filename;
    }
}
