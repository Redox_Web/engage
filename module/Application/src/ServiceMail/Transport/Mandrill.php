<?php

namespace Application\ServiceMail\Transport;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\TransportInterface;

/**
 * Class Mandrill
 *
 * @copyright (c) 2017, Redox
 */
class Mandrill extends Smtp implements TransportInterface
{
    /** @var  \Mandrill */
    protected $serviceMandrill;

    public function send(Message $message)
    {
        $fromEmail = $message->getSender() ?
            $message->getSender()->getEmail() :
            $message->getFrom()->current()->getEmail();
        $fromName = $message->getSender() ?
            $message->getSender()->getName() :
            $message->getFrom()->current()->getName();

        $messageForSend = array(
            'html' => $message->getBody(),
            'text' => $message->getBodyText(),
            'subject' => $message->getSubject(),
            'from_email' => $fromEmail,
            'from_name' => $fromName,
            'to' => array(
                array(
                    'email' => $message->getTo()->current()->getEmail(),
                    'name' => $message->getTo()->current()->getName(),
                    'type' => 'to'
                )
            ),
            'headers' => array('Reply-To' => $fromEmail)
        );

        try {
            $this->getServiceMandrill()->messages->send($messageForSend);
        } catch (\Exception $e) {
            //vred($e->getMessage());
        }
    }

    /**
     * @return \Mandrill
     */
    public function getServiceMandrill(): \Mandrill
    {
        return $this->serviceMandrill;
    }

    /**
     * @param \Mandrill $serviceMandrill
     */
    public function setServiceMandrill(\Mandrill $serviceMandrill)
    {
        $this->serviceMandrill = $serviceMandrill;
    }
}
