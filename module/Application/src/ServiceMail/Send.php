<?php
/**
 * Сервис для отправки емаила
 * @author Oleg Melnic
 */

namespace Application\ServiceMail;

use Application\ServiceMail\Header\Subject;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mail\Message;
use Zend\Mail\Transport\TransportInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class Send implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    /**
     * @var TransportInterface
     */
    protected $transport = null;


    /**
     * @param           $subject
     * @param ViewModel $viewModel
     * @param           $receiver
     * @param           $senderEmail
     * @param null      $senderName
     * @param array     $attachments
     * @return array|bool
     * @throws \Exception
     */
    public function sendEmail(
        $subject,
        ViewModel $viewModel,
        $receiver,
        $senderEmail,
        $senderName = null,
        array $attachments = []
    ) {
        /** @var \Zend\View\Renderer\PhpRenderer $renderer */
        $renderer = $this->getServiceLocator()->get(PhpRenderer::class);
        $renderedMail = $renderer->render($viewModel);
        $mimeMessage = new MimeMessage();

        $html = new Part($renderedMail);
        $html->type = \Zend\Mime\Mime::TYPE_HTML;
        $html->charset = 'UTF-8';
        $mimeMessage->addPart($html);

        if (count($attachments)) {
            /** @var \Zend\Mime\Part $attachment */
            foreach ($attachments as $attachment) {
                $mimeMessage->addPart($attachment);
            }
        }

        $receivers = explode(',', $receiver);
        foreach ($receivers as $key => $receiver) {
            $receivers[$key] = trim($receiver);
        }

        $mail = new Message();
        $mail->setEncoding('utf-8');
        $subjectHeader = new Subject($subject);
        $subjectHeader->setSubject($subject);
        $mail->getHeaders()->addHeader($subjectHeader);
        $mail->setBody($mimeMessage);
        $mail->addTo($receivers);
        $mail->setFrom($senderEmail, $senderName);

        try {
            $this->getTransport()->send($mail);
            return true;
        } catch (\Zend\Mail\Exception\RuntimeException $ex) {
            $ex->getMessage();
            $response = array('error' => 1, 'msg' => $ex->getMessage());
            return $response;
        }
    }

    /**
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return null|TransportInterface
     */
    public function getTransport()
    {
        return $this->transport;
    }
}
