<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Controller\Plugins\S3File;
use Application\Controller\S3\GetFileController;
use Application\Service\Course\Course;
use Application\View\Helper\SelectOptions;
use Application\Factory\Router\Http\StaticPages;
use Frontend\Form\Feedback\Feedback;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'get-file' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/getFile/:fileName/',
                    'defaults' => [
                        'controller' => GetFileController::class,
                        'action' => 'getFile',
                    ],
                ],
            ],
        ],
    ],
    'doctrine' => [
        'driver' => [
            'common_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__.'/../src/Entity',]
            ],
            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'common_driver',
                ]
            ],
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    \Gedmo\Tree\TreeListener::class,
                    \Gedmo\Sluggable\SluggableListener::class,
                    \Gedmo\Sortable\SortableListener::class,
                    \Gedmo\Timestampable\TimestampableListener::class,
                    \Application\Listener\User\User::class,
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\UserController::class => InvokableFactory::class,
        ],
        'abstract_factories' => [
            \Application\Factory\Controller\AbstractFactory::class,
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            S3File::class => \Application\Factory\Controller\Plugin\S3File::class,
        ],
        'aliases' => [
            'S3file' => S3File::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'application/index/index' => __DIR__.'/../view/application/index/index.phtml',
            'error/404' => __DIR__.'/../view/error/404.phtml',
            'error/index' => __DIR__.'/../view/error/index.phtml',
            'main_menu' => __DIR__.'/../view/application/menu/main_menu.phtml',
            'top_menu' => __DIR__.'/../view/application/menu/top_menu.phtml',
        ],
        'template_path_stack' => [
            __DIR__.'/../view',
        ],
    ],
    'view_helpers' => [
        'invokables' => [],
        'factories' => [
            'aepNavigation' => \Application\Factory\View\Helper\AepNavigation::class,
            'isAllowed' => \Application\Factory\View\Helper\IsAllowed::class,
            'authIdentity' => \Application\Factory\View\Helper\Authentication::class,
            'namedDateFormat' => \Application\Factory\View\Helper\NamedDateFormat::class,
            'offlineEvent' => \Application\Factory\View\Helper\OfflineEvent::class,
            'course' => \Application\Factory\View\Helper\Course::class,
            'chapter' => \Application\Factory\View\Helper\Chapter::class,
            'item' => \Application\Factory\View\Helper\Item::class,
            SelectOptions::class => \Application\Factory\View\Helper\SelectOptions::class,
            'social' => \Application\Factory\View\Helper\Social::class,
            's3File' => \Application\Factory\View\Helper\S3File::class,
            'image' => \Application\Factory\View\Helper\Image::class,
            'imageThumb' => \Application\Factory\View\Helper\Image\ImageThumb::class,
        ],
        'aliases' => [
            'selectOptionsHelper' => SelectOptions::class,
        ],
        'delegators' => [
            'Navigation' => \Application\Factory\View\Helper\Navigation::class,
        ],
    ],
    'validators' => [
        'invokables' => [
            'Instance' => \Application\Validator\Instance::class,
            'AgeGreaterDiff' => \Application\Validator\AgeGreaterDiff::class,
            'UniqueObject' => \DoctrineModule\Validator\UniqueObject::class,
            'IsArray' => \Application\Validator\IsArray::class,
            'ImageExtension' => \Application\Validator\ImageExtension::class,
            'ImageFileSize' => \Application\Validator\ImageFileSize::class,
            'IsJson' => \Application\Validator\IsJson::class,
        ],
        'factories' => [
            'OldPasswordCheck' => \Application\Factory\Validator\OldPasswordCheckFactory::class,
        ],
    ],
    'service_manager' => [
        'initializers' => [],
        'abstract_factories' => [
            \Zend\Navigation\Service\NavigationAbstractServiceFactory::class,
            \Application\Factory\Navigation\Partial::class,
        ],
        'aliases' => [
            'captcha.adapter' => \Zend\Captcha\Dumb::class,
        ],
        'factories' => [
            \Application\Listener\User\User::class => InvokableFactory::class,
            \Application\Service\User\Mapper\UserMapper::class =>
                \Application\Service\Factory\User\UserMapperFactory::class,
            \Application\Service\Mail\User\MailService::class => InvokableFactory::class,
            \Application\Service\Mail\User\Social\MailService::class => InvokableFactory::class,
            \Application\Service\Mail\Feedback\MailService::class => InvokableFactory::class,
            'mail.service' => \Application\Factory\ServiceMail\Send::class,
            'mail.transport' => \Application\Factory\ServiceMail\SmtpTransport::class,
            'injectedNavigationPluginManager' =>
                \Application\Factory\Navigation\PluginManagerFactory::class,
            'courseTopMenu' => \PersonalOffice\Factory\Navigation\CourseTopMenu::class,
            'courseLeftMenu' => \PersonalOffice\Factory\Navigation\CourseLeftMenu::class,
            's3NameGenerator' => \Application\Factory\File\NameGenerator\S3::class,
            's3Saver' => \Application\Factory\File\Saver\S3::class,
            's3Downloader' => \Application\Factory\File\Downloader::class,
            \Zend\Captcha\Dumb::class => \Application\Factory\Captcha\Adapter\Dumb::class,
            \Zend\Captcha\ReCaptcha::class => \Application\Factory\Captcha\Adapter\ReCaptcha::class,
        ],
        'delegators' => [
            \Application\Service\User\User::class => [
                \Application\Factory\AuthenticationService::class,
                \Application\Service\Factory\User\UserFactory::class,
            ],
            \Application\Service\User\Strategy\Admin::class => [
                \Application\Factory\RoleProvider::class,
                \Application\Factory\AuthenticationService::class,
            ],
            \Application\Service\User\Strategy\Student::class => [
                \Application\Factory\RoleProvider::class,
                \Application\Factory\AuthenticationService::class,
            ],
            \Application\Service\User\Strategy\Professor::class => [
                \Application\Factory\RoleProvider::class,
                \Application\Factory\AuthenticationService::class,
            ],
            \Application\Service\User\Social\User::class => [
                \Application\Service\Factory\User\Social\UserFactory::class,
            ],
            Course::class => [
                \Application\Factory\Course\Course::class
            ],
            \Application\Service\Mail\User\MailService::class => [
                \Application\Service\Factory\User\MailFactory::class,
            ],
            \Application\Service\Mail\User\Social\MailService::class => [
                \Application\Service\Factory\User\Social\MailFactory::class,
            ],
            Feedback::class => [
                \Frontend\Factory\Form\Feedback\CaptchaDelegator::class
            ],
        ],
        'invokables' => [
            \Application\Factory\Social\UserCustomWrapperFactory::class =>
                \Application\Factory\Social\UserCustomWrapperFactory::class,
            \Application\Navigation\ContainerProvider::class => \Application\Navigation\ContainerProvider::class,
        ],
        'shared' => [],
    ],
    'route_manager' => [
        'factories' => [
            'staticPages' => StaticPages::class,
        ],
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'Application' => [
                    "controllers" => [
                        \Application\Controller\IndexController::class => [
                            'default' => [], // anyone can access
                        ],
                        \Application\Controller\UserController::class => [
                            'default' => [], // anyone can access
                        ],
                        \DoctrineORMModule\Yuml\YumlController::class => [
                            'default' => [], // anyone can access
                        ],
                        GetFileController::class => [
                            'default' => [], // anyone can access
                        ],
                    ],
                ],
            ],
        ],
    ],
    'redox_injected_navigation' => [
        'factories' => [
            'courseTopMenu' => \PersonalOffice\Factory\Navigation\CourseTopMenu::class,
            'professorTopMenu' => \PersonalOffice\Factory\Navigation\ProfessorTopMenu::class,
            'professorLeftMenu' => \PersonalOffice\Factory\Navigation\ProfessorLeftMenu::class,
            'courseLeftMenu' => \PersonalOffice\Factory\Navigation\CourseLeftMenu::class,
        ],
        'shared' => [
        ],
    ],
];
