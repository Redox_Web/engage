<?php
/**
 * @author Шахновский Александр
 */
namespace ApplicationTest\Filter\From\FromScalar;

use \RedoxWeb\WTL\Filter\From\FromScalar\FilterCreate;
use Zend\InputFilter\InputFilter;

class FilterCreateTest extends \PHPUnit\Framework\TestCase
{
    protected function createFilter()
    {
        return new FilterCreate('ApplicationTest\Filter\Mock\ValueObject');
    }

    public function testFilterCreateObject()
    {
        $data = 'value';
        $filter = $this->createFilter();
        /** @var \ApplicationTest\Filter\Mock\ValueObject $valueObject */
        $valueObject = $filter->filter($data);
        $this->assertInstanceOf('ApplicationTest\Filter\Mock\ValueObject', $valueObject);
        $this->assertSame('value', $valueObject->getData());
    }

    public function testFilterReturnInitialValue()
    {
        $data = array('data' => 'null');
        $filter = $this->createFilter();
        $filteredData = $filter->filter($data);
        $this->assertSame($data, $filteredData);
    }
    public function testFilterNullValue()
    {
        $data = null;
        $filter = $this->createFilter();
        $filteredData = $filter->filter($data);
        $this->assertSame($data, $filteredData);
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testSetWrongObjectName()
    {
        new FilterCreate('ApplicationTest\Filter\Mock\SimpleObject');
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testFilterNullOption()
    {
        new FilterCreate(null);
    }

    public function testInputFilterOption()
    {
        $filter = $this->createFilter();
        $inputFilter = new InputFilter();
        $filter->setInputFilter($inputFilter);
        $this->assertSame($inputFilter, $filter->getInputFilter());
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testSetInvalidInputFilter()
    {
        $filter = $this->createFilter();
        $filter->setInputFilter('a');
    }

    public function dataNotUseInputFilterBefore()
    {
        return [
            [
                'value' => '1',
                'expected' => '1'
            ],
            [
                'value' => 'a',
                'expected' => 'a'
            ],
        ];
    }

    /**
     * @dataProvider dataNotUseInputFilterBefore
     * @param $value
     * @param $expected
     */
    public function testNotUseInputFilterBefore($value, $expected)
    {
        $filter = $this->createFilter();

        /** @var \ApplicationTest\Filter\Mock\ValueObject $result */
        $result = $filter->filter($value);
        $this->assertInstanceOf('ApplicationTest\Filter\Mock\ValueObject', $result);
        $this->assertSame($expected, $result->getData());
    }
}
