<?php
/**
 * @author Шахновский Александр
 */
namespace ApplicationTest\Filter\From\FromArray;

use \RedoxWeb\WTL\Filter\From\FromArray\FilterCreate;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

class FilterCreateTest extends \PHPUnit\Framework\TestCase
{
    protected function createFilter()
    {
        return new FilterCreate('ApplicationTest\Filter\Mock\ValueObject');
    }

    public function testFilterCreateObject()
    {
        $data = array('data' => 'value');
        $filter = $this->createFilter();
        /** @var \ApplicationTest\Filter\Mock\ValueObject $valueObject */
        $valueObject = $filter->filter($data);
        $this->assertInstanceOf('ApplicationTest\Filter\Mock\ValueObject', $valueObject);
        $this->assertSame('value', $valueObject->getData());
    }

    public function testFilterReturnInitialValue()
    {
        $data = array('data' => 'null');
        $filter = $this->createFilter();
        $filteredData = $filter->filter($data);
        $this->assertSame($data, $filteredData);
    }
    public function testFilterNullValue()
    {
        $data = null;
        $filter = $this->createFilter();
        $filteredData = $filter->filter($data);
        $this->assertSame($data, $filteredData);
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testSetWrongObjectName()
    {
        new FilterCreate('ApplicationTest\Filter\Mock\SimpleObject');
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testFilterNullOption()
    {
        new FilterCreate(null);
    }

    public function testInputFilterOption()
    {
        $filter = $this->createFilter();
        $inputFilter = new InputFilter();
        $filter->setInputFilter($inputFilter);
        $this->assertSame($inputFilter, $filter->getInputFilter());
    }

    /**
     * @expectedException \RedoxWeb\WTL\Filter\Exception\InvalidFilterConfig
     */
    public function testSetInvalidInputFilter()
    {
        $filter = $this->createFilter();
        $filter->setInputFilter('a');
    }

    public function testUseInputFilterBefore()
    {
        $filter = $this->createFilter();
        $inputFilterFactory = new Factory();
        $config = array(
            'data' => array(
                'name' => 'data',
                'required' => true,
                'filters' => array(
                    array('name' => 'int')
                ),
                'validators' => array(
                    array(
                        'name' => 'identical',
                        'options' => array(
                            'token' => 1,
                            'strict' => true
                        )
                    )
                )
            )
        );
        $inputFilter = $inputFilterFactory->createInputFilter($config);
        $filter->setInputFilter($inputFilter);

        /** @var \ApplicationTest\Filter\Mock\ValueObject $result */
        $result = $filter->filter(array('data' => '1'));
        $this->assertInstanceOf('ApplicationTest\Filter\Mock\ValueObject', $result);
        $this->assertSame(1, $result->getData());

    }

    /**
     * @expectedException \Application\Service\Exception\ValidationException
     */
    public function testInputFilterValidationFail()
    {
        $filter = $this->createFilter();
        $inputFilterFactory = new Factory();
        $config = array(
            'data' => array(
                'name' => 'data',
                'required' => true,
                'filters' => array(
                    array('name' => 'int')
                ),
                'validators' => array(
                    array(
                        'name' => 'identical',
                        'options' => array(
                            'token' => 1,
                            'strict' => true
                        )
                    )
                )
            )
        );
        $inputFilter = $inputFilterFactory->createInputFilter($config);
        $filter->setInputFilter($inputFilter);

        /** @var \ApplicationTest\Filter\Mock\ValueObject $result */
        $data = array('data' => 'a');
        $result = $filter->filter($data);
        $this->assertInternalType('array', $result);
        $this->assertSame($data, $result);
    }
}
