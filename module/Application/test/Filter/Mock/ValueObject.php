<?php
/**
 * @author Шахновский Александр
 */
namespace ApplicationTest\Filter\Mock;

use \RedoxWeb\WTL\Filter\From\FromArray\FactoryInterface;
use \RedoxWeb\WTL\Filter\From\FromScalar\FactoryInterface as FactoryInterfaceScalar;

class ValueObject implements FactoryInterface, FactoryInterfaceScalar
{
    protected $data;

    public function __construct($data)
    {
        if ($data == 'null') {
            throw new \Exception('for test');
        }
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return object
     */
    public static function createFromArray(array $data)
    {
        return new self($data['data']);
    }

    /**
     * @param $data
     * @return object
     */
    public static function createFromScalar($data)
    {
        return new self($data);
    }
}
