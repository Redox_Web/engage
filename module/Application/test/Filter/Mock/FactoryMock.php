<?php
/**
 * @author Шахновский Александр
 */
namespace ApplicationTest\Filter\Mock;

use Application\Tool\Factory\FactoryInterface;

class FactoryMock implements FactoryInterface
{
    /**
     * @var string
     */
    protected $alias;

    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param $name
     * @return boolean
     */
    public function canCreate($name)
    {
        return $name == $this->alias;
    }

    /**
     * @param $data
     * @return \stdClass
     */
    public function create($data)
    {
        return new \stdClass();
    }
}
