<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item;

use Application\Entity\Course\Item\Video;
use PHPUnit\Framework\TestCase;

class VideoTest extends TestCase
{
    /**
     * @return Video
     */
    protected function createVideo()
    {
        $video = new Video();

        return $video;
    }

    public function testSetGetVideo()
    {
        $video = $this->createVideo();
        $videoUrl = 'video-url';
        $video->setVideo($videoUrl);

        $this->assertSame($videoUrl, $video->getVideo());
    }
}
