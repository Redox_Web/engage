<?php
/**
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item;

use Application\Entity\Course\Item\Quiz;
use Application\Entity\Course\Item\Quiz\QuizVO;
use PHPUnit\Framework\TestCase;

class QuizTest extends TestCase
{
    /**
     * @return Quiz
     */
    protected function createQuiz()
    {
        $quiz = new Quiz();

        return $quiz;
    }

    public function testSetGetQuiz()
    {
        $quiz = $this->createQuiz();
        $quizVO = $this->getMockBuilder(QuizVO::class)->disableOriginalConstructor()->getMock();

        $quiz->setQuiz($quizVO);

        $this->assertSame($quizVO, $quiz->getQuiz());
    }
}
