<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item\Quiz;

use Application\Entity\Course\Item\Quiz\Question;
use Application\Entity\Course\Item\Quiz\QuizVO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class QuizVOTest extends TestCase
{
    /**
     * @param array $data
     *
     * @return QuizVO
     */
    protected function createQuizVO(array $data)
    {
        $quiz = new QuizVO($data);

        return $quiz;
    }

    public function testConstruct()
    {
        $data = [
            'questions' => [
                [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 1,
                ],
            ],
        ];

        $quiz = $this->createQuizVO($data);

        $this->assertAttributeInstanceOf(ArrayCollection::class, 'quizData', $quiz);
    }

    public function testCreateFromArray()
    {
        $data = [
            'questions' => [
                [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 1,
                ],
            ],
        ];

        $quiz = QuizVO::createFromArray($data);

        $this->assertAttributeInstanceOf(ArrayCollection::class, 'quizData', $quiz);
        $this->assertSame($data, $quiz->getQuizData()->toArray());
    }

    public function testGetQuizData()
    {
        $data = [
            'questions' => [
                [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 1,
                ],
            ],
        ];

        $quiz = $this->createQuizVO($data);

        $this->assertInstanceOf(Collection::class, $quiz->getQuizData());
        $this->assertSame($data, $quiz->getQuizData()->toArray());
    }

    public function dataCreateFailed()
    {
        return [
            'no questions' => [
                'data' => [],
                'message' => 'Provide data: questions',
            ],
            'invalid data for question' => [
                'data' => [
                    'questions' => [
                        [
                            'question' => 'Who?',
                        ],
                    ],
                ],
                'message' => 'Provide data: question, answers, correctAnswer',
            ],
        ];
    }

    /**
     * @param array $data
     * @param string $message
     *
     * @dataProvider dataCreateFailed
     * @expectedException \RedoxWeb\WTL\Entity\Exception\InvalidArgument
     */
    public function testCreateFailed(array $data, $message)
    {
        $this->expectExceptionMessage($message);
        $this->createQuizVO($data);
    }

    public function testGetQuestions()
    {
        $data = [
            'questions' => [
                [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 1,
                ],
            ],
        ];

        $quiz = $this->createQuizVO($data);

        $this->assertContainsOnlyInstancesOf(Question::class, $quiz->getQuestions());
    }

    public function testGetQuestionsAfterDbInit()
    {
        $data = [
            'questions' => [
                [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 1,
                ],
            ],
        ];

        $reflection = new \ReflectionClass(QuizVO::class);
        $quiz = $reflection->newInstanceWithoutConstructor();
        $this->setProperty($quiz, 'quizData', new ArrayCollection($data));

        $this->assertContainsOnlyInstancesOf(Question::class, $quiz->getQuestions());
    }

    /**
     * @param object $object
     * @param string $property
     * @param mixed $value
     */
    protected function setProperty($object, $property, $value)
    {
        $reflection = new \ReflectionClass($object);
        try {
            $refProp = $reflection->getProperty($property);
        } catch (\ReflectionException $e) {
            $refProp = $reflection->getParentClass()->getProperty($property);
        }
        $refProp->setAccessible(true);
        $refProp->setValue($object, $value);
    }
}
