<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item\Quiz;

use Application\Entity\Course\Item\Quiz\Question;
use PHPUnit\Framework\TestCase;

class QuestionTest extends TestCase
{
    /**
     * @param array $data
     *
     * @return Question
     */
    protected function createQuestion(array $data)
    {
        $question = new Question($data);

        return $question;
    }

    public function dataCreateFailed()
    {
        return [
            'empty array' => [
                'data' => [],
                'message' => 'Provide data: question, answers, correctAnswer',
            ],
            'only question' => [
                'data' => [
                    'question' => 'Who?',
                ],
                'message' => 'Provide data: question, answers, correctAnswer',
            ],
            'only answers' => [
                'data' => [
                    'answers' => [
                        1 => 'correct answer',
                    ],
                ],
                'message' => 'Provide data: question, answers, correctAnswer',
            ],
            'empty answers' => [
                'data' => [
                    'question' => 'Who?',
                    'answers' => [
                    ],
                    'correctAnswer' => 2,
                ],
                'message' => 'Provide an array of answers',
            ],
            'only correct answer id' => [
                'data' => [
                    'correctAnswer' => 1,
                ],
                'message' => 'Provide data: question, answers, correctAnswer',
            ],
            'not existing answer id' => [
                'data' => [
                    'question' => 'Who?',
                    'answers' => [
                        1 => 'correct answer',
                    ],
                    'correctAnswer' => 2,
                ],
                'message' => 'Provide a valid correct answer',
            ],
        ];
    }

    /**
     * @param array $data
     * @param string $message
     *
     * @dataProvider dataCreateFailed
     * @expectedException \RedoxWeb\WTL\Entity\Exception\InvalidArgument
     */
    public function testCreateFailed(array $data, $message)
    {
        $this->expectExceptionMessage($message);
        $this->createQuestion($data);
    }

    public function testCreateSuccess()
    {
        $data = [
            'question' => 'Who?',
            'answers' => [
                1 => 'correct answer',
            ],
            'correctAnswer' => 1,
        ];
        $question = $this->createQuestion($data);

        $this->assertInstanceOf(Question::class, $question);
    }

    public function testGetQuestion()
    {
        $questionString = 'Who?';
        $data = [
            'question' => $questionString,
            'answers' => [
                1 => 'correct answer',
            ],
            'correctAnswer' => 1,
        ];
        $question = $this->createQuestion($data);

        $this->assertSame($questionString, $question->getQuestion());
    }

    public function testGetAnswers()
    {
        $answers = [
            1 => 'correct answer',
        ];
        $data = [
            'question' => 'Who?',
            'answers' => $answers,
            'correctAnswer' => 1,
        ];
        $question = $this->createQuestion($data);

        $this->assertSame($answers, $question->getAnswers());
    }

    public function testGetCorrectAnswer()
    {
        $correctAnswer = 'correct answer';
        $data = [
            'question' => 'Who?',
            'answers' => [
                1 => 'correct answer',
            ],
            'correctAnswer' => 1,
        ];
        $question = $this->createQuestion($data);

        $this->assertSame($correctAnswer, $question->getCorrectAnswer());
    }

    public function testGetCorrectAnswerId()
    {
        $correctAnswerId = 1;
        $data = [
            'question' => 'Who?',
            'answers' => [
                1 => 'correct answer',
            ],
            'correctAnswer' => 1,
        ];
        $question = $this->createQuestion($data);

        $this->assertSame($correctAnswerId, $question->getCorrectAnswerId());
    }
}
