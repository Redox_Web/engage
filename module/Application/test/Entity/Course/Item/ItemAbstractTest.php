<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item;

use Application\Entity\Course\Item\ItemAbstract;
use Application\Entity\Course\Item\State;
use PHPUnit\Framework\TestCase;

class ItemAbstractTest extends TestCase
{
    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ItemAbstract
     */
    protected function createItem()
    {
        $mock = $this->getMockBuilder(ItemAbstract::class)->disableOriginalConstructor()->getMockForAbstractClass();

        return $mock;
    }

    public function testGetIdentity()
    {
        $identity = 2;
        $item = $this->createItem();
        $this->setProperty($item, 'identity', $identity);

        $this->assertSame($identity, $item->getIdentity());
    }

    public function testSetGetName()
    {
        $item = $this->createItem();
        $name = 'someName';
        $item->setName($name);

        $this->assertSame($name, $item->getName());
    }

    public function testGetAlias()
    {
        $alias = 'some-alias';
        $item = $this->createItem();
        $this->setProperty($item, 'alias', $alias);

        $this->assertSame($alias, $item->getAlias());
    }

    public function testSetGetDescription()
    {
        $description = <<<EOT
SOME LONG TEXT
AND SOME MORE TEXT
EOT;
        $item = $this->createItem();
        $item->setDescription($description);

        $this->assertSame($description, $item->getDescription());
    }

    public function testSetGetFree()
    {
        $free = true;
        $item = $this->createItem();
        $item->setIsFree($free);

        $this->assertSame($free, $item->isFree());
    }

    public function testSetStartDate()
    {
        $item = $this->createItem();
        $date = new \DateTime();
        $item->setStartDate($date);

        $this->assertAttributeEquals($date, 'startDate', $item);
        $this->assertAttributeNotSame($date, 'startDate', $item);
    }

    public function testGetStartDate()
    {
        $item = $this->createItem();
        $date = new \DateTime();
        $this->setProperty($item, 'startDate', $date);

        $this->assertEquals($date, $item->getStartDate());
        $this->assertNotSame($date, $item->getStartDate());
    }

    public function testGetSetState()
    {
        $state = new State(State::ACTIVE);
        $item = $this->createItem();
        $item->setState($state);

        $this->assertSame($state, $item->getState());
    }

    public function dataIsActive()
    {
        return [
            'true' => [
                'state' => State::ACTIVE,
                'expected' => true,
            ],
            'false' => [
                'state' => State::INACTIVE,
                'expected' => false,
            ],
        ];
    }

    /**
     * @param $state
     * @param boolean $expected
     * @dataProvider dataIsActive
     */
    public function testIsActive($state, $expected)
    {
        $state = new State($state);
        $item = $this->createItem();
        $item->setState($state);

        $this->assertSame($expected, $item->isActive());
    }

    public function testConstruct()
    {
        /** @var ItemAbstract $item */
        $item = $this->getMockBuilder(ItemAbstract::class)->getMockForAbstractClass();
        $this->assertAttributeInstanceOf(\DateTime::class, 'startDate', $item);
        $this->assertAttributeInstanceOf(State::class, 'state', $item);
        $this->assertSame(State::INACTIVE, $item->getState()->getState());
    }

    /**
     * @param object $object
     * @param string $property
     * @param mixed $value
     */
    protected function setProperty($object, $property, $value)
    {
        $reflection = new \ReflectionClass($object);
        try {
            $refProp = $reflection->getProperty($property);
        } catch (\ReflectionException $e) {
            $refProp = $reflection->getParentClass()->getProperty($property);
        }
        $refProp->setAccessible(true);
        $refProp->setValue($object, $value);
    }
}
