<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item;

use Application\Entity\Course\Item\Article;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    /**
     * @return Article
     */
    protected function createArticle()
    {
        $article = new Article();

        return $article;
    }

    public function testSetGetContent()
    {
        $content = <<<EOT
SOME LONG TEXT
AND SOME MORE TEXT
EOT;
        $article = $this->createArticle();
        $article->setContent($content);

        $this->assertSame($content, $article->getContent());
    }
}
