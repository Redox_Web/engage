<?php
/**
 *
 * @author Shahnovsky Alex
 */
namespace ApplicationTest\Entity\Course\Item;

use Application\Entity\Course\Item\State;
use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
    /**
     * @param $state
     *
     * @return State
     */
    protected function createState($state)
    {
        $state = new State($state);

        return $state;
    }

    public function testCreateSuccess()
    {
        $state = $this->createState(State::ACTIVE);

        $this->assertInstanceOf(State::class, $state);
    }

    /**
     * @expectedException \Application\Entity\Exception\InvalidArgument
     */
    public function testCreateFailed()
    {
        $state = $this->createState('invalidState');
    }

    public function testGetState()
    {
        $stateValue = State::INACTIVE;
        $state = $this->createState($stateValue);

        $this->assertSame($stateValue, $state->getState());
    }

    public function testGetConstValues()
    {
        $expected = [
            State::ACTIVE,
            State::INACTIVE
        ];

        $this->assertEquals($expected, State::getConstValues());
    }

    public function dataIsActive()
    {
        return [
            'true' => [
                'state' => State::ACTIVE,
                'expected' => true,
            ],
            'false' => [
                'state' => State::INACTIVE,
                'expected' => false,
            ],
        ];
    }

    /**
     * @param $state
     * @param boolean $expected
     * @dataProvider dataIsActive
     */
    public function testIsActive($state, $expected)
    {
        $state = $this->createState($state);

        $this->assertSame($expected, $state->isActive());
    }

    public function testCreateFromScalar()
    {
        $state = State::ACTIVE;
        $stateObj = State::createFromScalar($state);
        $this->assertInstanceOf(State::class, $stateObj);
        $this->assertSame($state, $stateObj->getState());
    }
}
