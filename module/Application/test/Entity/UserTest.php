<?php

namespace Application;

/**
 *
 * @author Melnic Oleg
 */

use Application\Entity\User\UserAbstract;
use Application\Entity\User\State;
use Application\Entity\User\Sex;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testStudentInitialState()
    {
        $student = new \Application\Entity\User\Student();

        $this->assertNull($student->getAddress(), '"address" should initially be null');
        $student->setAddress('test address');
        $this->assertEquals('test address', $student->getAddress(), '"address" should be "test address"');

        $this->assertNull($student->getIdentity(), '"id" should initially be null');

        $this->assertNull($student->getFullName(), '"full name" should initially be null');
        $student->setFullName('Semion Semionich');
        $this->assertEquals('Semion Semionich', $student->getFullName(), '"full name" should be "Semion Semionich"');

        $this->assertNull($student->getBirthday(), '"Birthday" should initially be null');
        $date = new \DateTime('2015-01-01 05:30:30');
        $student->setBirthday($date);
        $this->assertInstanceOf('DateTime', $student->getBirthday());
        $this->assertEquals(
            $date->format('Y-m-d H:i:s'),
            $student->getBirthday()->format('Y-m-d H:i:s'),
            '"Birthday" should be equal "2015-01-01 05:30:30"'
        );

        $this->assertNull($student->getState(), '"State" should initially be null');
        $student->setState(new State(State::ACTIVE));
        $this->assertInstanceOf(\Application\Entity\User\State::class, $student->getState());
        $this->assertEquals(
            State::ACTIVE,
            $student->getState()->getValue(),
            '"State" should be true'
        );

        $this->assertNull($student->getSex(), '"Sex" should initially be null');
        $student->setSex(new Sex(Sex::MALE));
        $this->assertInstanceOf(\Application\Entity\User\Sex::class, $student->getSex());
        $this->assertEquals(
            Sex::MALE,
            $student->getSex()->getValue(),
            '"Sex" should be "m"'
        );
    }
}
