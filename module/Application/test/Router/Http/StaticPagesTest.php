<?php
namespace ApplicationTest\Router\Http;

use Application\Router\Http\StaticPages;
use PHPUnit\Framework\TestCase;
use Zend\Http\Request as Request;
use Zend\Stdlib\Request as BaseRequest;

/**
 * Class StaticPagesTest
 *
 * @author Shahnovsky Alex
 */
class StaticPagesTest extends TestCase
{
    public static function routeProvider()
    {
        return [
            'simple-match' => [
                new StaticPages(['foo' => '/foo']),
                '/foo',
                null,
                true,
                'foo',
            ],
            'simple-match array' => [
                new StaticPages(['foo' => '/foo', 'bar' => '/bar']),
                '/foo',
                null,
                true,
                'foo',
            ],
            'no-match-without-leading-slash' => [
                new StaticPages(['foo' => 'foo']),
                '/foo',
                null,
                false,
                'foo',
            ],
            'no-match-with-trailing-slash' => [
                new StaticPages(['foo' => '/foo']),
                '/foo/',
                null,
                false,
                'foo',
            ],
            'offset-skips-beginning' => [
                new StaticPages(['foo' => 'foo']),
                '/foo',
                1,
                true,
                'foo',
            ],
            'offset-enables-partial-matching' => [
                new StaticPages(['foo' => '/foo']),
                '/foo/bar',
                0,
                false,
                'foo',
            ],
        ];
    }

    /**
     * @dataProvider routeProvider
     *
     * @param        StaticPages $route
     * @param        string $path
     * @param        integer $offset
     * @param        bool $shouldMatch
     * @param        string $alias
     */
    public function testMatching(StaticPages $route, $path, $offset, $shouldMatch, $alias)
    {
        $request = new Request();
        $request->setUri('http://example.com'.$path);
        $match = $route->match($request, $offset);

        if (!$shouldMatch) {
            $this->assertNull($match);
        } else {
            $this->assertInstanceOf(\Zend\Router\Http\RouteMatch::class, $match);
            $this->assertSame($alias, $match->getParam('alias'));

            if ($offset === null) {
                $this->assertEquals(strlen($path), $match->getLength());
            }
        }
    }

    public function dataAssembling()
    {
        $routeProviderData = $this->routeProvider();

        return array_filter(
            $routeProviderData,
            function ($value) {
                return $value[3];
            }
        );
    }

    /**
     * @dataProvider dataAssembling
     *
     * @param        StaticPages $route
     * @param        string $path
     * @param        integer $offset
     * @param        bool $shouldMatch
     * @param        string $alias
     */
    public function testAssembling(StaticPages $route, $path, $offset, $shouldMatch, $alias)
    {
        if (!$shouldMatch) {
            // Data which will not match are not tested for assembling.
            return;
        }
        $result = $route->assemble(['alias' => $alias]);

        if ($offset !== null) {
            $this->assertEquals($offset, strpos($path, $result, $offset));
        } else {
            $this->assertEquals($path, $result);
        }
    }

    public function testNoMatchWithoutUriMethod()
    {
        $route = new StaticPages(['/foo']);
        $request = new BaseRequest();

        $this->assertNull($route->match($request));
    }

    public function testGetAssembledParams()
    {
        $route = new StaticPages(['bar' => '/foo']);
        $route->assemble(['alias' => 'bar']);

        $this->assertEquals([], $route->getAssembledParams());
    }

    /**
     * @group ZF2-436
     */
    public function testEmptyRouter()
    {
        $request = new Request();
        $route = new StaticPages(['']);
        $this->assertNull($route->match($request, 0));
    }
}
