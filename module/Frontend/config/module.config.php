<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend;

use Frontend\Controller\Feedback\Feedback;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\TreeRouteStack;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'router_class' => TreeRouteStack::class,
        'routes' => [
            'frontend' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                    'route_plugins' => [],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'static_page' => [
                        'type' => 'staticPages',
                        'options' => [
                            'site' => 'front_end',
                            'defaults' => [
                                'controller' => \Frontend\Controller\StaticPageController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'action' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '[:action]/',
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'signup' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'signup/',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'signUp',
                            ],
                        ],
                    ],
                    'login' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'login/',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'logout' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'logout/',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'logout',
                            ],
                        ],
                    ],
                    'social' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'social/[:provider/]',
                            'defaults' => [
                                'controller' => Controller\SocialController::class,
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'socialbackend' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'socialbackend/[:provider/]',
                            'defaults' => [
                                'controller' => Controller\SocialController::class,
                                'action' => 'socialbackend',
                                // 'provider' => 'facebook'
                            ],
                        ],
                    ],
                    'socialdisconnect' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'socialdisconnect/[:provider/]',
                            'defaults' => [
                                'controller' => Controller\SocialController::class,
                                'action' => 'disconnect',
                            ],
                        ],
                    ],
                    'reset-password' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'reset-password',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'resetPassword',
                            ],
                        ],
                    ],
                    'set-password' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'set-password',
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'setPassword',
                            ],
                        ],
                    ],
                    'message' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'message[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                                'controller' => Controller\UserController::class,
                                'action' => 'message',
                            ],
                        ],
                    ],
                    'subscription_plans' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'subscription-plans',
                            'defaults' => [
                                'controller' => Controller\IndexController::class,
                                'action' => 'subscriptionPlans',
                            ],
                        ],
                    ],
                    'feedback' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'feedback/',
                            'defaults' => [
                                'controller' => Feedback::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'session_containers' => [
        'FrontendSession'
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\SocialController::class => InvokableFactory::class,
            Controller\UserController::class => InvokableFactory::class,
            \Frontend\Controller\StaticPageController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__.'/../view/frontend/layout/layout.phtml',
            'frontend/index/index' => __DIR__.'/../view/frontend/index/index.phtml',
            'error/404' => __DIR__.'/../view/error/404.phtml',
            'error/index' => __DIR__.'/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__.'/../view',
        ],
    ],
    'service_manager' => [
        'factories' => [
            \Frontend\Seo\ConfigProvider::class => InvokableFactory::class,
        ],
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'Frontend' => [
                    "controllers" => [
                        \Frontend\Controller\IndexController::class => [
                            'default' => [], // anyone can access
                        ],
                        \Frontend\Controller\UserController::class => [
                            'default' => [], // anyone can access
                        ],
                        \Frontend\Controller\SocialController::class => [
                            'default' => [], // anyone can access
                        ],
                        \Frontend\Controller\StaticPageController::class => [
                            'default' => [], // anyone can access
                        ],
                        Feedback::class => [
                            'default' => [], // anyone can access
                        ],
                        'SocialController' => [
                            'default' => [], // anyone can access
                        ],
                    ],
                ],
            ],
        ],
    ],
];
