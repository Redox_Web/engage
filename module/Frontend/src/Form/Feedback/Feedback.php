<?php
namespace Frontend\Form\Feedback;

use RedoxWeb\WTL\Form\Form;

/**
 * Class Feedback
 *
 * @author Shahnovsky Alex
 */
class Feedback extends Form
{
    /**
     * @var object
     */
    private $captchaAdapter;

    /**
     * @return object
     */
    public function getCaptchaAdapter()
    {
        return $this->captchaAdapter;
    }

    /**
     * @param object $captchaAdapter
     */
    public function setCaptchaAdapter($captchaAdapter)
    {
        $this->captchaAdapter = $captchaAdapter;
    }

    /**
     * UserForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('feedback-form');

        //Set POST method for
        $this->setAttribute('method', 'post');
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    public function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder' => 'Enter Name',
                'class' => 'form-control',
            ],
            'options' => [
                'label' => 'Name',
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'email',
            'attributes' => [
                'id' => 'email',
                'placeholder' => 'Enter Email',
                'class' => 'form-control',
            ],
            'options' => [
                'label' => 'Email',
            ],
        ]);

        $this->add([
            'type' => 'textarea',
            'name' => 'content',
            'attributes' => [
                'id' => 'content',
                'class' => 'form-control',
            ],
            'options' => [
                'label' => 'Content',
            ],
        ]);

        $this->add([
            'type' => 'captcha',
            'name' => 'captcha',
            'options' => [
                'label' => 'Please verify you are human',
                'captcha' => $this->getCaptchaAdapter(),
            ],
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submitbutton',
                'class' => 'button button-green-out',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Frontend\InputFilter\Feedback\Feedback';
    }
}
