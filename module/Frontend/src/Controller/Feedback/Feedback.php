<?php
namespace Frontend\Controller\Feedback;

use Application\Controller\AbstractActionController;
use Application\Service\Mail\Feedback\MailService;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;

/**
 * Class Feedback
 *
 * @author Shahnovsky Alex
 */
class Feedback extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $form = $form = $this->getServiceLocator()->get(\Frontend\Form\Feedback\Feedback::class);

        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            if ($form->isValid()) {
                $data = $form->getData();
                /** @var MailService $mailService */
                $mailService = $this->getServiceLocator()->get(MailService::class);
                $mailService->sendFeedback($data);
                $this->flashMessenger()->addMessage('Thank you for your Feedback!');

                return $this->redirect()->toRoute('frontend/feedback');
            }
        }

        $viewModel = new ViewModel([
            'form' => $form
        ]);
        $viewModel->setTemplate('frontend/feedback/index');

        return $viewModel;
    }
}
