<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Frontend\Controller;

use Application\Service\Subscription\SubscriptionPackage;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Index action.
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    public function subscriptionPlansAction()
    {
        /** @var SubscriptionPackage $subscriptionPackages */
        $subscriptionPackages = $this->getServiceLocator()->get(SubscriptionPackage::class);
        $allPackages = $subscriptionPackages->getAllPackages();

        return new ViewModel([
            'allPackages' => $allPackages,
        ]);
    }
}
