<?php
namespace Frontend\Controller;

use Application\Entity\User\State;
use Application\Service\Exception\EntityNotFound;
use Application\Service\Exception\ValidationException;
use CirclicalUser\Service\AuthenticationService;
use Application\Factory\Social\UserCustomWrapperFactory;
use Zend\Mvc\Controller\AbstractActionController;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Application\Service\User\Social\User as SocialUser;
use Application\Service\User\Social\Social;
use Zend\View\Model\ViewModel;


/**
 * Class SocialController
 * Controller for authentificate through social networks
 * @package Frontend\Controller
 */

class SocialController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Popup paremeters
     *
     * @var array
     */
    protected $jsArguments = array('popup' => array('width' => 500, 'height' => 450));

    /**
     * login using social accounts
     */
    public function loginAction()
    {
        $providerName = $this->params()->fromRoute('provider');

        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")
                                  ->offsetSet('redirect', $this->params()->fromRoute('redirect'));

        $provider = $this->getServiceLocator()->get("OrgHeiglHybridAuthBackend")->getProvider($providerName);

        return $this->redirectTo($provider->makeAuthUrl());
    }

    /**
     * Logout
     */
    public function logoutAction()
    {
        /**
         * @var \OrgHeiglHybridAuth\SocialAuthUserWrapper $socialAuthUserWrapper
         */
        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('authenticated', false);
        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('user', null);
        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('backend', null);

        return $this->doRedirect();
    }

    /**
     * Redirect to the last known URL
     *
     * @return boolean
     */
    protected function doRedirect()
    {
        if (! $redirect = $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetGet('redirect')) {
            $redirect = $this->getEvent()->getRouteMatch()->getParam('redirect');
        }

        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetUnset('redirect');
        $redirect = base64_decode($redirect);

        if (! $redirect) {
            $redirect = '/';
        }

        if (preg_match('|://|', $redirect)) {
            $this->redirect()->toUrl($redirect);
        } else {
            $this->redirect()->toRoute($redirect);
        }
        return false;
    }

    public function redirectTo($uri)
    {
        $this->redirect()->toUrl($uri);
    }

    /**
     * Call the HybridAuth-Backend
     */
    public function socialbackendAction()
    {

        $view = new ViewModel([
            'url' => $this->url()->fromRoute('frontend'),
            'redirect' => true,
            'params' => array()
        ]);
        $view->setTemplate('application/auth/redirect');

        $providerName = $this->params()->fromRoute('provider');
        $provider = $this->getServiceLocator()->get("OrgHeiglHybridAuthBackend")->getProvider($providerName);
        $accessToken = $provider->getAccessTokenByRequestParameters($_GET);

        if (!$accessToken) {
            $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('authenticated', false);
            $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('user', null);
            $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('backend', $providerName);

            return $this->doRedirect();
        }

        /**
         * @var \Application\Entity\Social\SocialCustomAuthUserWrapper $socialAuthUserWrapper
         */
        $socialAuthUserWrapper = $this->getServiceLocator()
                                      ->get(UserCustomWrapperFactory::class)
                                      ->factory($provider->getIdentity($accessToken));

        if (!$socialAuthUserWrapper->isAgeSuitalbe()) {
            throw new ValidationException('Your age must be greater then 13');
        }

        $socialIdentity = $socialAuthUserWrapper->getUID();

        /** @var Social $socialService */
        $socialService = $this->getServiceLocator()->get(Social::class);
        $social = $socialService->findByAlias($providerName);
        if (!$social) {
            throw new EntityNotFound("Not found social provider entity: $providerName.");
        }

        /**
         * @var SocialUser $socialUserService
         */
        $socialUserService = $this->getServiceLocator()->get(SocialUser::class);

        /** in case when we want to link to social account when we are loged in (i.e. from profile page) */
        if ($this->auth()->getIdentity()) {
            /** create social user */
            $socialUserService->register($this->auth()->getIdentity(), $social, $socialIdentity);

            $view->url = $this->url()->fromRoute('personal-office/linked-accounts');
            return $view;
        }

        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('authenticated', true);
        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('user', $socialAuthUserWrapper);
        $this->getServiceLocator()->get("OrgHeiglHybridAuthSession")->offsetSet('backend', $providerName);

        /** @var $user \Application\Entity\User\UserAbstract|null */
        $user = $socialUserService->findMainUserBySocialKeyActive($socialAuthUserWrapper->getUID(), $social);
        if (!$user
            && !$socialAuthUserWrapper->getBirthday()
            && !$socialUserService->checkUserExistance($socialAuthUserWrapper->getMail())) {

            $sessionContainer = $this->getServiceLocator()->get('FrontendSession');

            $sessionContainer->data = [
                'email' => $socialAuthUserWrapper->getMail(),
                'fullName' => $socialAuthUserWrapper->getDisplayName(),
            ];

            $view->url = $this->url()->fromRoute('frontend/signup');
            return $view;
        }


        if (!$user) {
            /** create simple user */
            $user = $socialUserService->createUser($socialAuthUserWrapper);
        }

        /** create social user */
        $socialUserService->register($user, $social, $socialIdentity);

        /** @var \Application\Service\Auth\AuthenticationService $auth */
        $auth = $this->getServiceLocator()->get(AuthenticationService::class);
        $auth->authenticateByEntity($user);

        return $view;
    }

    /**
     * disconnect action.
     * disconnect from social account (i.e. in profile page)
     */
    public function disconnectAction()
    {
        $providerName = $this->params()->fromRoute('provider');
        /** @var Social $socialService */
        $socialService = $this->getServiceLocator()->get(Social::class);
        $social = $socialService->findByAlias($providerName);
        // Get current user.
        $user = $this->auth()->getIdentity();
        if (!$social || !$user) {
            $this->notFoundAction();
        }

        /**
         * @var SocialUserService $socialUserService
         */
        $socialUserService = $this->getServiceLocator()->get(SocialUser::class);
        $socialUserService->disconnect($user, $social);
        $this->redirect()->toRoute('personal-office/linked-accounts');
    }
}
