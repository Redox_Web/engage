<?php
namespace Frontend\Controller;

use Application\Controller\AbstractActionController;
use Application\Entity\User\Admin;
use Application\Entity\User\Professor;
use Application\Entity\User\State;
use Application\Entity\User\Student;
use Application\Entity\User\UserAbstract;
use Application\Form\LoginForm;
use Application\Service\Mail\User\MailService;
use Application\Service\User\User;
use CirclicalUser\Exception\BadPasswordException;
use CirclicalUser\Exception\NoSuchUserException;
use CirclicalUser\Service\AuthenticationService;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use RedoxWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;
use Application\Form\UserForm;
use Application\Form\PasswordResetForm;
use Application\Form\PasswordChangeForm;
use Application\Service\User\Mapper\UserMapper;

/**
 * This controller is responsible for user management (adding, editing,
 * viewing users and changing user's password).
 */
class UserController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Authenticates user given email address and password credentials.
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function loginAction()
    {
        if ($this->auth()->getIdentity()) {
            return $this->redirect()->toRoute('personal-office/index');
        }
        $view = new ViewModel();
        $form = $this->getServiceLocator()->get(LoginForm::class);
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $form->setData($formData);
            if ($form->isValid()) {
                try {
                    $formData = $form->getData();
                    $user = $this->auth()->authenticate($formData['email'], $formData['password']);
                    if ($user instanceof Admin) {
                        return $this->redirect()->toRoute('backend');
                    } elseif ($user instanceof Student || $user instanceof Professor) {
                        return $this->redirect()->toRoute('personal-office');
                    } else {
                        return $this->redirect()->toRoute('frontend');
                    }
                } catch (BadPasswordException $exception) {
                    $form->populateValues($formData);
                    $view->setVariables(['errors' => 'Wrong password or login']);
                } catch (NoSuchUserException $exception) {
                    $form->populateValues($formData);
                    $view->setVariables(['errors' => 'Wrong password or login']);
                }
            } else {
                $form->populateValues($formData);
                $view->setVariables(['messages' => $form->getMessages()]);
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('frontend/layout/signup_layout');

        $view->setVariables(
            [
                'form' => $form
            ]
        );

        return $view;
    }

    /**
     * The "logout" action performs logout operation.
     *
     * @return \Zend\Http\Response
     */
    public function logoutAction()
    {
        $this->auth()->clearIdentity();

        return $this->redirect()->toRoute('frontend/login');
    }

    /**
     * This action displays a page allowing to sign up a new user.
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function signUpAction()
    {
        if ($this->auth()->getIdentity()) {
            return $this->redirect()->toRoute('personal-office/index');
        }
        /** @var UserForm $form */
        $form = $this->getServiceLocator()->get(UserForm::class);

        $sessionContainer = $this->getServiceLocator()->get('FrontendSession');
        if (isset($sessionContainer->data)) {
            $authData = $sessionContainer->data;

            $form->setData($authData);
            $form->get('birthday')->setMessages(['Value is required and can\'t be empty']);

            unset($sessionContainer->data);
        }

        /** @var User $service */
        $service = $this->getServiceLocator()->get(User::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();
            $data['state'] = State::ACTIVE;
            $data['identity'] = '';

            $form->setData($data);

            if ($form->isValid()) {
                $data = $form->getData();
                $data['type'] = $service::TYPE_STUDENT;

                /** @var UserAbstract $user */
                $user = $service->create($data);
                /** @var MailService $sendService */
                $sendService = $this->getServiceLocator()->get(MailService::class);
                $sendService->signUpEmail($data);
                /** @var \Application\Service\Auth\AuthenticationService $auth */
                $auth = $this->getServiceLocator()->get(AuthenticationService::class);
                $auth->authenticateByEntity($user);

                return $this->redirect()->toRoute(
                    'frontend',
                    ['id' => $user->getIdentity()]
                );
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('frontend/layout/signup_layout');

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    /**
     * This action displays the "Reset Password" page.
     */
    public function resetPasswordAction()
    {
        // Create form
        $form = $this->getServiceLocator()->get(PasswordResetForm::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid()) {
                /**
                 * Look for the user with such email.
                 * @var User $user
                 */
                $user = $this->getServiceLocator()->get(UserMapper::class)->findByEmail($data['email']);
                if ($user!=null) {
                    // Generate a new password for user and send an E-mail
                    // notification about that.
                    $this->getServiceLocator()->get(User::class)->generatePasswordResetToken($user);

                    // Redirect to "message" page
                    return $this->redirect()->toRoute(
                        'frontend/message',
                        ['action'=>'message', 'id'=>'sent']
                    );
                } else {
                    return $this->redirect()->toRoute(
                        'frontend/message',
                        ['action'=>'message', 'id'=>'invalid-email']
                    );
                }
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('frontend/layout/signup_layout');

        $view = new ViewModel([
            'form' => $form
        ]);

        return $view;
    }

    /**
     * This action displays the "Set Password" page.
     */
    public function setPasswordAction()
    {
        $token = $this->params()->fromQuery('token', null);

        if ($token != null && (!is_string($token) || strlen($token)!=32)) {
            throw new \Application\Service\Exception\ValidationException('Invalid token type or length');
        }

        if ($token === null ||
            !$this->getServiceLocator()->get(User::class)->validatePasswordResetToken($token)) {
            return $this->redirect()->toRoute(
                'frontend/message',
                ['action'=>'message', 'id'=>'failed']
            );
        }

        // Create form
        $form = $this->getServiceLocator()->get(PasswordChangeForm::class);

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid()) {
                $data = $form->getData();

                // Set new password for the user.
                if ($this->getServiceLocator()->get(User::class)
                                              ->setNewPasswordByToken($token, $data['new_password'])) {
                    // Redirect to "message" page
                    return $this->redirect()->toRoute(
                        'frontend/message',
                        ['action'=>'message', 'id'=>'set']
                    );
                } else {
                    // Redirect to "message" page
                    return $this->redirect()->toRoute(
                        'frontend/message',
                        ['action'=>'message', 'id'=>'failed']
                    );
                }
            }
        }

        $layout = $this->layout();
        $layout->setTemplate('frontend/layout/signup_layout');

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action displays an informational message page.
     * For example "Your password has been resetted" and so on.
     */
    public function messageAction()
    {
        // Get message ID from route.
        $id = (string)$this->params()->fromRoute('id');

        // Validate input argument.
        if ($id!='invalid-email' && $id!='sent' && $id!='set' && $id!='failed') {
            throw new \Exception('Invalid message ID specified');
        }

        return new ViewModel([
            'id' => $id
        ]);
    }
}


