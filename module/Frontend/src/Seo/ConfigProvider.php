<?php
namespace Frontend\Seo;

use Seo\StdLib\SeoConfigInterface;

/**
 * Class ConfigProvider
 *
 * @author Shahnovsky Alex
 */
class ConfigProvider implements SeoConfigInterface
{
    const TEST_ALIAS = 'test-alias';

    /**
     * Get an array of aliases to be registered in seo system.
     *
     * @return array
     */
    public function getAliases()
    {
        return [
            self::TEST_ALIAS,
        ];
    }
}
