<?php
namespace Frontend\InputFilterBuilder\Feedback;

use RedoxWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Feedback
 *
 * @author Shahnovsky Alex
 */
class Feedback extends BuilderAbstract
{
    /**
     * Метод использующийся для найсройки конфига InputFilter
     * @return void
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
        $this->getInputFilterBuilder()->add('email', true, 'base.email');
        $this->getInputFilterBuilder()->add('content', true, 'base.long_text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return void
     */
    protected function buildInvariant()
    {
    }
}
