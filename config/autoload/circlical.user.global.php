<?php

return [
    'service_manager' => [
        'factories' => [
            \CirclicalUser\Service\AuthenticationService::class =>
                \Application\Service\Factory\Auth\AuthenticationService::class,
        ],
    ],
];
