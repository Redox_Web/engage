<?php
return [
    'inputFilterBase' => [
        'base.title' => [
            'validators' => [
                'not_empty' => [
                    'name' => 'not_empty',
                ],
                'stringLength' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 255
                    ]
                ],
            ],
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            )
        ],
        'base.string' => array(
            'validators' => array(),
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            )
        ),
        'base.float' => [
            'validators' => [
                [
                    'name' => 'IsFloat', //'IsFloatSimple'
                    'options' => [
                        'locale' => 'en',
                    ],
                ]
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
//                ['name' => 'ToFloat'],
            ]
        ],
        'base.text' => [
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'toNull')
            ),
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 255
                    ]
                ],
            ]
        ],
        'base.hash' => [
            'filters' => [
                ['name' => 'ToNull']
            ],
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 32
                    ]
                ],
            ]
        ],
        'base.integer' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                [
                    'name' => 'ToNull',
                    'options' => [
                        'type' => [
                            \Zend\Filter\ToNull::TYPE_STRING,
                            \Zend\Filter\ToNull::TYPE_EMPTY_ARRAY,
                            \Zend\Filter\ToNull::TYPE_BOOLEAN,
                        ]
                    ]
                ],
                ['name' => 'int'],
            ],
            'validators' => [
                'int' => [
                    'name' => 'digits'
                ],
            ]
        ],
        'base.integer.positive' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                [
                    'name' => 'ToNull',
                    'options' => [
                        'type' => [
                            \Zend\Filter\ToNull::TYPE_STRING,
                            \Zend\Filter\ToNull::TYPE_EMPTY_ARRAY,
                            \Zend\Filter\ToNull::TYPE_BOOLEAN,
                        ]
                    ]
                ],
                ['name' => 'int'],
            ],
            'validators' => [
                'int' => [
                    'name' => 'digits'
                ],
                'between' => [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => PHP_INT_MAX,
                        'inclusive' => false,
                    ]
                ]
            ]
        ],
        'base.alias' => [
            'validators' => [
                'notEmpty' => [
                    'name' => 'notEmpty',
                    'break_chain_on_failure' => true,
                ],
                'regexp' => [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[a-z0-9-_]+$~'
                    ]
                ],
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 100
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ]
        ],
        'base.entity_name' => [
            'validators' => [
                'notEmpty' => ['name' => 'not_empty'],
                'regexp' => [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[A-Za-z\\\\]+$~'
                    ]
                ],
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 100
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ]
        ],
        'base.uri' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'uri',
                    'options' => [

                    ],
                ]
            ],
        ],
        'base.uri.onlyAbsolute' => [
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'uri',
                    'options' => [
                        'allowRelative' => false
                    ],
                )
            ),
        ],
        'base.position' => [
            'filters' => [
                ['name' => 'int']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => -1,
                        'max' => PHP_INT_MAX,
                    ]
                ]
            ]
        ],
        'base.boolean' => [
            'filters' => [
                [
                    'name' => 'null',
                    'options' => [
                        'type' => [
                            \Zend\Filter\ToNull::TYPE_STRING,
                            \Zend\Filter\ToNull::TYPE_EMPTY_ARRAY,
                        ]
                    ]
                ],
                [
                    'name' => 'boolean',
                    'options' => [
                        'type' => [
                            \Zend\Filter\Boolean::TYPE_ZERO_STRING,
                            \Zend\Filter\Boolean::TYPE_FALSE_STRING,
                            \Zend\Filter\Boolean::TYPE_INTEGER,
                        ],
                        'casting' => false,
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'inArray',
                    'options' => [
                        'haystack' => [true, false],
                        'strict' => true
                    ]
                ]
            ]
        ],
        'base.user' => [
            'filters' => [
                [
                    'name' => 'FilterGetEntity',
                    'options' => [
                        'entity_name' => \Application\Entity\User\UserAbstract::class,
                    ]
                ],
            ],
            'validators' => [
                [
                    'name' => 'Instance',
                    'options' => [
                        'className' => \Application\Entity\User\UserAbstract::class,
                        'messages' => [
                            'notInstanceOf' => 'Wrong user data',
                        ]
                    ]
                ]
            ]
        ],
        'base.email' => [
            'validators' => [
                [
                    'name' => 'emailaddress',
                    'options' => [
                        'messages' => [
                            \Zend\Validator\EmailAddress::INVALID_FORMAT
                            => 'The input is not a valid e-mail address.',
                            \Zend\Validator\EmailAddress::INVALID_HOSTNAME
                            => "Is not a valid hostname for the e-mail address"
                        ]
                    ]
                ],
                [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 50
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'stringtolower']
            ]
        ],
        'base.datetime' => [
            'validators' => [
                [
                    'name' => 'Instance',
                    'options' => [
                        'AllowNull' => true,
                        'className' => 'DateTime',
                    ],
                ],
            ],
            'filters' => [
                'toNull' => [
                    'name' => 'toNull'
                ],
                'datetime' => [
                    'name' => \RedoxWeb\WTL\Filter\DateTime::class
                ],
            ]
        ],
        'base.seo' => [
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 255
                    ]
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'ToNull']
            ],
        ],
        'base.seo.text' => [
            'validators' => [],
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'ToNull']
            ]
        ],
        'base.long_text' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'ToNull']
            ],
            'validators' => [
                'length' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => null
                    ]
                ],
            ]
        ],
        'base.digits' => [
            'filters' => [],
            'validators' => [
                ['name' => 'digits']
            ]
        ],
        'base.ip' => [
            'validators' => [
                [
                    'name' => 'ip',
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ]
        ],
        'base.user.fullname' => [
            'validators' => [
                'stringLength' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 100,
                        'min' => 2,
                    ]
                ],
                'regexp' => [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[а-яА-ЯёЁa-zA-Z0-9 ]+$~',
                        'messages' => [
                            \Zend\Validator\Regex::NOT_MATCH => "Invalid name format.
    You can use only uppercase and lowercase letters(a-z, A-Z)",
                        ],
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'ToNull'],
            ],
        ],
        'base.fio' => [
            'validators' => [
                'stringLength' => [
                    'name' => 'stringlength',
                    'options' => [
                        'max' => 150
                    ]
                ],
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'ToNull'],
            ],
        ],
        'base.date' => [
            'validators' => [
                [
                    'name' => 'Instance',
                    'options' => [
                        'AllowNull' => true,
                        'className' => 'DateTime',
                    ],
                ],
            ],
            'filters' => [
                ['name' => 'ToNull'],
                'datetime' => [
                    'name' => \RedoxWeb\WTL\Filter\DateTime::class
                ],
            ]
        ],
        'base.color' => [
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'inArray',
                    'options' => [
                        'haystack' => [
                            'aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black',
                            'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse',
                            'chocolate', 'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue',
                            'darkcyan', 'darkgoldenrod', 'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta',
                            'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen',
                            'darkslateblue', 'darkslategray', 'darkslategrey', 'darkturquoise', 'darkviolet',
                            'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite',
                            'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray',
                            'green', 'greenyellow', 'grey', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory',
                            'khaki', 'lavender', 'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue',
                            'lightcoral', 'lightcyan', 'lightgoldenrodyellow', 'lightgray', 'magenta', 'maroon',
                            'mediumaquamarine', 'mediumspringgreen', 'moccasin', 'navy', 'oldlace', 'olive',
                            'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen',
                            'paleturquoise', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red', 'rosybrown',
                            'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'springgreen', 'tomato'
                        ],
                        'strict' => true
                    ]
                ]
            ]
        ],
        'base.password' => [
            'filters' => [
                ['name' => 'null'],
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 6,
                        'max' => 15,
                        'messages' => [
                            \Zend\Validator\StringLength::TOO_SHORT =>
                                "Password cannot be less than %min% characters long",
                            \Zend\Validator\StringLength::TOO_LONG =>
                                "Password cannot greater than %max% characters long",
                        ],
                    ]
                ],
                [
                    'name' => 'regex',
                    'options' => [
                        'pattern' => '~^[a-zA-Z0-9@*#_]*$~',
                        'messages' => [
                            \Zend\Validator\Regex::NOT_MATCH => "Invalid password format.
    You can use only uppercase and lowercase letters(a-z, A-Z), numbers(0-9) and special symbols(@,#,*)",
                        ],
                    ]
                ],
            ]
        ],
    ],
];
