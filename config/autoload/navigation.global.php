<?php

return [
    'navigation' => [
        'navigationaep' => [
            'home' => [
                'label' => 'Главная',
                'uri'   => '/',
                'pages' => [
                    'top-menu' => [
                        'label' => 'ENGAGE',
                        'uri'   => '',
                        'id'    => 'top_menu',
                        'pages' => [
                            'home'   => [
                                'id' => 'simple',
                                'label' => 'Home',
                                'route'   => 'frontend',
                            ],
                            'subscription_plans'   => [
                                'id' => 'simple',
                                'label' => 'Subscription plans',
                                'route'   => 'frontend/subscription_plans',
                            ],
                            'courses'   => [
                                'id' => 'simple',
                                'label' => 'Courses',
                                'route'   => 'frontend/static_page',
                                'params' => [
                                    'alias' => 'course-stub',
                                ],
                            ],
                            'help'   => [
                                'id' => 'simple',
                                'label' => 'Help',
                                'route'   => 'frontend/static_page',
                                'params' => [
                                    'alias' => 'help',
                                ],
                            ],
                            'my_courses'   => [
                                'id' => 'professor_links',
                                'label' => 'My courses',
                                'route'   => 'personal-office/course/my-courses',
                                'class' => 'waves-effect button button-green-out',
                            ],
                            'sign_in'   => [
                                'id' => 'user_authorization',
                                'label' => 'Sign in',
                                'route'   => 'frontend/login',
                                'class' => null,
                            ],
                            'sign_up'   => [
                                'id' => 'user_authorization',
                                'label' => 'Sign Up',
                                'route'   => 'frontend/signup',
                                'class' => 'waves-effect button button-green-out',
                            ],
                            'profile'   => [
                                'id' => 'user_profile',
                                'label' => 'Profile',
                                'route'   => 'personal-office/subscription',
                                'class' => 'waves-effect button button-green-out',
                            ],
                            'logout'   => [
                                'id' => 'user_logout',
                                'label' => 'Logout',
                                'route'   => 'frontend/logout',
                                'class' => 'waves-effect button button-green-out',
                            ],
                        ],
                    ],
                    'left-menu-profile' => [
                        'label' => 'PROFILE',
                        'uri'   => '',
                        'id'    => 'left_menu_profile',
                        'pages' => [
                            'personal-data'   => [
                                'id' => 'simple',
                                'label' => 'Personal data',
                                'route'   => 'personal-office/personal-data',
                                'iconClass' => 'icon-data',
                            ],
                            'change-password'   => [
                                'id' => 'simple',
                                'label' => 'Change password',
                                'route'   => 'personal-office/change-password',
                                'iconClass' => 'icon-password',
                            ],
                            'identity-verification'   => [
                                'id' => 'simple',
                                'label' => 'Identity verification',
                                'route'   => 'personal-office/identity-verification',
                                'iconClass' => 'icon-password',
                            ],
                            'linked-accounts'   => [
                                'id' => 'simple',
                                'label' => 'Linked accounts',
                                'route'   => 'personal-office/linked-accounts',
                                'iconClass' => 'icon-social',
                            ],
                            'subscription-plan'   => [
                                'id' => 'simple',
                                'label' => 'Subscription plan',
                                'route'   => 'personal-office/subscription-plan',
                                'iconClass' => 'icon-plan',
                            ],
                        ],
                    ],
                    'left-menu' => [
                        'label' => 'ENGAGE',
                        'uri'   => '',
                        'id'    => 'left_menu',
                        'pages' => [
                        ]
                    ],
                    'profesor-menu' => [
                        'label' => 'Professor\'s menu' ,
                        'uri'   => '',
                        'id'    => 'profesor_menu',
                        'pages' => [
                            'general'   => [
                                'id' => 'simple',
                                'label' => 'GENERAL',
                                'route'   => 'frontend/subscription_plans',
                            ],
                            'course_info'   => [
                                'id' => 'simple',
                                'label' => 'COURSE INFO',
                                'route'   => 'personal-office/course-info',
                            ],
                            'contents'   => [
                                'id' => 'simple',
                                'label' => 'CONTENTS',
                                'route'   => 'frontend/subscription_plans',
                            ],
                            'rewiews'   => [
                                'id' => 'simple',
                                'label' => 'REVIEWS',
                                'route'   => 'frontend/subscription_plans',
                            ],
                            'discussions'   => [
                                'id' => 'simple',
                                'label' => 'DISCUSSIONS',
                                'route'   => 'frontend/subscription_plans',
                            ],
                        ],
                    ],
                    'left-menu-backend' => [
                        'label' => 'LEFT MENU BACKEND',
                        'uri'   => '',
                        'id'    => 'left_menu_backend',
                        'pages' => [
                            'dashboard'   => [
                                'id' => 'dashboard',
                                'label' => 'Dashboard',
                                'route'   => 'backend',
                                'iconContent' => 'settings_input_svideo',
                            ],
                            'payment-transactions'   => [
                                'id' => 'payment_transactions',
                                'label' => 'Payment transactions',
                                'route'   => 'backend/payments',
                                'iconContent' => 'payment',
                            ],
                            'courses'   => [
                                'id' => 'courses',
                                'label' => 'Courses',
                                'route'   => 'backend/courses',
                                'iconContent' => 'view_list',
                            ],
                            'users'   => [
                                'id' => 'users',
                                'label' => 'User management',
                                'route'   => 'backend/users',
                                'iconContent' => 'supervisor_account',
                            ],
                            'static-pages'   => [
                                'id' => 'static-pages',
                                'label' => 'Static pages',
                                'route'   => 'backend/static-pages',
                                'iconContent' => 'subject',
                            ],
                            'mail'   => [
                                'id' => 'mail',
                                'label' => 'Emails',
                                'route'   => 'backend/mail',
                                'iconContent' => 'mail',
                            ],
                            'logout'   => [
                                'id' => 'logout',
                                'label' => 'Logout',
                                'route'   => 'frontend/logout',
                                'iconContent' => 'exit_to_app',
                            ],
                        ],
                    ],
                ],
            ],
            'personal-office-menu' => [
                'label' => 'Professor\'s menu' ,
                'uri'   => '',
                'id'    => 'personal_office_menu',
                'pages' => [],
            ],
            'personal-office-left-menu' => [
                'label' => 'Chapters' ,
                'uri'   => '',
                'id'    => 'personal-office-left-menu',
                'pages' => [],
            ],
        ],
    ],
];
