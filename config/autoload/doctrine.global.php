<?php
return array(
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'proxy_dir'         => 'data/cache/doctrine/proxy',
                'proxy_namespace'   => 'ProxyEntity',
                // metadata cache instance to use. The retrieved service name will
                // be `doctrine.cache.$thisSetting`
                'metadata_cache'    => 'array',

                // DQL queries parsing cache instance to use. The retrieved service
                // name will be `doctrine.cache.$thisSetting`
                'query_cache'       => 'array',

                // ResultSet cache to use.  The retrieved service name will be
                // `doctrine.cache.$thisSetting`
                'result_cache'      => 'array',

                // Hydration cache to use.  The retrieved service name will be
                // `doctrine.cache.$thisSetting`
                'hydration_cache'   => 'array',

                // Mapping driver instance to use. Change this only if you don't want
                // to use the default chained driver. The retrieved service name will
                // be `doctrine.driver.$thisSetting`
                'driver'            => 'orm_default',
                // Generate proxies automatically (turn off for production)
                'generate_proxies'  => false,

                'types' => array(
                ),
            ),
        ),
        'driver' => array(
            'common_driver' => array(
                'cache' => 'array',
            ),
            'auth_driver' => array(
                'cache' => 'array',
            )
        ),
        'migrations_configuration' => array(
            'orm_default' => array(
                'directory' => 'data/migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'MigrationsRedox',
            ),
        ),
        'connection' => array(
            'orm_default' => array(
                'driverClass' => \Doctrine\DBAL\Driver\PDOSqlite\Driver::class,
                'params' => array(
                    'path' => 'data/sqlite/redox.sqlite'
                ),
            ),
        ),
        'cache' => array(
            'memcached' => array(
                'instance' => 'doctrine_memcached'
            )
        ),
    ),
);
