<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return [
    // Session configuration.
    'session_config' => [
        'cookie_lifetime'     => 60*60*1, // Session cookie will expire in 1 hour.
        'gc_maxlifetime'      => 60*60*24*30, // How long to store session data on server (for 1 month).
    ],
    // Session manager configuration.
    'session_manager' => [
        // Session validators (used for security).
        'validators' => [
            RemoteAddr::class,
            HttpUserAgent::class,
        ]
    ],
    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => [
                    'host' => 'localhost',
                    'port' => '3306',
                    'dbname' => 'dbname'
                ]
            ]
        ]
    ],
    'module_layouts' => [
        'Application' => 'application/layout/layout',
        'Frontend' => 'frontend/layout/layout',
        'PersonalOffice' => 'personal-office/layout/layout',
        'Payment' => 'frontend/layout/layout',
        'Backend' => 'backend/layout/layout',
    ],
    'redox' => [
        'feedbackEmail' => 'omelnic@redox.ca',
        'mail' => array(
            'transport' => array(
                'smtp' => array(
                    'name' => 'redox.engage',
                    'host' => '	jersey.wznoc.com',
                    'port'              => 465,
                    'connection_class'  => 'plain', //plain, login, crammd5
                    'connection_config' => array(
                        'username' => 'email',
                        'password' => 'pwd',
                        'ssl'      => 'tls',
                    ),
                ),
                'file' => array
                (
                    'path' => 'data/mails/'
                )
            )
        ),
        'file' => [
            'uploadPath' => 'public/files/imagemanager',
            'webPath' => '/files/imagemanager',
            'downloader' => [
                'basePath' => 'data/files/tmp/',
                'pathDepth' => 3
            ],
            'uploader' => [
                'basePath' => 'public/files/downloads/',
                'pathDepth' => 3,
            ]
        ]
    ],
    'aws' => [
        'bucket-media' => 'engage-media',
        'directories' => [
            'audio' => 'audio/',
            'video' => 'video/',
            'vnd' => 'docs/',
            'text' => 'docs/',
            'application' => 'docs/',
            'image' => 'img/',
        ],
    ],
];
