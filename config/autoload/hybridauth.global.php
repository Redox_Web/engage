<?php
return [
    'router' => [
        'routes' => [
             'hybridauth' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/authenticate',
                    'defaults' => [
                        '__NAMESPACE__' => 'Frontend\Controller',
                        'controller' => 'SocialController',
                        'action'     => 'login',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'login' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/login/:provider[/:redirect]',
                            'defaults' => [
                                'action'   => 'login',
                                'redirect' => 'home'
                            ],
                        ],
                    ],
                    'logout' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/logout[/:redirect]',
                            'defaults' => [
                                'action' => 'logout',
                                'redirect' => 'home'
                            ],
                        ],
                    ],
                    'backend' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/backend/:provider[/]',
                            'defaults' => [
                                'action' => 'backend',
                                'redirect' => 'home',
                            ],
                        ],
                    ]
                ],
            ],
        ],
    ],
    'OrgHeiglHybridAuth' => [
        'socialAuth' => [
            'redirectUri' => 'http://engage-dev.redox-dev.com/socialbackend',
            'provider' => [
//                'twitter' => [
//                    'applicationId' => '',
//                    'applicationSecret' => '',
//                    'scope' => ['email'],
//                ],
                'facebook' => [
                    'applicationId' => '113322409256617',
                    'applicationSecret' => 'b25542511e326ff29ee775af925e72a9',
                    'scope' => ['email', 'public_profile'],
                ],
                'google' => [
                    'applicationId' => '148851836342-vt0v24232mj5ge9lja501qi9q6amffk1.apps.googleusercontent.com',
                    'applicationSecret' => 'riYBdXZ8ds6rrKutBps7skoq',
                    'scope' => [
                        'https://www.googleapis.com/auth/userinfo.email',
                        'https://www.googleapis.com/auth/plus.me',
                        'https://www.googleapis.com/auth/userinfo.profile',
                        'https://www.googleapis.com/auth/plus.login',
                        'https://www.googleapis.com/auth/user.birthday.read'
                    ]
                ],
                'github' => [
                    'applicationId' => '',
                    'applicationSecret' => '',
                    'scope' => ['email'],
                ],
            ],
        ],
        'session_name' => 'orgheiglhybridauth',
        'backend'         => ['Facebook' => 'facebook', 'Google'=>'google'], // could also be ['Twitter', 'Facebook']
        // 'link'            => '<a class="hybridauth" href="%2$s">%1$s</a>', // Will be either inserted as first parameter into item or simply returned as complete entry
        // 'item'            => '<li%2$s>%1$s</li>',
        // 'itemlist'        => '<ul%2$s>%1$s</ul>',
        // 'logincontainer'  => '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">%1$s<b class="caret"></b></a>%2$s</li>',
        // 'logoffcontainer' => '<li>%1$s</li>',
        // 'logoffstring'    => 'Logout %1$s',
        // 'loginstring'     => 'Login%1$s',
        // 'listAttribs'     => null, // Will be inserted as 2nd parameter into item
        // 'itemAttribs'     => null, // Will be inserted as 2nd parameter into itemlist
    ]
];