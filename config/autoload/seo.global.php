<?php
/**
 *
 * @author Shahnovsky Alex
 */

return [
    'redox' => [
        'mapping' => [
            'ArrayMapper' => [
                'seoMapper' => [
                ],
            ],
        ],
    ],
    'seo_manager' => [
        'configProviders' => [
            \Frontend\Seo\ConfigProvider::class,
        ],
        'parser_config_manager' => [
            'aliases' => [
                'test-alias' => \Seo\Service\Parser\Config\NullConfig::class,
            ],
        ],
        'template_provider' => [
            'aliases' => [
                'test-alias' => \Seo\Service\TemplateProvider\NullProvider::class,
            ],
        ],
        'generator' => [
            'aliases' => [
                'test-alias' => \Seo\Service\Generator\NullGenerator::class,
            ],
        ],
    ],
];
